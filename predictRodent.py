#!/usr/bin/env python

import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
from scipy import stats
#import prettytable
import pickle
import os
import datetime
import pandas as pd
#import tabulate as tabulate

def logit(x):
    """
    Function (logit) to convert probability to real number
    """
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    """
    Function to convert real number to probability
    """
    return np.exp(x) / (1 + np.exp(x))

class MakeParas(object):
    def __init__(self):
        """
        set predictionnames for input and output pickle files
        """
        ########################
        self.iter = 1000
        self.monBack = 3

        self.covMod = 'tmp0Rice'       # Rain, Temp, 'RainTemp, Rain0Rice', tmp0Rice, vegTree
        
        self.laggedMonths = np.array([6,4])
        self.CovNames = ('dev_' + self.covMod + str(self.laggedMonths[0]) + '_' +
            str(self.laggedMonths[1]) + '_Back' + str(self.monBack) + '.pkl')
        self.gibbsName = ('gibbs' + self.covMod + str(self.laggedMonths[0]) + '_' +
            str(self.laggedMonths[1]) + '.pkl')

        self.nRandCaptures = 500

        print('predicting model', self.CovNames)
        ########################



class PredictProcessing(object):
    def __init__(self, gibbsobj, rodentpath, makeparas):

        
        #####################################
        #####################################
        # Run functions

        self.getData(gibbsobj, rodentpath, makeparas)

        self.simWrapper()

        self.summaryStats()
        self.plotForecast(makeparas)

        #####################################
        #####################################

    def getData(self, gibbsobj, rodentpath, makeparas):
        """
        read in data from mcmc
        """
        self.rodentpath = rodentpath
        self.makeparas = makeparas
        self.gibbsobj = gibbsobj
        self.ndat = len(self.gibbsobj.siggibbs)
        self.results = np.hstack([self.gibbsobj.bgibbs,
                                self.gibbsobj.alphagibbs,
                                self.gibbsobj.ggibbs,
                                self.gibbsobj.siggibbs])
#        self.agibbs = self.gibbsobj.agibbs
        self.bgibbs = self.gibbsobj.bgibbs
        self.alphagibbs = self.gibbsobj.alphagibbs
        self.nAlpha = np.shape(self.alphagibbs)[1]
        self.ggibbs = self.gibbsobj.ggibbs
        self.siggibbs = self.gibbsobj.siggibbs
        self.nMcmc = len(self.siggibbs)
        self.Ngibbs = self.gibbsobj.Ngibbs

        print('Number of MCMC iterations', self.ndat, 'nAlpha', self.nAlpha)
#        # prediction data arrays
#        self.rainLagPredCov = self.gibbsobj.rainLagPredCov
#        self.tempLagPredCov = self.gibbsobj.rainLagPredCov

        self.rainLagCovAll = self.gibbsobj.rainLagCovAll
        self.tempLagCovAll = self.gibbsobj.tempLagCovAll
        self.monthsAll = self.gibbsobj.monthsAll
        self.yearsAll = self.gibbsobj.yearsAll
        self.Raint_1All = self.gibbsobj.Raint_1All
        self.riceAll = self.gibbsobj.riceAll
        self.vegetablesAll = self.gibbsobj.vegetablesAll
        self.treesAll = self.gibbsobj.treesAll
        self.rainDatAll = self.gibbsobj.rainDatAll
        self.monthRunAll = self.gibbsobj.monthRunAll
        self.nsessionsAll = self.gibbsobj.nsessionsAll
        self.sessionsAll = self.gibbsobj.sessionsAll
        self.nTrappedAll = self.gibbsobj.nTrappedAll
        self.habIndxAll = self.gibbsobj.habIndxAll
        self.nTrapSetAll = self.gibbsobj.nTrapSetAll
        self.nDatAll = len(self.nTrappedAll)
#        self.habIndx = self.gibbsobj.habIndx
        self.years = self.gibbsobj.years
        self.months = self.gibbsobj.months
        self.sessions = self.gibbsobj.sessions
        self.nsessions = self.gibbsobj.nsessions
        self.rainDat = self.gibbsobj.rainDat
        self.rainLagCov = self.gibbsobj.rainLagCov
        self.tempLagCov = self.gibbsobj.tempLagCov

        # crossvalidation data
        self.monthCV = self.gibbsobj.monthCV
        self.yearCV = self.gibbsobj.yearCV

        ##### Incorporate changes to remove first cv point  ################################
        #
#        self.removeFirstMask = (self.monthsAll == 1) & (self.yearsAll == 1988)
#        self.cvPointMask = self.gibbsobj.cvPointMask
#        self.cvPointMask[self.removeFirstMask] = 0
#        self.cvDates = self.gibbsobj.cvDates[1:]
#        self.nCV = self.gibbsobj.nCV -1
#        self.monthCV = self.monthCV[1:]
#        self.yearCV = self.yearCV[1:]
        #
        ####################################################################################

        self.cvPointMask = self.gibbsobj.cvPointMask
        self.cvDates = self.gibbsobj.cvDates
        self.cvDates2Hab = np.append(self.cvDates, self.cvDates)
        self.nCV = self.gibbsobj.nCV

        self.cvMonthPred = self.monthsAll[self.cvPointMask] 
        self.nonPredMask = np.invert(self.cvPointMask)
        self.cvNTrapped = self.nTrappedAll[self.cvPointMask]
        self.cvNTrapSet = self.nTrapSetAll[self.cvPointMask]
        self.cvTrapRate = self.cvNTrapped / self.cvNTrapSet
        self.cvHabIndx = self.habIndxAll[self.cvPointMask]
        self.cvNPred = self.nCV * 2
        self.cvRainDat = self.rainDatAll[self.cvPointMask]

    def makeCVNgibbsIndx(self):
        """
        make indx array cut for Ngibbs dimensions
        """
        ndat = len(self.months)
        seqCVNgibbs = np.arange(ndat)
        self.cvNgibbsMask = np.zeros(ndat, dtype = bool)
        for i in np.arange(self.nCV):
            mmask = self.months == self.monthCV[i]
            ymask = self.years == self.yearCV[i]
            mymask = mmask & ymask
            self.cvNgibbsMask[mymask] = 1
        self.cvNgibbsIndx = seqCVNgibbs[self.cvNgibbsMask]


    ############################################
    ############################################
    ### Functions for forecast 0: 1 month out            
    def simWrapper(self):
        """
        Wrapper fx for running 1-month forecast
        """
        self.iter = self.makeparas.iter
        self.monthsBack = self.makeparas.monBack
        self.nRandCaptures = self.makeparas.nRandCaptures
        self.variateIndx = np.random.choice(self.nMcmc, self.iter)
        self.getMeanRainTemp()
        self.getMeanTrapRateCV()
#        self.getTrapData()
#        self.getCropData()
        self.loopIter()     # run loop function

    def getMeanRainTemp(self):
        """
        get mean rain and temp across non-prediction months for detect model
        """
        self.meanDetectRain = np.zeros(12)
        for i in range(1, 13):
            monMask = self.monthsAll == i
            totMask = monMask & self.nonPredMask
            self.meanDetectRain[(i - 1)] = np.mean(self.rainDatAll[totMask])
        # detect covariate array
        self.detectCovariates = np.ones((12, 2))
        self.detectCovariates[:, 1] = np.log(self.meanDetectRain + 1.0)
        # detect covariates for Rsq Model - real data, not means
        self.detectCovRsqModel = np.ones((self.cvNPred,2))
        self.detectCovRsqModel[:, 1] = np.log(self.cvRainDat + 1.0)

    def getMeanTrapRateCV(self):
        """
        get mean trap rate for each calendar month of year
        """
        # trap rate for all non pred points
        trapSetTmp = self.nTrapSetAll[self.nonPredMask] * 1.0
        trapSetTmp[trapSetTmp == 0.0] = np.nan
        self.nonPredTrapRate = (self.nTrappedAll[self.nonPredMask] /  
            trapSetTmp)
#            self.nTrapSetAll[self.nonPredMask])
        self.nonPredHabIndx = self.habIndxAll[self.nonPredMask]
        self.nonPredMonths = self.monthsAll[self.nonPredMask]
        # empty array to populate with mean traprates for each calendar month 
        self.meanTrapRateCV = np.zeros(self.cvNPred)
        # Deviance using mean trap rate as theta
        self.devUseMeanTR = 0.0
        #### R-squared storage arrays
        self.Rsq_Forecast = np.zeros(self.iter)
        self.Rsq_Model = np.zeros(self.iter)
        self.makeCVNgibbsIndx()
        # Denominator for r-sq predicted
        self.sumDiffMeanTrapRate = 0.0
        # loop thru cv points
        for j in range(self.cvNPred):
            month_j = self.cvMonthPred[j]
            habIndx_j = self.cvHabIndx[j]
            nonPredMask_j = ((self.nonPredMonths == month_j) & 
                (self.nonPredHabIndx == habIndx_j))
            self.meanTrapRateCV[j] = np.nanmean(self.nonPredTrapRate[nonPredMask_j])
            # calc deviance using mean trap rate of nonPred months
            devMeanTR_j = (stats.binom.logpmf(self.cvNTrapped[j], self.cvNTrapSet[j],
                self.meanTrapRateCV[j]))            
            # sum the deviance
            self.devUseMeanTR += devMeanTR_j
            # calc R-sq denominator
            denomDiff = (self.meanTrapRateCV[j] - self.cvTrapRate[j])**2
            self.sumDiffMeanTrapRate += denomDiff
        self.devUseMeanTR = -2.0 * self.devUseMeanTR

    def loopIter(self):
        """
        Create arrays and loop thru iterations
        """
        seqTmp = np.arange(self.nDatAll)
        self.cvIndx = seqTmp[self.cvPointMask]
        self.NForecast = np.zeros((self.iter, self.cvNPred))   # 3-d storage array N
#        self.NCaptured = np.zeros((self.nRandCaptures, self.iter, self.cvNPred), dtype = int)   # 3-d storage array captureed
        self.NCaptured = np.zeros((self.iter, self.cvNPred), dtype = int)   # 3-d storage array captureed
        self.captDeviance = np.empty((self.iter, 3))
        for i in range(self.iter):
            # draw random varitates
            self.drawVariates(i)
            self.loopMonths(i)
            self.calcModelRsq(i)        # calc model Rsq

    def drawVariates(self, i):
        """
        draw random variates for iteration i from gibbs
        """
        self.variate_i = self.variateIndx[i]
#        self.xt_1 = np.log(self.startN[self.variate_i])
#        self.a_i = self.agibbs[self.variate_i]
        self.b_i = self.bgibbs[self.variate_i]        
        self.alpha_i = self.alphagibbs[self.variate_i]
        self.g_i = self.ggibbs[self.variate_i]

    def loopMonths(self, i):
        """
        loop thru months j.
        """
        # storage array for deviance to add up across pred months
        self.devianceAdd = np.zeros(3)
        self.sumRsq_Forecast = 0.0
        for j in range(self.cvNPred):
            # initial data and N
            cvNgibbs_j = self.cvNgibbsIndx[j]
            self.cv_j = self.cvIndx[j]
            self.hab_j = self.habIndxAll[self.cv_j]
            self.xt_1 = np.log(self.Ngibbs[self.variate_i, (cvNgibbs_j - self.monthsBack)])
            # loop thru months Back to get to predicted month
            if self.monthsBack > 1:
                for k in range(self.monthsBack):
                    predIndx = self.cv_j - self.monthsBack + k + 1
                    # initial covariates for pop model
                    self.getPredCovariates(predIndx, modelRsq = False)
                    # if lag > 1 and don't have real data get meanRain data
                    harmonics_j = np.dot(self.tArray_j, self.alpha_i)
                    mu = self.xt_1 + (self.b_i[self.hab_j] * self.xt_1) + harmonics_j
                    nPred = np.exp(mu)
                    self.xt_1 = mu
                    if predIndx == self.cv_j:
                        self.NForecast[i, j] = nPred
                        # simulate the capture process
                        self.captureProcess(i, j)
            else:
                # initial covariates for pop model
                predIndx = self.cv_j
                # initial covariates for pop model
                self.getPredCovariates(predIndx, modelRsq = False)
                # if lag > 1 and don't have real data get meanRain data
                harmonics_j = np.dot(self.tArray_j, self.alpha_i)
                mu = self.xt_1 + (self.b_i[self.hab_j] * self.xt_1) + harmonics_j
                nPred = np.exp(mu)
                self.xt_1 = mu
                self.NForecast[i, j] = nPred
                # simulate the capture process
                self.captureProcess(i, j)
        # populate Rsq Forecast array
        self.Rsq_Forecast[i] = 1. - (self.sumRsq_Forecast / self.sumDiffMeanTrapRate)  


    def calcModelRsq(self, i):
        """
        Calc Rsq predicting 1 month ahead and use known weather data
        """
        # loop cv points
        self.sumRsq_Model = 0.0
        self.rSQ_Model_Loop(i)

    def rSQ_Model_Loop(self, i):
        """
        loop thru cv points
        """
        for j in range(self.cvNPred):
            # initial data and N
            cvNgibbs_j = self.cvNgibbsIndx[j]
            cv_j = self.cvIndx[j]
            hab_j = self.habIndxAll[self.cv_j]
            xt_1 = np.log(self.Ngibbs[self.variate_i, (cvNgibbs_j - 1)])
            # covariates for pop model
            self.getPredCovariates(cv_j, modelRsq = True)
            harmonics_j = np.dot(self.tArray_j, self.alpha_i)
            mu = xt_1 + (self.b_i[hab_j] * xt_1) + harmonics_j
            nPred = np.exp(mu)
            detCov_j = self.detectCovRsqModel[j]
            mon_j = self.monthsAll[self.cv_j]
            lambdaPred = np.exp(np.dot(detCov_j, self.g_i))
            expTerm = nPred * lambdaPred
            th = 1.0 - np.exp(-expTerm)
            ### Calc R-squared for Model
            self.sumRsq_Model += (th - self.cvTrapRate[j])**2.0
        self.Rsq_Model[i] = 1.0 - (self.sumRsq_Model / self.sumDiffMeanTrapRate)
        
    def getPredCovariates(self, predIndx, modelRsq = False):
        """
        get covariates for months being predicted
        """
        # if prediction lag is less or equal to weather lag then just use data
        if (self.monthsBack <= self.makeparas.laggedMonths[1]):
            self.rainGrowDat = self.rainLagCovAll[predIndx]
            self.tempGrowDat = self.tempLagCovAll[predIndx]
        else:                    # else, use means
            mon_j = self.monthsAll[self.cv_j]
            monMask = self.monthsAll == mon_j
            monthPredMask = monMask & self.nonPredMask
            self.rainGrowDat = np.mean(self.rainLagCovAll[monthPredMask])
            self.tempGrowDat = np.mean(self.tempLagCovAll[monthPredMask])
        if modelRsq:
            self.rainGrowDat = self.rainLagCovAll[predIndx]
            self.tempGrowDat = self.tempLagCovAll[predIndx]
        # Array of covariates
        self.tArray_j = np.ones(self.nAlpha)
        self.tArray_j[1] = np.sin(2.0 * np.pi * self.monthsAll[predIndx] / 12.0)
        self.tArray_j[2] = np.cos(2.0 * np.pi * self.monthsAll[predIndx] / 12.0)
        self.tArray_j[3] = np.sin(4.0 * np.pi * self.monthsAll[predIndx] / 12.0)
        self.tArray_j[4] = np.cos(4.0 * np.pi * self.monthsAll[predIndx] / 12.0)
#        self.tArray_j[5] = np.log(self.rainGrowDat + 1.0)
        self.tArray_j[5] = np.log(self.tempGrowDat + 1.0)
#        self.tArray_j[6] = self.riceAll[predIndx]
        self.tArray_j[6] = self.vegetablesAll[predIndx]
        self.tArray_j[7] = self.treesAll[predIndx]

    def captureProcess(self, i, j):
        """
        # do random draw of captured individuals
        """
        mon_j = self.monthsAll[self.cv_j]
        detectCov_ij = self.detectCovariates[(mon_j - 1)]
        self.lambdaPred = np.exp(np.dot(detectCov_ij, self.g_i))
        expTerm = self.NForecast[i, j] * self.lambdaPred
        theta = 1.0 - np.exp(-expTerm)
        ncapt = np.random.binomial(self.nTrapSetAll[self.cv_j], theta)
#        ncapt = np.random.binomial(self.nTrapSetAll[self.cv_j], theta, size = self.nRandCaptures)
        self.NCaptured[i, j] = ncapt  
#        self.NCaptured[:, i, j] = ncapt  
        self.logLikCaptData(i, j, theta)
        ### Calc R-squared for forecast 
        self.sumRsq_Forecast += (theta - self.cvTrapRate[j])**2.0        

    def logLikCaptData(self, i, j, theta):
        """
        calc log lik of observed capture data given parameters and estimated N
        """
        dev = stats.binom.logpmf(self.nTrappedAll[self.cv_j], self.nTrapSetAll[self.cv_j], theta)
        self.devianceAdd[self.hab_j] += dev
        if j == (self.cvNPred - 1):
            self.devianceAdd[2] = self.devianceAdd[0] + self.devianceAdd[1]
            self.captDeviance[i] = self.devianceAdd * -2.0
    
    def summaryStats(self):
        """
        get summary stats from simulation
        """
        # 2-d arrays with rows from mean and CI, columns for months.
        self.summaryN = np.zeros((2, 3, self.nCV)) 
        self.summaryCapt = np.zeros((2, 3, self.nCV))
        meanN = np.mean(self.NForecast, axis = 0)
        meanCapt = np.mean(self.NCaptured, axis = 0)
        for i in range(2):
            habMask = self.cvHabIndx == i
            self.summaryN[i, 0] = meanN[habMask]
            self.summaryCapt[i, 0] = meanCapt[habMask]
            NQuants = mquantiles(self.NForecast[:, habMask], prob=[0.005, 0.995], axis = 0)

            self.summaryN[i, 1:] = NQuants
            self.summaryCapt[i, 1:] = mquantiles(self.NCaptured[:, habMask], prob=[0.005, 0.995], axis = 0)
#            self.getCaptureQuants(i, habMask)

    def getCaptureQuants(self, i, habMask):
        """
        loop through prediction points to get CIs
        """
        nCapt_hab = self.NCaptured[:, :, habMask]
        for m in range(self.nCV):
            nCapt_m = nCapt_hab[:, :, m]            
            self.summaryCapt[i, 1:, m] = mquantiles(nCapt_m, prob = [0.005, 0.995])

    def plotForecast(self, makeparas):
        """
        plot simulation results
        """
        minDate = np.min(self.cvDates) - datetime.timedelta(days = 120)
        maxDate = np.max(self.cvDates) + datetime.timedelta(days = 120)
        P.figure(figsize=(14, 11))
        for i in range(2):
            meanN = self.summaryN[i, 0]
            nLowCI = self.summaryN[i, 1]
            nHighCI = self.summaryN[i, 2]
            meanCapt = self.summaryCapt[i, 0]
            captLowCI = self.summaryCapt[i, 1]
            captHighCI = self.summaryCapt[i, 2]
            nTrapped = self.cvNTrapped[self.cvHabIndx == i]
            ax1 = P.subplot(2, 1, i + 1)
            lns1 = ax1.plot(self.cvDates, meanN, 'ko', label = 'Forecast pop. density', ms = 8)
            lns3 = ax1.plot(self.cvDates, captLowCI, 'b_', label = 'Forecast capture 95% CI', ms = 24)
            lns4 = ax1.plot(self.cvDates, captHighCI, 'b_', ms = 24)                        
            lns5 = ax1.plot(self.cvDates, nTrapped, 'ro', label = 'Mice captured', ms = 8)
            lns = lns1 + lns3 + lns5 
            labs = [l.get_label() for l in lns]
            if i == 0:
                lTit = 'Lowland'
                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            else:
                lTit = 'Upland'
                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            ax1.set_ylim([0, 80])
            ax1.set_xlim(minDate, maxDate)
            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            for tick in ax1.yaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            ax1.set_ylabel('Population and captures', fontsize = 17)
            ax1.set_xlabel('Time', fontsize = 17)
    #        P.xticks([datetime.date(2009, 1, 1), datetime.date(2011, 1, 1), datetime.date(2013, 1, 1),
    #                    datetime.date(2015, 1, 1), datetime.date(2017, 1, 1), datetime.date(2019, 1, 1)])
        figFname = ('forecast' + makeparas.covMod + str(makeparas.laggedMonths[0]) + '_' +
            str(makeparas.laggedMonths[1]) + '_Back' + str(makeparas.monBack) + '.png')
        print('figure name', figFname)
        fnamePath = os.path.join(self.rodentpath, figFname)
        P.savefig(fnamePath, format='png')
        P.show()

    #############################
    ####    CV R-sq of model
    ####    Use known weather data and predict trap rate




class DeviancePickle(object):
    def __init__(self, predictobj, makeparas):
        """
        obj to pickle deviance calculations
        """
        self.captDeviance = predictobj.captDeviance
        self.quantsDev = mquantiles(self.captDeviance, prob=[0.025, 0.975], axis = 0)
        self.meanDev = np.mean(self.captDeviance, axis = 0)
#        print('Quantiles', self.quantsDev, 'Mean =', self.meanDev)
        # make array and save text file to directory
        self.predictQuants = np.zeros((3,5))
        self.predictQuants[:2, :3] = self.quantsDev
        self.predictQuants[2, :3] = self.meanDev 
#        print('quantsMean', self.predictQuants)
        # Rsq results
        self.Rsq_Forecast = predictobj.Rsq_Forecast        
        self.quants_RsqForecast = mquantiles(self.Rsq_Forecast, prob=[0.025, 0.975])
        self.mean_RsqForecast = np.mean(self.Rsq_Forecast)
#        print('meanRSqForecast', self.mean_RsqForecast, 
#            'quants Rsq Forecast', self.quants_RsqForecast)
        # model Rsq results
        self.Rsq_Model = predictobj.Rsq_Model
        self.quants_RsqModel = mquantiles(self.Rsq_Model, prob=[0.025, 0.975])
        self.mean_RsqModel = np.mean(self.Rsq_Model)
#        print('meanRSqModel', self.mean_RsqModel, 'quants RsqModel', self.quants_RsqModel)
        self.predictQuants[:2, 3] = self.quants_RsqForecast
        self.predictQuants[2, 3] = self.mean_RsqForecast
        self.predictQuants[:2, 4] = self.quants_RsqModel
        self.predictQuants[2, 4] = self.mean_RsqModel
        stats = ['2.5 CI', '97.5 CI', 'Mean']
        df = pd.DataFrame({'A) Stats' : stats ,
                        'B) Model R2' : self.predictQuants[:, 4], 
                        'C) Forecast R2' : self.predictQuants[:, 3]})
        print(df)
#        print(tabulate(df, headers='keys', tablefmt='psql'))
 #       print('quantsMean', self.predictQuants)
        # save text file
        fname = ('predQuant_' + makeparas.covMod + str(makeparas.laggedMonths[0]) + '_' +
            str(makeparas.laggedMonths[1]) + '_Back' + str(makeparas.monBack) + '.csv') 
        fnamePath = os.path.join(predictobj.rodentpath, fname)
        print('deviance file name', fnamePath)
        np.savetxt(fnamePath, self.predictQuants, delimiter = ',')
 

def main():

    # paths and data to read in
    rodentpath = os.getenv('CHINAPROJDIR', default='.')

    makeparas = MakeParas()

    inputGibbs = os.path.join(rodentpath, makeparas.gibbsName)
    fileobj = open(inputGibbs, 'rb')
    gibbsobj = pickle.load(fileobj)
    fileobj.close()

    predictobj = PredictProcessing(gibbsobj, rodentpath, makeparas)

    deviancepickle = DeviancePickle(predictobj, makeparas)

    # pickle to directory the deviance
    outDev = os.path.join(rodentpath, makeparas.CovNames)
    fileobj = open(outDev, 'wb')
    pickle.dump(deviancepickle, fileobj)
    fileobj.close()

if __name__ == '__main__':
    main()


