#!/usr/bin/env python

import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
from scipy import stats
import pandas as pd
#import prettytable
import pickle
import os
import datetime
from mcmcApodemus import Gibbs

def logit(x):
    """
    Function (logit) to convert probability to real number
    """
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    """
    Function to convert real number to probability
    """
    return np.exp(x) / (1 + np.exp(x))


class PredictProcessing(object):
    def __init__(self, gibbsobj, monthsBack, outputDataPath):
        """
        ## Predicting CV points for assessing model prediction strength
        """
        self.gibbsobj = gibbsobj
        self.modelNumber = self.gibbsobj.modelNumber
        self.modelName = 'Model' + str(self.modelNumber) 
        ### SET SIMULATION PARAMETERS
        self.iter = 600
        self.nRandCaptures = 500
        #####################################
        print('Simulation iterations:', self.iter)
        print('N random captures:', self.nRandCaptures)

        #####################################
        # Run functions

        self.getData(monthsBack, outputDataPath)

        self.simWrapper()

        self.summaryStats()
        self.plotForecast()

        #####################################
        #####################################

    def getData(self, monthsBack, outputDataPath):
        """
        read in data from mcmc
        """
        self.nMcmc = len(self.gibbsobj.siggibbs)
        self.results = np.hstack([self.gibbsobj.bgibbs,
                                self.gibbsobj.alphagibbs,
                                self.gibbsobj.ggibbs,
                                self.gibbsobj.siggibbs])
#        self.agibbs = self.gibbsobj.agibbs
        self.bgibbs = self.gibbsobj.bgibbs
        self.alphagibbs = self.gibbsobj.alphagibbs
        self.nAlpha = np.shape(self.alphagibbs)[1]
        self.ggibbs = self.gibbsobj.ggibbs
        self.siggibbs = self.gibbsobj.siggibbs
        if self.modelNumber == 3:
            self.lrho_1gibbs = self.gibbsobj.lrho_1gibbs
            self.lrho_2gibbs = self.gibbsobj.lrho_2gibbs            
            self.sigmarhogibbs = self.gibbsobj.sigmarhogibbs         
            self.logit_rhogibbs = self.gibbsobj.logit_rhogibbs
            self.Rho2D = inv_logit(self.logit_rhogibbs)
        self.nMcmc = len(self.siggibbs)
        self.Ngibbs = self.gibbsobj.Ngibbs
        print('Number of MCMC iterations', self.nMcmc, 'nAlpha', self.nAlpha)

        # move variables
        self.nsessions = self.gibbsobj.nsessions      # n sessions per habitat
        self.datSessions = self.gibbsobj.datSessions
        self.datNTrapped = self.gibbsobj.datNTrapped
        self.datNSetTraps = self.gibbsobj.datNSetTraps
        self.datMonthRun = self.gibbsobj.datMonthRun  # from 13 up
        self.datMonth = self.gibbsobj.datMonth
        self.datYear = self.gibbsobj.datYear
        self.datHabitat = self.gibbsobj.datHabitat    # 0 = lowland and 1 = upland
        self.habMask0 = self.datHabitat == 0
        self.habMask1 = self.datHabitat == 1
        self.datStartMonth = self.gibbsobj.datStartMonth
        self.datMinMonthRun = self.gibbsobj.datMinMonthRun
        self.datMaxMonthRun = self.gibbsobj.datMaxMonthRun
        ## repro variables
        self.datNFemales = self.gibbsobj.datNFemales
        self.datNPregnant = self.gibbsobj.datNPregnant
        self.datPromTesticle = self.gibbsobj.datPromTesticle
        self.nDat = self.gibbsobj.nDat                # total data
        self.datRain = self.gibbsobj.datRain
        self.datTemperature = self.gibbsobj.datTemperature
        self.datRice = self.gibbsobj.datRice
        self.datRapeWheat = self.gibbsobj.datRapeWheat
        self.datVegetables = self.gibbsobj.datVegetables
        self.datTrees = self.gibbsobj.datTrees
        self.datDryCrop = self.gibbsobj.datDryCrop
        self.uDatYear = self.gibbsobj.uDatYear
        self.nDatYear = self.gibbsobj.nDatYear
        # Cross validation data
        self.monthCV = self.gibbsobj.monthCV
        self.yearCV = self.gibbsobj.yearCV
        self.cvDates = self.gibbsobj.cvDates
        self.nCV = self.gibbsobj.nCV
        self.cvPointMask = self.gibbsobj.cvPointMask
        self.cvMonthPred = self.datMonth[self.cvPointMask] 
        self.nonPredMask = np.invert(self.cvPointMask)
        self.cvNTrapped = self.datNTrapped[self.cvPointMask]
        self.cvNTrapSet = self.datNSetTraps[self.cvPointMask]
        self.cvTrapRate = self.cvNTrapped / self.cvNTrapSet
        self.cvHabIndx = self.datHabitat[self.cvPointMask]
        self.cvNPred = self.nCV * 2
        self.cvRainDat = self.datRain[self.cvPointMask]
        # Covariates
        self.tArray = self.gibbsobj.tArray
        self.detectCovariates = self.gibbsobj.detectCovariates
        # Params
        self.outputDataPath = outputDataPath
        self.monthsBack = monthsBack
        self.nFetusPerPreg = self.gibbsobj.nFetusPerPreg
        print('Burn in:', self.gibbsobj.burnin)
        print('Thin rate:', self.gibbsobj.thinrate)
        print('############################################')


    def makeCVNgibbsIndx(self):
        """
        make indx array cut for Ngibbs dimensions
        """
#        ndat = len(self.datMonth)
        seqCVNgibbs = np.arange(self.nDat)
        self.cvNgibbsMask = np.zeros(self.nDat, dtype = bool)
        for i in np.arange(self.nCV):
            mmask = self.datMonth == self.monthCV[i]
            ymask = self.datYear == self.yearCV[i]
            mymask = mmask & ymask
            self.cvNgibbsMask[mymask] = 1
        self.cvNgibbsIndx = seqCVNgibbs[self.cvNgibbsMask]      # len of cv points: 48
#        print('cvNgibbsIndx', self.cvNgibbsIndx, len(self.cvNgibbsIndx))


    ############################################
    ############################################
    ### Functions for forecast 0: 1 month out            
    def simWrapper(self):
        """
        Wrapper fx for running 1-month forecast
        """
        self.variateIndx = np.random.choice(self.nMcmc, self.iter)
        self.getMeanRainTemp()
        self.getMeanTrapRateCV()
###        self.getTrapData()
###        self.getCropData()
        self.loopIter()     # run loop function

    def getMeanRainTemp(self):
        """
        get mean rain and temp across non-prediction months for detect model
        """
        self.meanDetectRain = np.zeros(12)
        for i in range(1, 13):
            monMask = self.datMonth == i
            totMask = monMask & self.nonPredMask
            self.meanDetectRain[(i - 1)] = np.mean(self.datRain[totMask])
        # detect covariate array
        self.detectCovariatesMean = np.ones((12, 2))
        self.detectCovariatesMean[:, 1] = np.log(self.meanDetectRain + 1.0)
        # detect covariates for Rsq Model - real data, not means
        self.detectCovRsqModel = np.ones((self.cvNPred, 2))
        self.detectCovRsqModel[:, 1] = np.log(self.cvRainDat + 1.0)

    def getMeanTrapRateCV(self):
        """
        get mean trap rate for each calendar month of year
        """
        # trap rate for all non pred points
        trapSetTmp = self.datNSetTraps[self.nonPredMask] * 1.0
        trapSetTmp[trapSetTmp == 0.0] = np.nan
        self.nonPredTrapRate = (self.datNTrapped[self.nonPredMask] /  
            trapSetTmp)
#            self.nTrapSetAll[self.nonPredMask])
        self.nonPredHabIndx = self.datHabitat[self.nonPredMask]
        self.nonPredMonths = self.datMonth[self.nonPredMask]
        # empty array to populate with mean traprates for each calendar month 
        self.meanTrapRateCV = np.zeros(self.cvNPred)
        # Deviance using mean trap rate as theta
        self.devUseMeanTR = 0.0
        #### R-squared storage arrays
        self.Rsq_Forecast = np.zeros(self.iter)
        self.Rsq_Model = np.zeros(self.iter)
#        self.makeCVNgibbsIndx()
        # Denominator for r-sq predicted
        self.sumDiffMeanTrapRate = 0.0
        # loop thru cv points
        for j in range(self.cvNPred):
            month_j = self.cvMonthPred[j]
            habIndx_j = self.cvHabIndx[j]
            nonPredMask_j = ((self.nonPredMonths == month_j) & 
                (self.nonPredHabIndx == habIndx_j))
            self.meanTrapRateCV[j] = np.nanmean(self.nonPredTrapRate[nonPredMask_j])
            # calc deviance using mean trap rate of nonPred months
            devMeanTR_j = (stats.binom.logpmf(self.cvNTrapped[j], self.cvNTrapSet[j],
                self.meanTrapRateCV[j]))            
            # sum the deviance
            self.devUseMeanTR += devMeanTR_j
            # calc R-sq denominator
            denomDiff = (self.meanTrapRateCV[j] - self.cvTrapRate[j])**2
            self.sumDiffMeanTrapRate += denomDiff
        self.devUseMeanTR = -2.0 * self.devUseMeanTR

    def loopIter(self):
        """
        Create arrays and loop thru iterations
        """
        seqTmp = np.arange(self.nDat)
        self.cvIndx = seqTmp[self.cvPointMask]
        self.NForecast = np.zeros((self.iter, self.cvNPred))   # 3-d storage array N
#        self.NCaptured = np.zeros((self.nRandCaptures, self.iter, self.cvNPred), dtype = int)   # 3-d storage array captureed
        self.NCaptured = np.zeros((self.iter, self.cvNPred), dtype = int)   # 3-d storage array captureed
        self.captDeviance = np.empty((self.iter, 3))
        for i in range(self.iter):
            # draw random varitates
            self.drawVariates(i)
            self.loopMonths(i)
            self.calcModelRsq(i)        # calc model Rsq


    def drawVariates(self, i):
        """
        draw random variates for iteration i from gibbs
        """
        self.variate_i = self.variateIndx[i]
#        self.xt_1 = np.log(self.startN[self.variate_i])
#        self.a_i = self.agibbs[self.variate_i]
        self.b_i = self.bgibbs[self.variate_i]        
        self.alpha_i = self.alphagibbs[self.variate_i]
        self.g_i = self.ggibbs[self.variate_i]

    def loopMonths(self, i):
        """
        loop thru months j.
        """
        # storage array for deviance to add up across pred months
        self.devianceAdd = np.zeros(3)
        self.sumRsq_Forecast = 0.0
        for j in range(self.cvNPred):
            # initial data and N
#            cvNgibbs_j = self.cvNgibbsIndx[j]
            self.cv_j = self.cvIndx[j]                  # seq number to nDat
            self.hab_j = self.datHabitat[self.cv_j]
            self.mon_j = self.datMonth[self.cv_j]
            indxStart = self.cv_j - self.monthsBack
#            self.xt_1 = np.log(self.Ngibbs[self.variate_i, (cvNgibbs_j - self.monthsBack)])
#            Nt_1 = self.Ngibbs[self.variate_i, indxStart]
#            Nt_2 = self.Ngibbs[self.variate_i, (indxStart - 1)]
#            if self.modelNumber == 3:
#                logitRho_1 = self.logit_rhogibbs[self.variate_i, indxStart]
#                logitRho_2 = self.logit_rhogibbs[self.variate_i, indxStart - 1]               
            # loop thru months Back to get to predicted month
            if self.monthsBack > 1:
                for k in range(self.monthsBack):
                    predIndx = indxStart + k + 1    # predIndx is t-1; one before predicted pt.
                    t_2Indx = indxStart - 1 + k
                    t_1Indx = indxStart + k
                    if self.modelNumber == 3:
                        if t_1Indx <= indxStart:
                            rho_2 = self.Rho2D[self.variate_i, t_2Indx] 
                            Nt_1 = self.Ngibbs[self.variate_i, t_1Indx]
                            Nt_2 = self.Ngibbs[self.variate_i, t_2Indx]
                            nRec = Nt_2 / 2.0 * rho_2 * self.nFetusPerPreg
                            St = np.exp(-Nt_1 * self.b_i[self.hab_j])
                            Nt = (Nt_1 + nRec) * St
                        elif t_2Indx == indxStart:
                            rho_2 = self.Rho2D[self.variate_i, t_2Indx]
                            Nt_2 = self.Ngibbs[self.variate_i, t_2Indx]
                            Nt_1 = Nt
                            nRec = Nt_2 / 2.0 * rho_2 * self.nFetusPerPreg
                            St = np.exp(-Nt_1 * self.b_i[self.hab_j])
                            Nt = (Nt_1 + nRec) * St
                        elif t_2Indx == (indxStart + 1):
                            Nt_2 = Nt_1
                            Nt_1 = Nt
                        else:
                            Nt_2 = Nt_1
                            Nt_1 = Nt
                            t_3Indx = t_2Indx - 1
                            t_4Indx = t_2Indx - 2
                            tArray_2 = self.tArray[t_2Indx]
                            lrho_3 = self.logit_rhogibbs[self.variate_i, t_3Indx]
                            lrho_4 = self.logit_rhogibbs[self.variate_i, t_4Indx]
                            tArray_2[-2] = lrho_3
                            tArray_2[-1] = lrho_4
                            logitRho = np.dot(tArray_2, self.alpha_i)
                            nRec = Nt_2 / 2.0 * inv_logit(logitRho) * self.nFetusPerPreg
                            St = np.exp(-Nt_1 * self.b_i[self.hab_j])
                            Nt = (Nt_1 + nRec) * St
                    else:
                        # initial covariates for pop model
#                        self.getPredCovariates(t_2Indx, modelRsq = False)
                        tArray_2 = self.tArray[t_2Indx]
                        logitRho = np.dot(tArray_2, self.alpha_i)
                        rho = inv_logit(logitRho)
                        nRec = Nt_2 / 2.0 * rho * self.nFetusPerPreg
                        St = np.exp(-Nt_1 * self.b_i[self.hab_j])
                        Nt = (Nt_1 + nRec) * St
#                        # if lag > 1 and don't have real data get meanRain data
#                        harmonics_j = np.dot(self.tArray_j, self.alpha_i)
#                        mu = self.xt_1 + (self.b_i[self.hab_j] * self.xt_1) + harmonics_j
#                        nPred = np.exp(mu)
    #                    self.xt_1 = mu
                    if predIndx == self.cv_j:
                        self.NForecast[i, j] = Nt
                        # simulate the capture process
                        self.captureProcess(i, j)
        # populate Rsq Forecast array
        self.Rsq_Forecast[i] = 1. - (self.sumRsq_Forecast / self.sumDiffMeanTrapRate)  


    def calcModelRsq(self, i):
        """
        Calc Rsq predicting 1 month ahead and use known weather data
        """
        # loop cv points
        self.sumRsq_Model = 0.0
        self.rSQ_Model_Loop(i)

    def rSQ_Model_Loop(self, i):
        """
        loop thru cv points
        """
        for j in range(self.cvNPred):
            # initial data and N
#            cvNgibbs_j = self.cvNgibbsIndx[j]
            self.cv_j = self.cvIndx[j]
            hab_j = self.datHabitat[self.cv_j]
            t_2Indx = self.cv_j - 2
            t_1Indx = self.cv_j - 1
            Nt_1Model = self.Ngibbs[self.variate_i, t_1Indx]
            Nt_2Model = self.Ngibbs[self.variate_i, t_2Indx]
#            tArray_2 = self.tArray[(self.cv_j - 2)]
#            logitRho = np.dot(tArray_2, self.alpha_i)
#            rho = inv_logit(logitRho)
            rho_2 = self.Rho2D[self.variate_i, t_2Indx]
            nRec = Nt_2Model / 2.0 * rho_2 * self.nFetusPerPreg
            St = np.exp(-Nt_1Model * self.b_i[hab_j])
            NtModel = (Nt_1Model + nRec) * St
            detCov_j = self.detectCovRsqModel[j]
#            mon_j = self.monthsAll[self.cv_j]
            lambdaPred = np.exp(np.dot(detCov_j, self.g_i))
            expTerm = NtModel * lambdaPred
            th = 1.0 - np.exp(-expTerm)
            ### Calc R-squared for Model
            self.sumRsq_Model += (th - self.cvTrapRate[j])**2.0
        self.Rsq_Model[i] = 1.0 - (self.sumRsq_Model / self.sumDiffMeanTrapRate)
        
    def captureProcess(self, i, j):
        """
        # do random draw of captured individuals for Rsq_Forecast
        """
#        mon_j = self.datMonth[self.cv_j]
        detectCov_ij = self.detectCovariatesMean[(self.mon_j - 1)]
#        detectCov_ij = self.detectCovariates[self.cv_j]
        self.lambdaPred = np.exp(np.dot(detectCov_ij, self.g_i))
        expTerm = self.NForecast[i, j] * self.lambdaPred
        theta = 1.0 - np.exp(-expTerm)
        ncapt = np.random.binomial(self.datNSetTraps[self.cv_j], theta)
###        ncapt = np.random.binomial(self.nTrapSetAll[self.cv_j], theta, size = self.nRandCaptures)
        self.NCaptured[i, j] = ncapt  
##        self.NCaptured[:, i, j] = ncapt  
        self.logLikCaptData(i, j, theta)
        ### Calc R-squared for forecast 
        self.sumRsq_Forecast += (theta - self.cvTrapRate[j])**2.0        
#        print('######## Finish capture Process')

    def logLikCaptData(self, i, j, theta):
        """
        calc log lik of observed capture data given parameters and estimated N
        """
        dev = stats.binom.logpmf(self.datNTrapped[self.cv_j], self.datNSetTraps[self.cv_j], theta)
        self.devianceAdd[self.hab_j] += dev
        if j == (self.cvNPred - 1):
            self.devianceAdd[2] = self.devianceAdd[0] + self.devianceAdd[1]
            self.captDeviance[i] = self.devianceAdd * -2.0
    
    def summaryStats(self):
        """
        get summary stats from simulation
        """
        # 2-d arrays with rows from mean and CI, columns for months.
        self.summaryN = np.zeros((2, 3, self.nCV)) 
        self.summaryCapt = np.zeros((2, 3, self.nCV))
        meanN = np.mean(self.NForecast, axis = 0)
        meanCapt = np.mean(self.NCaptured, axis = 0)
        for i in range(2):
            habMask = self.cvHabIndx == i
            self.summaryN[i, 0] = meanN[habMask]
            self.summaryCapt[i, 0] = meanCapt[habMask]
            NQuants = mquantiles(self.NForecast[:, habMask], prob=[0.005, 0.995], axis = 0)

            self.summaryN[i, 1:] = NQuants
            self.summaryCapt[i, 1:] = mquantiles(self.NCaptured[:, habMask], prob=[0.005, 0.995], axis = 0)
#            self.getCaptureQuants(i, habMask)

    def getCaptureQuants(self, i, habMask):
        """
        loop through prediction points to get CIs
        """
        nCapt_hab = self.NCaptured[:, :, habMask]
        for m in range(self.nCV):
            nCapt_m = nCapt_hab[:, :, m]            
            self.summaryCapt[i, 1:, m] = mquantiles(nCapt_m, prob = [0.005, 0.995])

    def plotForecast(self):
        """
        plot simulation results
        """
        minDate = np.min(self.cvDates) - datetime.timedelta(days = 120)
        maxDate = np.max(self.cvDates) + datetime.timedelta(days = 120)
        P.figure(figsize=(14, 11))
        for i in range(2):
            meanN = self.summaryN[i, 0]
            nLowCI = self.summaryN[i, 1]
            nHighCI = self.summaryN[i, 2]
            meanCapt = self.summaryCapt[i, 0]
            captLowCI = self.summaryCapt[i, 1]
            captHighCI = self.summaryCapt[i, 2]
            nTrapped = self.cvNTrapped[self.cvHabIndx == i]
            ax1 = P.subplot(2, 1, i + 1)
            lns1 = ax1.plot(self.cvDates, meanN, 'ko', label = 'Forecast pop. density', ms = 8)
            lns3 = ax1.plot(self.cvDates, captLowCI, 'b_', label = 'Forecast capture 95% CI', ms = 24)
            lns4 = ax1.plot(self.cvDates, captHighCI, 'b_', ms = 24)                        
            lns5 = ax1.plot(self.cvDates, nTrapped, 'ro', label = 'Mice captured', ms = 8)
            lns = lns1 + lns3 + lns5 
            labs = [l.get_label() for l in lns]
            if i == 0:
                lTit = 'Lowland'
                ax1.legend(lns, labs, title = lTit, loc = 'upper left')
            else:
                lTit = 'Upland'
                ax1.legend(lns, labs, title = lTit, loc = 'upper left')
            ax1.set_ylim([0, (np.max(meanN) + 30)])
            ax1.set_xlim(minDate, maxDate)
            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            for tick in ax1.yaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            ax1.set_ylabel('Population and captures', fontsize = 17)
            ax1.set_xlabel('Time', fontsize = 17)
    #        P.xticks([datetime.date(2009, 1, 1), datetime.date(2011, 1, 1), datetime.date(2013, 1, 1),
    #                    datetime.date(2015, 1, 1), datetime.date(2017, 1, 1), datetime.date(2019, 1, 1)])
        figFname = ('forecast' + self.modelName + '_Back' + str(self.monthsBack) + '.png')
        print('figure name', figFname)
        fnamePath = os.path.join(self.outputDataPath, figFname)
        P.savefig(fnamePath, format='png')
        P.show()

    #############################
    ####    CV R-sq of model
    ####    Use known weather data and predict trap rate




class DeviancePickle(object):
    def __init__(self, predictobj):
        """
        obj to pickle deviance calculations
        """
        self.captDeviance = predictobj.captDeviance
        self.quantsDev = mquantiles(self.captDeviance, prob=[0.025, 0.975], axis = 0)
        self.meanDev = np.mean(self.captDeviance, axis = 0)
#        print('Quantiles', self.quantsDev, 'Mean =', self.meanDev)
        # make array and save text file to directory
        self.predictQuants = np.zeros((3,5))
        self.predictQuants[:2, :3] = self.quantsDev
        self.predictQuants[2, :3] = self.meanDev 
#        print('quantsMean', self.predictQuants)
        # Rsq results
        self.Rsq_Forecast = predictobj.Rsq_Forecast        
        self.quants_RsqForecast = mquantiles(self.Rsq_Forecast, prob=[0.025, 0.975])
        self.mean_RsqForecast = np.mean(self.Rsq_Forecast)
#        print('meanRSqForecast', self.mean_RsqForecast, 
#            'quants Rsq Forecast', self.quants_RsqForecast)
        # model Rsq results
        self.Rsq_Model = predictobj.Rsq_Model
        self.quants_RsqModel = mquantiles(self.Rsq_Model, prob=[0.025, 0.975])
        self.mean_RsqModel = np.mean(self.Rsq_Model)
#        print('meanRSqModel', self.mean_RsqModel, 'quants RsqModel', self.quants_RsqModel)
        self.predictQuants[:2, 3] = self.quants_RsqForecast
        self.predictQuants[2, 3] = self.mean_RsqForecast
        self.predictQuants[:2, 4] = self.quants_RsqModel
        self.predictQuants[2, 4] = self.mean_RsqModel
        stats = ['2.5 CI', '97.5 CI', 'Mean']
        df = pd.DataFrame({'A) Stats' : stats ,
                        'B) Model R2' : self.predictQuants[:, 4], 
                        'C) Forecast R2' : self.predictQuants[:, 3]})
        print(df)
#        print(tabulate(df, headers='keys', tablefmt='psql'))
 #       print('quantsMean', self.predictQuants)
        # save text file
        fname = ('predQuant_' + predictobj.modelName + '_Back' + str(predictobj.monthsBack) + '.csv') 
        fnamePath = os.path.join(predictobj.outputDataPath, fname)
        print('deviance file name', fnamePath)
        np.savetxt(fnamePath, self.predictQuants, delimiter = ',')
 

def main():

    #####################
    modelNumber = 3
    modelName = 'Model' + str(modelNumber)      ##  SET THE MODEL NAME
    monthsBack = 2                              ##  SET NUMBER OF MONTHS AHEAD TO PREDICT
    #####################
    print('################################################')
    print('Model Name:', modelName)
    print('Months ahead predicting:', monthsBack)

    gibbsName = ('gibbs_' + modelName + '.pkl')
    outputDataPath = os.path.join(os.getenv('CHINAPROJDIR', default='.'), 
            'Results_' + modelName)

    inputGibbs = os.path.join(outputDataPath, gibbsName)
    fileobj = open(inputGibbs, 'rb')
    gibbsobj = pickle.load(fileobj)
    fileobj.close()

    predictobj = PredictProcessing(gibbsobj, monthsBack, outputDataPath)

    deviancepickle = DeviancePickle(predictobj)

    # pickle to directory the deviance
#    outDev = os.path.join(outputDataPath, makeparas.CovNames)
#    fileobj = open(outDev, 'wb')
#    pickle.dump(deviancepickle, fileobj)
#    fileobj.close()

if __name__ == '__main__':
    main()


