"""
This Python script contains parameters for forecasting striped field mouse
(Apodemus agrarius) abundance in agricultural fields in Yuqing County,
Guizhou, China.
"""

# Copyright (C) 2017 Dean Anderson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.



#!/usr/bin/env python

import numpy as np
import os

class Params(object):
    def __init__(self):
        """
        set prediction names for input and output pickle files
        """
        ########################
        ## number of iterations
        self.iter = 1000

        ## current Date [year, month], month is numeric (i.e. 1 = January and 12 = December)
        self.currentDate = [2015, 12]
        ## Year and month to forecast
        self.forecastDate = [2016, 1]

        ## Environmental model (covariates):
            ## options are Rain, Temp, Rain0Rice', tmp0Rice, vegTree
        self.covMod = 'tmp0Rice'       # Rain, Temp, 'RainTemp, Rain0Rice', tmp0Rice, vegTree

        ## Lagged months for weather data
            # options are [10,8], [9,7], [8,6], [7,5], [6,4], [5,3], [4,2], [3,1]
        self.laggedMonths = np.array([6,4])

        ## File names of forecasting data to read in
        ## rain and temperature data
        self.weatherFname = 'weatherForecast.csv'   # 'weatherForecastTemp3.csv'  
        ## crop data
        self.cropFname = 'cropForecast.csv'
        ## trap set data
        self.trapFname = 'trapForecast.csv'
        
        ## set paths to scripts and data
        self.inputDataPath = os.path.join(os.getenv('CHINAPROJDIR', default='.'))
        ## write results here
        self.outputDataPath = os.path.join(os.getenv('CHINAPROJDIR', default='.'))
        if not os.path.isdir(self.outputDataPath):
            os.mkdir(self.outputDataPath)





        ########################
        ########################


