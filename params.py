#!/usr/bin/env python

#import sys
#import defol
import numpy as np

class Params(object):
    def __init__(self):
        """
        Object to set initial parameters
        """
        ##############
        ###############      Initial values parameters for updating
        self.sigma = np.array([1.1, 1.2])
        self.sigmaPriors = np.array([0.1, 0.1])

        # Population model
#        self.a = np.array([2.4, 2.2])                       # base pop growth by season
        self.b = np.array([-.7, -.7])                     # density dependence by hab
        self.abPrior = np.array([0.0, np.sqrt(100)])
        self.ab_vinvert = np.array([[0.01]])
        self.ab_ppart = np.array([[0.0]])

        self.alpha = np.array([1.2, .021, -.003, -.17, -0.1, .01, -.01, -.12, -.36])       # .reshape(5,1) # sin cos harmonic parameters
        self.alpha_Prior = np.array([0.0, np.sqrt(100)])
        # normal alpha priors
        nalpha = np.shape(self.alpha)[0]
        diag = np.diagflat(np.tile(100, nalpha))
        self.alpha_vinvert = np.linalg.inv(diag)
        self.alpha_ppart = np.dot(self.alpha_vinvert, np.zeros(nalpha))

        # Detection model
        self.g = np.array([-6.50, .2])                # covariates on detection (Rain)
        self.n_gPara = len(self.g)
        # priors on rain coefficients
        self.gPrior = np.array([0.0, np.sqrt(100.)])
        self.ngcov = len(self.g)
        self.maxN =  600

        self.a_search = .01
        self.b_search = .05
        self.g_search = np.array([.05, .025])
        # no intercept
        self.alpha_search = np.array([.05, .075, .075, .075, 0.075, 0.02, 0.02, 0.075, 0.1])
        self.sigma_search = .1

        self.followSess = np.array([5, 23, 100, 174, 260, 341])
#        self.meanNgibbs = np.zeros((self.basicdata.nsessions, 2))
        self.nSample = np.arange(-10, 10, dtype = int)
        self.nSample = self.nSample[self.nSample != 0]

        ######################################
        ######################################
        # modify  variables used for population model
        self.laggedMonths = np.array([10,8])
        ###
        ################

#        # set prediction year
#        self.predictYear = 2013
        self.maskTrapDatName = ('self.maskT' + str(self.laggedMonths[0]) +
            '_' + str(self.laggedMonths[1]) + 'All')
        self.rainCovName = ('self.rainT' + str(self.laggedMonths[0]) +
            '_' + str(self.laggedMonths[1]) + 'All')
        self.tempCovName = ('self.tempT' + str(self.laggedMonths[0]) +
            '_' + str(self.laggedMonths[1]) + 'All')
        #########
        ## make output basicdata and gibbs pickle names
        self.covNames = 'Temp'     # Rain, Temp, Rain0Rice, vegTree, tmp0Rice
        #########

        self.gibbsName = ('gibbs' + self.covNames + str(self.laggedMonths[0]) + '_' +
            str(self.laggedMonths[1]) + '.pkl')
        self.basicdataName = ('basicdata' + self.covNames + str(self.laggedMonths[0]) + '_' +
            str(self.laggedMonths[1]) + '.pkl')

        ######################################
        ######################################
        # modify  variables used for detection model
        self.xdatDictionary = {'intercept' : 0, 'Rain' : 1, 'RainSq' : 2}
        self.intercept = self.xdatDictionary['intercept']
        self.Rain = self.xdatDictionary['Rain']
        self.RainSq = self.xdatDictionary['RainSq']
        # array of index values to get variables from 2-d array
        self.xdatIndx = np.array([self.intercept, self.Rain, self.RainSq], dtype = int)
        ######################################
        ######################################

        ###################################################
        ###################################################
        # Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 2500    # 2500      # number of estimates to save for each parameter
        self.thinrate = 110   # 200      # thin rate
        self.burnin = 10000       # burn in number of iterations
                                    # array of iteration id to keep
        self.keepseq = np.arange(self.burnin, ((self.ngibbs * self.thinrate) + self.burnin),
            self.thinrate)
        ###################################################
        ###################################################


