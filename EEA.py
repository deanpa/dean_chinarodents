#!/usr/bin/env python


import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import statsmodels.api as sm
import prettytable
import pickle
import os
import datetime
from processRawData import ApodemusData

###################################
#############################
#########################       HAVE TO SET THE MODEL NAME HERE

#from model1 import Params

#########################
############################
##################################


def logit(x):
    """
    Function (logit) to convert probability to real number
    """
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    """
    Function to convert real number to probability
    """
    return np.exp(x) / (1 + np.exp(x))


class ResultsProcessing(object):
    def __init__(self, apodemusdata, outputDataPath):

#        self.params = params
        self.apodemusdata = apodemusdata
        self.outputDataPath = outputDataPath
#        self.ndat = len(self.gibbsobj.siggibbs)

        ##############################################
        ## RUN FUNCTIONS
        self.getVariables(apodemusdata)
        self.getSummaryMeans()
        self.plotPredictPregRate()

        ## plot of correlations of preg rate
#        self.getCorrelations3_9()
#        resultsobj.getCorrelations4_5()
#        resultsobj.getCorrelations8_9()
#        self.getCorrelations4_5_8_9()
#        resultsobj.getCorrelations4_5_6_7()
#        resultsobj.getCorrelations6_7_8_9()

        self.regress4_9()

        ##############################################

                
    def getVariables(self, apodemusdata):
        """
        get variables from apodemusdata
        """
        # move apodemusdata variables into basicdata
        self.apodemusdata = apodemusdata
        # move variables
        self.nsessions = self.apodemusdata.nsessions      # n sessions per habitat
        self.datSessions = self.apodemusdata.datSessions
        self.datNTrapped = self.apodemusdata.datNTrapped
        self.datNSetTraps = self.apodemusdata.datNSetTraps
        self.datMonthRun = self.apodemusdata.datMonthRun  # from 13 up
        self.datMonth = self.apodemusdata.datMonth
        self.datYear = self.apodemusdata.datYear
        self.datHabitat = self.apodemusdata.datHabitat    # 0 = lowland and 1 = upland
        self.habMask0 = self.datHabitat == 0
        self.habMask1 = self.datHabitat == 1
        self.datStartMonth = apodemusdata.datStartMonth
        self.datMinMonthRun = apodemusdata.datMinMonthRun
        self.datMaxMonthRun = apodemusdata.datMaxMonthRun
        ## repro variables
        self.datNFemales = self.apodemusdata.datNFemales
        self.datNPregnant = self.apodemusdata.datNPregnant
        self.datPromTesticle = self.apodemusdata.datPromTesticle
        self.nDat = self.apodemusdata.nDat                # total data
        self.datRain = self.apodemusdata.datRain
        self.datTemperature = self.apodemusdata.datTemperature
        self.datRice = self.apodemusdata.datRice
        self.datRapeWheat = self.apodemusdata.datRapeWheat
        self.datVegetables = self.apodemusdata.datVegetables
        self.datTrees = self.apodemusdata.datTrees
        self.datDryCrop = self.apodemusdata.datDryCrop
        self.uDatYear = self.apodemusdata.uDatYear
        self.nDatYear = self.apodemusdata.nDatYear

    def getSummaryMeans(self):
        """
        get mean trap rate and summaries for Results
        """
        trapSetNAN = self.datNSetTraps.astype(float)
        trapSetNAN[trapSetNAN <= 0.0] = np.nan
        self.summaryTrapRate = self.datNTrapped / trapSetNAN
        self.meanMonthTrapRate = np.empty((12,6))
        self.meanMonthTrapRate[:,0] =  np.arange(1.,13.)
        for i in range(12):
            monthMask = self.datMonth == (i + 1)
            lowHabMonthMask = monthMask & self.habMask0
            highHabMonthMask = monthMask & self.habMask1
            # mean trap rate
            tr_i = self.summaryTrapRate[lowHabMonthMask] * 100.0
            tr_i_up = self.summaryTrapRate[highHabMonthMask] * 100.0
            self.meanMonthTrapRate[i,1] = np.nanmean(tr_i) 
            
            self.meanMonthTrapRate[i, 2] =  np.nanmean(tr_i_up)
            # traprate quantiles
#            self.meanMonthTrapRate[i, 2] =  np.nanpercentile(tr_i, 2.5)
            self.meanMonthTrapRate[i, 3] =  np.nanpercentile(tr_i, 97.5)
            # mean log rainfall 
            rain_i = self.datRain[monthMask]
            self.meanMonthTrapRate[i,4] = np.nanmean(rain_i)
#            self.meanMonthTrapRate[i,4] = np.log(np.nanmean(rain_i) + 1.0)
            temp_i = self.datTemperature[monthMask]
            # mean log temp
            self.meanMonthTrapRate[i,5] = np.nanmean(temp_i)
#            self.meanMonthTrapRate[i,5] = np.log(np.nanmean(temp_i) + 1.0)
#        print('meanMonthTrapRate', np.round(self.meanMonthTrapRate, 2))
        
    def plotPredictPregRate(self):
        """
        ## plot temperature and preg rates against calendar month
        ## plot predicted preg rates across temperature gradient
        """        
        self.getObsPregRate()
        P.figure(figsize=(11,9))
        monNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
            'Sep', 'Oct', 'Nov', 'Dec']
        ax1 = P.gca()
        mon = self.meanMonthTrapRate[:, 0]
        lns1 = ax1.plot(mon, self.meanMonthTrapRate[:, 5], color = 'k', 
            label = 'Mean temperature', linewidth = 3.0)
        lns5 = ax1.plot(mon, (self.meanMonthTrapRate[:, 4] / 10.0), color = 'k', 
            label = 'Mean rainfall', ls = '--',  linewidth = 2) 
        # second axis
        ax2 = ax1.twinx()
#        lns3 = ax2.plot(mon, self.pregRateCal[:, 0], color = 'r', 
#            label = 'Predicted pregnacy rate', linewidth = 3.0)
        lns2 = ax2.plot(mon, self.obserPregRate, color = 'b', 
            label = 'Observed pregnacy rate', linewidth = 3.0)
#        lns3 = ax2.plot(mon, self.pregRateCal[:, 1], color = 'b', label = 'Mean rainfall', linewidth = 3.0)
        lns = lns1 + lns5 + lns2
        labs = [l.get_label() for l in lns]
#        ax1.set_xlim(minDate, maxDate)
        for tick in ax1.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax1.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        ax1.set_xlabel('Months', fontsize = 17)
        ax1.set_ylabel('Mean temperature C$^\circ$', fontsize = 17)
        ax2.set_ylabel('Mean pregnancy rate', fontsize = 17)
        ymax0 = np.max(self.meanMonthTrapRate[:, 5]) + 5.0
        ax1.set_ylim([0.0, ymax0])
        ax2.set_ylim([0.0, 1.0])
        ax1.legend(lns, labs, loc = 'upper right')
        P.xticks(mon, monNames)
#        figFname = os.path.join(self.outputDataPath, 'predictedPregRate.png')
#        P.savefig(figFname, format='png', dpi = 1000)
#        P.show()                

    def getObsPregRate(self):
        """
        ## get predicted preg rates for temp gradient and calendar months
        """
        ## get preg rates for mean temps for calendar months
        self.obserPregRate = np.zeros(12)
        zeroFemsMask = self.datNFemales > 0
        for i in range(12):
            ## get observed preg rate distribution by month
            monMask = self.datMonth == (i + 1)
            mask_i = monMask & zeroFemsMask
            pr_i = np.sum(self.datNPregnant[mask_i]) / np.sum(self.datNFemales[mask_i])
            self.obserPregRate[i] = pr_i     # np.mean(pr_i)


    def plotMeanTraprate_RainTemp(self):
        """
        plot monthly means of traprates,rainfall and temperature
        """
        P.figure(figsize=(11,9))
        monNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
            'Sep', 'Oct', 'Nov', 'Dec']
        ax1 = P.gca()
        mon = self.meanMonthTrapRate[:, 0]
        lns1 = ax1.plot(mon, self.meanMonthTrapRate[:, 1], color = 'k', label = 'Mean lowland trap rate', linewidth = 3)
        lns2 = ax1.plot(mon, self.meanMonthTrapRate[:, 2], color = 'k', 
                ls = '--', label = 'Mean upland trap rate', linewidth = 2.5)
#        lns2 = ax1.plot(mon, self.meanMonthTrapRate[:, 2], color = 'k', 
#                ls = '--', label = '95% CI trap rate', linewidth = 1.0)
#        lns3 = ax1.plot(mon, self.meanMonthTrapRate[:, 3], color = 'k', ls = '--', linewidth = 1.0)
        lns5 = ax1.plot(mon, self.meanMonthTrapRate[:, 5], color = 'r', label = 'Mean temperature', linewidth = 1.0)
        # second axis
        ax2 = ax1.twinx()
        lns4 = ax2.plot(mon, self.meanMonthTrapRate[:, 4], color = 'b', label = 'Mean rainfall', linewidth = 1.0)

        lns = lns1 + lns2 + lns5 + lns4
        labs = [l.get_label() for l in lns]
#        ax1.set_xlim(minDate, maxDate)
        for tick in ax1.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax1.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        ax1.set_xlabel('Months', fontsize = 17)
        ax2.set_ylabel('Mean rainfall (mm)', fontsize = 17)
        ymax = np.max(self.meanMonthTrapRate[:, 3])
        ax1.set_ylabel('Mean trap rate & temperature', fontsize = 17)
        ax1.set_ylim([-1.0, ymax])
        ax1.legend(lns, labs, loc = 'upper right')
        P.xticks(mon, monNames)
        figFname = 'monthMeanTR_rainTmp.png'
        P.savefig(figFname, format='png', dpi = 1000)
        P.show()                


    def getCorrelations3_9(self):
        """
        ## get correlations of pregrates
        """
        monSeq = np.arange(4, 10)       # April - Sept
        nMon = len(monSeq)
        nCorr = self.nDatYear * nMon
        keepMask = np.zeros(nCorr, dtype = bool)
        self.t_1MarSept = np.zeros((nCorr, 2))
        cc = 0
        for i in self.uDatYear:
            yrMask = self.datYear == i
            for j in monSeq:
                m0 = (self.datMonth == (j - 2)) & yrMask
                m1 = (self.datMonth == j) & yrMask
                nfem0 = np.sum(self.datNFemales[m0])
                nfem1 = np.sum(self.datNFemales[m1])
                if (nfem0 > 0) & (nfem1 > 0):
                    npreg0 = np.sum(self.datNPregnant[m0])
                    npreg1 = np.sum(self.datNPregnant[m1])
                    self.t_1MarSept[cc, 0] = npreg0 / nfem0 
                    self.t_1MarSept[cc, 1] = npreg1 / nfem1
                    keepMask[cc] = 1
                cc += 1
        self.t_1MarSept = self.t_1MarSept[keepMask]
        print('corr t_2 Mar-Sept', np.corrcoef(self.t_1MarSept[:, 0], self.t_1MarSept[:, 1]))
        P.figure()
        P.scatter(self.t_1MarSept[:, 0], self.t_1MarSept[:, 1])
        P.show()


    def regress4_9(self):
        """
        ## get correlations of pregrates
        """
        monSeq = np.arange(3, 13)       # April - Sept
        nMon = len(monSeq)
        nCorr = self.nDatYear * nMon
        keepMask = np.zeros(nCorr, dtype = bool)
        self.regr4_9= np.ones((nCorr, 5))
        self.pr4_9 = np.zeros(nCorr)
        self.trapRate0 = np.zeros(nCorr)
        self.pr_2 = np.zeros(nCorr)
        self.regTrRate = np.ones((nCorr, 2))
#        feb_AprMask = np.in1d(self.datMonth, [2,3,4])
#        feb_AprMask = np.in1d(self.datMonth, [2,3])
        feb_AprMask = np.in1d(self.datMonth, [3,4])         # best for Deng regression
        seqDeng = [4,5,6,7]
        cc = 0
        for i in self.uDatYear:
            yrMask = self.datYear == i
            habMask = self.datHabitat == 0
            yrHabMask = yrMask & habMask
            ## Deng's Feb - Apr temperature
            febAprHabMask = yrHabMask & feb_AprMask
            temp_ij = np.mean(self.datTemperature[febAprHabMask])

            for k in monSeq:
                m2 = (self.datMonth == (k - 2)) & yrMask
                m1 = (self.datMonth == (k - 1)) & yrMask
                m0 = (self.datMonth == k) & yrMask
                nfem0 = np.sum(self.datNFemales[m0])
                nfem1 = np.sum(self.datNFemales[m1])
                nfem2 = np.sum(self.datNFemales[m2])
                if (nfem0 > 0) & (nfem1 > 0) & (nfem2 > 0):
                    npreg0 = np.sum(self.datNPregnant[m0])
                    npreg1 = np.sum(self.datNPregnant[m1])
                    npreg2 = np.sum(self.datNPregnant[m2])
                    tempDat0 = self.datTemperature[m0]
                    tempDat1 = self.datTemperature[m1]
                    tempDat2 = self.datTemperature[m2]
                    rainDat0 = self.datRain[m0]
                    rainDat1 = self.datRain[m1]
                    rainDat2 = self.datRain[m2]
                    self.pr4_9[cc] = npreg0 / nfem0 
                    self.regr4_9[cc, 1] = npreg1 / nfem1
                    self.regr4_9[cc, 2] = npreg2 / nfem2
                    self.trapRate0[cc] = (self.datNTrapped[m0] / self.datNSetTraps[m0])[0]
                    pr_2 = npreg2 / nfem2
                    pr_1 = npreg1 / nfem1
                    ######################################
                    ## Good polynomial model
#                    self.regr4_9[cc, 3] = np.log(tempDat0[0] + 1.) 
#                    self.regr4_9[cc, 4] = (np.log(tempDat0[0] + 1.))**2.0
##                    self.regr4_9[cc, 3] = np.log(rainDat0[0] + 1.) 
##                    self.regr4_9[cc, 4] = (np.log(rainDat0[0] + 1.))**2.0

#                    self.regr4_9[cc, 3] = np.log(tempDat2[0] + 1.)
#                    self.regr4_9[cc, 4] = np.log(tempDat0[0] + 1.)
                    self.regr4_9[cc, 3] = np.log(tempDat0[0] + 1.)
                    self.regr4_9[cc, 4] = (np.log(tempDat0[0] + 1.))**2.0
#                    self.regr4_9[cc, 5] = np.log(rainDat0[0] + 1.) 
#                    self.regr4_9[cc, 5] = np.log(tempDat0[0] + 1.)**2.0

#                   self.regr4_9[cc, 3] = (np.log(tempDat1[0] + 1.) + np.log(tempDat2[0] + 1.)) / 2.0
#                    self.regr4_9[cc, 4] = (np.log(tempDat0[0] + 1.))**2.0


                    #### traprate regression
                    self.regTrRate[cc, 1] = pr_1 + pr_2
#                    self.regTrRate[cc, 2] = np.log(rainDat0[0] + 1.)
                    ##################################################

#                    self.regr4_9[cc, 4] = np.log(rainDat0[0] + 1.)
#                    self.regr4_9[cc, 5] = np.log(rainDat0[0] + 1.)**2


#                    if np.in1d(k, seqDeng):
#                        self.regr4_9[cc, 3] = np.log(tempDat0[0] + 1.) 
#                        self.regr4_9[cc, 4] = (np.log(tempDat0[0] + 1.))**2.0
#                    else:
#                        self.regr4_9[cc, 3] = 0.0 
#                        self.regr4_9[cc, 4] = 0.0

#                    if np.in1d(k, seqDeng):
#                        self.regr4_9[cc, 3] = (np.log(tempDat0[0] +
#                            tempDat1[0]) / 2.0) 




#                    self.regr4_9[cc, 3] = np.log(tempDat1[0] + 1.0)

#                    self.regr4_9[cc, 3] = (np.log(tempDat0[0] +
#                        tempDat1[0]) / 2.0) 

#                    self.regr4_9[cc, 3] = ((self.datTemperature[m0] +
#                        self.datTemperature[m1] +
#                        self.datTemperature[m2]) / 3.0) 

#                    self.regr4_9[cc, 3] = ((self.datTemperature[m1] +
#                        self.datTemperature[m2]) / 2.0) 

#                    self.regr4_9[cc, 3] = ((np.log(self.datTemperature[m0] + .1)) +
#                        (np.log(self.datTemperature[m1] + .1)) +
#                        (np.log(self.datTemperature[m2] + .1)) / 3.0) 

                    #################################
                    ## Deng's model
#                    self.regr4_9[cc, 3] = temp_ij 
#                    if np.in1d(k, seqDeng):
#                        self.regr4_9[cc, 3] = temp_ij
#                    else:
#                        self.regr4_9[cc, 3] = 0.0
                    #################################
                       
                    keepMask[cc] = 1
                    cc += 1
#        print('reg data', self.regr4_9[:60])
        self.regr4_9 = self.regr4_9[keepMask]
        self.pr4_9 = self.pr4_9[keepMask]
        print('corr', np.corrcoef(self.regr4_9[:, 2], self.pr4_9))
#        P.figure()
#        P.scatter(self.regr4_9[:, 2], self.pr4_9)
#        P.show()

        glm_binom = sm.GLM(self.pr4_9, self.regr4_9, family=sm.families.Binomial())
        res = glm_binom.fit()
        print('Preg Rate GLM Results', res.summary())


###        glm_binom = sm.GLM(self.trapRate0, self.regTrRate, family=sm.families.Binomial())
###        res = glm_binom.fit()
###        print('Trap Rate GLM Results', res.summary())

#        model = sm.OLS(self.pr4_9, self.regr4_9)
#        results = model.fit()
#        print('Regression Results', results.summary())


    def regress4_9Hab2(self):
        """
        ## get correlations of pregrates
        """
        monSeq = np.arange(4, 10)       # April - Sept
        nMon = len(monSeq)
        nCorr = self.nDatYear * nMon * 2
        keepMask = np.zeros(nCorr, dtype = bool)
        self.regr4_9= np.ones((nCorr, 5))
        self.pr4_9 = np.zeros(nCorr)
        feb_AprMask = np.in1d(self.datMonth, [2,3,4])
        cc = 0
        for i in self.uDatYear:
            yrMask = self.datYear == i
            for j in range(2):
                habMask = self.datHabitat == j
                yrHabMask = yrMask & habMask
                febAprHabMask = yrHabMask & feb_AprMask
                temp_ij = np.sum(self.datTemperature[febAprHabMask])
                for k in monSeq:
                    m2 = (self.datMonth == (k - 2)) & yrHabMask
                    m1 = (self.datMonth == (k - 1)) & yrHabMask
                    m0 = (self.datMonth == k) & yrHabMask
                    nfem0 = (self.datNFemales[m0])
                    nfem1 = (self.datNFemales[m1])
                    nfem2 = (self.datNFemales[m1])
                    if (nfem0 > 0) & (nfem1 > 0) & (nfem2 > 0):
                        npreg0 = (self.datNPregnant[m0])
                        npreg1 = (self.datNPregnant[m1])
                        npreg2 = (self.datNPregnant[m2])
                        self.pr4_9[cc] = npreg0 / nfem0 
                        self.regr4_9[cc, 1] = npreg1 / nfem1
                        self.regr4_9[cc, 2] = npreg2 / nfem2
                        self.regr4_9[cc, 3] = np.log(self.datTemperature[m0] + .1) 
                        self.regr4_9[cc, 4] = (np.log(self.datTemperature[m0] + .1))**2.0
#                        self.regr4_9[cc, 4] = self.datTemperature[m1]

#                        self.regr4_9[cc, 3] = (np.log(self.datTemperature[m0] +
#                            self.datTemperature[m1]) / 2.0) 

#                        self.regr4_9[cc, 3] = ((self.datTemperature[m0] +
#                            self.datTemperature[m1] +
#                            self.datTemperature[m2]) / 3.0) 

#                        self.regr4_9[cc, 3] = ((self.datTemperature[m1] +
#                            self.datTemperature[m2]) / 2.0) 

#                        self.regr4_9[cc, 3] = ((np.log(self.datTemperature[m0] + .1)) +
#                            (np.log(self.datTemperature[m1] + .1)) +
#                            (np.log(self.datTemperature[m2] + .1)) / 3.0) 
#                        self.regr4_9[cc, 3] = temp_ij 
                        
                        keepMask[cc] = 1
                    cc += 1
#        print('reg data', self.regr4_9[:60])
        self.regr4_9 = self.regr4_9[keepMask]
        self.pr4_9 = self.pr4_9[keepMask]
        print('corr', np.corrcoef(self.regr4_9[:, 2], self.pr4_9))
#        P.figure()
#        P.scatter(self.regr4_9[:, 2], self.pr4_9)
#        P.show()
        model = sm.OLS(self.pr4_9, self.regr4_9)
        results = model.fit()
        print('Regression Results', results.summary())





    def getCorrelations4_5(self):
        """
        ## get correlations of pregrates
        """
        nMon = 1
        nCorr = self.nDatYear * nMon
        keepMask45 = np.zeros(nCorr, dtype = bool)
        self.cor45 = np.zeros((nCorr, 2))
        cc = 0
        for i in self.uDatYear:
            yrMask = self.datYear == i
            m4 = (self.datMonth == 4) & yrMask
            m5 = (self.datMonth == 5) & yrMask
            nfem4 = np.sum(self.datNFemales[m4])
            nfem5 = np.sum(self.datNFemales[m5])
            if (nfem4 > 0) & (nfem5 > 0):
                npreg4 = np.sum(self.datNPregnant[m4])
                npreg5 = np.sum(self.datNPregnant[m5])
                self.cor45[cc, 0] = npreg4 / nfem4 
                self.cor45[cc, 1] = npreg5 / nfem5
                keepMask45[cc] = 1
                cc += 1
        self.cor45 = self.cor45[keepMask45]
        print('corr 4_5', np.corrcoef(self.cor45[:, 0], self.cor45[:, 1]))
        P.figure()
        P.scatter(self.cor45[:, 0], self.cor45[:, 1])
        P.show()

    def getCorrelations8_9(self):
        """
        ## get correlations of pregrates
        """
        nMon = 1
        nCorr = self.nDatYear * nMon
        keepMask89 = np.zeros(nCorr, dtype = bool)
        self.cor89 = np.zeros((nCorr, 2))
        cc = 0
        for i in self.uDatYear:
            yrMask = self.datYear == i
            m8 = (self.datMonth == 8) & yrMask
            m9 = (self.datMonth == 9) & yrMask
            nfem8 = np.sum(self.datNFemales[m8])
            nfem9 = np.sum(self.datNFemales[m9])
            if (nfem8 > 0) & (nfem9 > 0):
                npreg8 = np.sum(self.datNPregnant[m8])
                npreg9 = np.sum(self.datNPregnant[m9])
                self.cor89[cc, 0] = npreg8 / nfem8 
                self.cor89[cc, 1] = npreg9 / nfem9
                keepMask89[cc] = 1
                cc += 1
        self.cor89 = self.cor89[keepMask89]
        print('corr 8_9', np.corrcoef(self.cor89[:, 0], self.cor89[:, 1]))
        P.figure()
        P.scatter(self.cor89[:, 0], self.cor89[:, 1])
        P.show()

    def getCorrelations4_5_8_9(self):
        nMon = 1
        nCorr = self.nDatYear * nMon
        keepMask4589 = np.zeros(nCorr, dtype = bool)
        self.cor4589 = np.zeros((nCorr, 2))
        cc = 0
        i45 = [4,5]
        i89 = [8,9]
        for i in self.uDatYear:
            yrMask = self.datYear == i
            m45 = (np.in1d(self.datMonth, i45)) & yrMask
            m89 = (np.in1d(self.datMonth, i89)) & yrMask
            nfem45 = np.sum(self.datNFemales[m45])
            nfem89 = np.sum(self.datNFemales[m89])
            if (nfem45 > 0) & (nfem89 > 0):
                npreg45 = np.sum(self.datNPregnant[m45])
                npreg89 = np.sum(self.datNPregnant[m89])
                self.cor4589[cc, 0] = npreg45 / nfem45 
                self.cor4589[cc, 1] = npreg89 / nfem89
                keepMask4589[cc] = 1
                cc += 1
        self.cor4589 = self.cor4589[keepMask4589]
        print('corr 45_89', np.corrcoef(self.cor4589[:, 0], self.cor4589[:, 1]))
        P.figure()
        P.scatter(self.cor4589[:, 0], self.cor4589[:, 1])
        P.show()


    def getCorrelations4_5_6_7(self):
        nMon = 1
        nCorr = self.nDatYear * nMon
        keepMask4567 = np.zeros(nCorr, dtype = bool)
        self.cor4567 = np.zeros((nCorr, 2))
        cc = 0
        i45 = [4,5]
        i67 = [6,7]
        for i in self.uDatYear:
            yrMask = self.datYear == i
            m45 = (np.in1d(self.datMonth, i45)) & yrMask
            m67 = (np.in1d(self.datMonth, i67)) & yrMask
            nfem45 = np.sum(self.datNFemales[m45])
            nfem67 = np.sum(self.datNFemales[m67])
            if (nfem45 > 0) & (nfem67 > 0):
                npreg45 = np.sum(self.datNPregnant[m45])
                npreg67 = np.sum(self.datNPregnant[m67])
                self.cor4567[cc, 0] = npreg45 / nfem45 
                self.cor4567[cc, 1] = npreg67 / nfem67
                keepMask4567[cc] = 1
                cc += 1
        self.cor4567 = self.cor4567[keepMask4567]
        print('corr 45_67', np.corrcoef(self.cor4567[:, 0], self.cor4567[:, 1]))
        P.figure()
        P.scatter(self.cor4567[:, 0], self.cor4567[:, 1])
        P.show()


    def getCorrelations6_7_8_9(self):
        nMon = 1
        nCorr = self.nDatYear * nMon
        keepMask6789 = np.zeros(nCorr, dtype = bool)
        self.cor6789 = np.zeros((nCorr, 2))
        cc = 0
        i67 = [4,5]
        i89 = [8,9]
        for i in self.uDatYear:
            yrMask = self.datYear == i
            m67 = (np.in1d(self.datMonth, i67)) & yrMask
            m89 = (np.in1d(self.datMonth, i89)) & yrMask
            nfem67 = np.sum(self.datNFemales[m67])
            nfem89 = np.sum(self.datNFemales[m89])
            if (nfem67 > 0) & (nfem89 > 0):
                npreg67 = np.sum(self.datNPregnant[m67])
                npreg89 = np.sum(self.datNPregnant[m89])
                self.cor6789[cc, 0] = npreg67 / nfem67 
                self.cor6789[cc, 1] = npreg89 / nfem89
                keepMask6789[cc] = 1
                cc += 1
        self.cor6789 = self.cor6789[keepMask6789]
        print('corr 67_89', np.corrcoef(self.cor6789[:, 0], self.cor6789[:, 1]))
        P.figure()
        P.scatter(self.cor6789[:, 0], self.cor6789[:, 1])
        P.show()

  
    def regress3_4(self):
        """
        ## get correlations of pregrates
        """
        nMon = 2
        nCorr = self.nDatYear * nMon
        keepMask34 = np.zeros(nCorr, dtype = bool)
        self.pr34 = np.zeros(nCorr)
        self.reg34 = np.ones((nCorr, 4))
        cc = 0
        for i in self.uDatYear:
            yrMask = self.datYear == i
            m3 = (self.datMonth == 3) & yrMask
            m4 = (self.datMonth == 4) & yrMask
            nfem3 = np.sum(self.datNFemales[m3])
            nfem4 = np.sum(self.datNFemales[m4])
            if (nfem3 > 0) & (nfem4 > 0):
                npreg3 = np.sum(self.datNPregnant[m3])
                npreg4 = np.sum(self.datNPregnant[m4])
                temp4 = self.datTemperature[m3]
                self.reg34[cc, 1] = npreg3 / nfem3
                self.reg34[cc, 2] = 222.2
                self.pr34[cc] = npreg4 / nfem4
                keepMask34[cc] = 1
                cc += 1
        self.reg34 = self.reg34[keepMask34]
        self.pr34 = self.pr34[keepMask34]
        print('regr 4_5', np.corrcoef(self.reg34[:, 0], self.reg34[:, 1]))
        model = sm.OLS(pr34, self.reg34)
        results = model.fit()
        print('Regression Results', results.summary())
        P.figure()
        P.scatter(self.reg34[:, 0], self.reg34[:, 1])
        P.show()
          

    def getTrapNAN(self):
        """
        get trap data with nan inserted
        """
        self.trapRate = self.nTrapped.copy()
        zeroTrapMask = self.nTrapSet == 0
        trapMask = self.nTrapSet > 0
        self.trapRate[trapMask] = self.nTrapped[trapMask] / self.nTrapSet[trapMask] * 100.0
        self.trapRate = self.trapRate.astype(np.float64)
        self.trapRate[zeroTrapMask] = np.nan
#        self.trapRate = self.trapRate.astype(np.float64)
 

    @staticmethod    
    def quantileFX(a):
        return mquantiles(a, prob=[0.025, 0.5, 0.975], axis = 0)

    def getDates(self):
        """
        make dates array
        """
        self.dates = []
        mmm = self.months[self.habMask0]
        yyy = self.years[self.habMask0]
        for month, year in zip(mmm, yyy):
            date = datetime.date(int(year), int(month), 1)
            self.dates.append(date)




    def plotMakeFigures(self):
        """
        plot growth rate over time
        """
#        minDate = datetime.date(2003, 1, 1)
#        maxDate = datetime.date(2007, 12, 31)
#        minDate = datetime.date(1990, 1, 1)
#        maxDate = datetime.date(2010, 12, 31)
        minDate = datetime.date(1987, 1, 1)
        maxDate = datetime.date(2015, 12, 1)
        P.figure(figsize=(14, 11))
        for i in range(2):
            mask_i = self.habIndx == i
            ax1 = P.subplot(2, 1, i + 1)
#            pGrowth = self.pGrowth[mask_i]
#            harmonics = self.harmonics[mask_i]
            rain_i = self.rainDat[mask_i]
            rainT_1 = self.Raint_1[mask_i] 
            npred_i = self.nPred[mask_i]
            tRate_i = self.trapRate[mask_i]
#            pRice = self.tArray[mask_i, 7]
#            pVeg = self.tArray[mask_i, 8]
#            pTrees = self.tArray[mask_i, 9]
#            pRice = self.tArray[mask_i,6 ]
            pVeg = self.tArray[mask_i, 6]
            pTrees = self.tArray[mask_i, 7]
#            lns1 = ax1.plot(self.dates, npred_i, color = 'k', label = 'Population size', linewidth = 3.0)
#            lns1 = ax1.plot(self.dates, tRate_i, color = 'k', label = 'Trap rate (x100)', linewidth = 2.0)
            lns1 = ax1.plot(self.dates, tRate_i, color = 'k', linewidth = 2.0)
            
###            ax2 = ax1.twinx()
###            lns2 = ax2.plot(self.dates, rain_i, color = 'b', label = 'Rainfall (mm)', linewidth = 1.0)
#            lns3 = ax2.plot(self.dates, pRice , color = 'y', label = '%Rice', linewidth = 2.0)
#            lns4 = ax2.plot(self.dates, pVeg , color = 'r', label = '%Veg', linewidth = 2.0)
#            lns5 = ax2.plot(self.dates, pTrees , color = 'g', label = 'Trees crop', linewidth = 4.0)
###            lns = lns1 # + lns2
###            labs = [l.get_label() for l in lns]
###            if i == 0:
###                lTit = 'Lowland'
###                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
###            else:
###                lTit = 'Upland'
###                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            ax1.set_ylim(0.0, 35.0)
            ax1.set_xlim(minDate, maxDate)
#            ax2.set_ylim([-0.02, 1.05])
###            ax2.set_ylim([0.0, 400.0])

            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            for tick in ax1.yaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            ax1.set_xlabel('Time', fontsize = 17)
            ax1.set_ylabel('Monthly trap rate (x 100)', fontsize = 17)
###            ax2.set_ylabel("Monthly rainfall (mm)", fontsize = 17)
        figFname = 'trapRate_No_Rain.png'
        P.savefig(figFname, format='png', dpi = 1000)
        P.show()



def main():
    # paths and data to read in

    #########################       HAVE TO SET THE MODEL NAME HERE

    modelNumber = 3

    #########################

    modelName = 'Model' + str(modelNumber) 
    
    manipName = ('manip_' + modelName + '.pkl')
    outputDataPath = os.path.join(os.getenv('CHINAPROJDIR', default='.'), 
            'Results_' + modelName)

    print('############################################')
    print('Model name:', modelName)
    print('Pickle Name:', manipName)
    print('path name', outputDataPath)

    ## read in manipulated data from pickle
    apodemusFile = os.path.join(outputDataPath, manipName)
    fileobj = open(apodemusFile, 'rb')
    apodemusdata = pickle.load(fileobj)
    fileobj.close()


    resultsobj = ResultsProcessing(apodemusdata, outputDataPath)

#    resultsobj.getSummaryMeans()
#    resultsobj.plotMeanTraprate_RainTemp()

##    resultsobj.getPopNames()              # not used

#    resultsobj.getTrapNAN()
#    resultsobj.getDates()



if __name__ == '__main__':
    main()

