#!/usr/bin/env python

import sys
import apodemus
import params
import optparse
from exploreRodents import ApodemusData

class CmdArgs(object):
    def __init__(self):
        p = optparse.OptionParser()
        p.add_option("--basicdata", dest="basicdata", help="Input basicdata pkl")
        (options, args) = p.parse_args()
        self.__dict__.update(options.__dict__)

cmdargs = CmdArgs()
print(cmdargs.basicdata)
# an instance of the Class params.Params()
params = params.Params()

if len(sys.argv) == 1:
    # no basicdata
    apodemus.main(params)

else:
    #then passed the name of the pickle file on the command line
    apodemus.main(params, cmdargs.basicdata)



