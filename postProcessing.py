#!/usr/bin/env python


import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import pickle
import os
import datetime
#from mcmcApodemus import Gibbs

###################################
#############################
#########################       HAVE TO SET THE MODEL NAME HERE

#from model1 import Params

#########################
############################
##################################


def logit(x):
    """
    Function (logit) to convert probability to real number
    """
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    """
    Function to convert real number to probability
    """
    return np.exp(x) / (1 + np.exp(x))


class ResultsProcessing(object):
    def __init__(self, gibbsobj, outputDataPath):

#        self.params = params
        self.gibbsobj = gibbsobj
        self.modelNumber = gibbsobj.modelNumber
        self.modelName = 'Model' + str(self.modelNumber) 
#        self.ndat = len(self.gibbsobj.siggibbs)
        self.results = np.hstack([self.gibbsobj.bgibbs,
                                self.gibbsobj.alphagibbs,
                                self.gibbsobj.ggibbs,
                                self.gibbsobj.siggibbs])
        if self.modelNumber == 3:
            self.results = np.column_stack((self.results, self.gibbsobj.lrho_1gibbs))
            self.results = np.column_stack((self.results, self.gibbsobj.lrho_2gibbs))            
            self.results = np.column_stack((self.results, self.gibbsobj.sigmarhogibbs))         
#            self.logit_rhogibbs = mcmcobj.logit_rhogibbs
            self.Rho2D = inv_logit(self.gibbsobj.logit_rhogibbs)
#            self.meanRho = np.mean(Rho2D, axis = 0)
#            self.rhoQuants = mquantiles(Rho2D, prob = [0.025, 0.975], axis = 0)
        self.meanN = np.mean(self.gibbsobj.Ngibbs, axis = 0)

        self.npara = self.results.shape[1]
        self.nMcmc = len(self.gibbsobj.siggibbs)
        print('Keep MCMC iters (ngibbs):', self.nMcmc)
        print('Burn in:', self.gibbsobj.burnin)
        print('Thin rate:', self.gibbsobj.thinrate)
        print('############################################')

        # move variables
        self.nsessions = self.gibbsobj.nsessions      # n sessions per habitat
        self.datSessions = self.gibbsobj.datSessions
        self.datNTrapped = self.gibbsobj.datNTrapped
        self.datNSetTraps = self.gibbsobj.datNSetTraps
        self.datMonthRun = self.gibbsobj.datMonthRun  # from 13 up
        self.datMonth = self.gibbsobj.datMonth
        self.datYear = self.gibbsobj.datYear
        self.datHabitat = self.gibbsobj.datHabitat    # 0 = lowland and 1 = upland
        self.habMask0 = self.datHabitat == 0
        self.habMask1 = self.datHabitat == 1
        self.datStartMonth = self.gibbsobj.datStartMonth
        self.datMinMonthRun = self.gibbsobj.datMinMonthRun
        self.datMaxMonthRun = self.gibbsobj.datMaxMonthRun
        ## repro variables
        self.datNFemales = self.gibbsobj.datNFemales
        self.datNPregnant = self.gibbsobj.datNPregnant
        self.datPromTesticle = self.gibbsobj.datPromTesticle
        self.nDat = self.gibbsobj.nDat                # total data
        self.datRain = self.gibbsobj.datRain
        self.datTemperature = self.gibbsobj.datTemperature
        self.datRice = self.gibbsobj.datRice
        self.datRapeWheat = self.gibbsobj.datRapeWheat
        self.datVegetables = self.gibbsobj.datVegetables
        self.datTrees = self.gibbsobj.datTrees
        self.datDryCrop = self.gibbsobj.datDryCrop
        self.uDatYear = self.gibbsobj.uDatYear
        self.nDatYear = self.gibbsobj.nDatYear
        # Cross validation data
        self.monthCV = self.gibbsobj.monthCV
        self.yearCV = self.gibbsobj.yearCV
        self.cvDates = self.gibbsobj.cvDates
        self.nCV = self.gibbsobj.nCV
        self.cvPointMask = self.gibbsobj.cvPointMask
        # Covariates
        self.tArray = self.gibbsobj.tArray
        self.detectCovariates = self.gibbsobj.detectCovariates
        # Params
        self.outputDataPath = outputDataPath
        self.thinrate = gibbsobj.thinrate
        self.burnin = gibbsobj.burnin
        self.followSess = gibbsobj.followSess
        


    def getSummaryMeans(self):
        """
        get mean trap rate and summaries for Results
        """
        trapSetNAN = self.datNSetTraps.astype(float)
        trapSetNAN[trapSetNAN <= 0.0] = np.nan
        self.summaryTrapRate = self.datNTrapped / trapSetNAN
        self.meanMonthTrapRate = np.empty((12,6))
        self.meanMonthTrapRate[:,0] =  np.arange(1.,13.)
        for i in range(12):
            monthMask = self.datMonth == (i + 1)
            lowHabMonthMask = monthMask & self.habMask0
            highHabMonthMask = monthMask & self.habMask1
            # mean trap rate
            tr_i = self.summaryTrapRate[lowHabMonthMask] * 100.0
            tr_i_up = self.summaryTrapRate[highHabMonthMask] * 100.0
            self.meanMonthTrapRate[i,1] = np.nanmean(tr_i) 
            
            self.meanMonthTrapRate[i, 2] =  np.nanmean(tr_i_up)
            # traprate quantiles
#            self.meanMonthTrapRate[i, 2] =  np.nanpercentile(tr_i, 2.5)
            self.meanMonthTrapRate[i, 3] =  np.nanpercentile(tr_i, 97.5)
            # mean log rainfall 
            rain_i = self.datRain[monthMask]
            self.meanMonthTrapRate[i,4] = np.nanmean(rain_i)
#            self.meanMonthTrapRate[i,4] = np.log(np.nanmean(rain_i) + 1.0)
            temp_i = self.datTemperature[monthMask]
            # mean log temp
            self.meanMonthTrapRate[i,5] = np.nanmean(temp_i)
#            self.meanMonthTrapRate[i,5] = np.log(np.nanmean(temp_i) + 1.0)
#        print('meanMonthTrapRate', np.round(self.meanMonthTrapRate, 2))
        
    def plotPredictPregRate(self):
        """
        ## plot temperature and preg rates against calendar month
        ## plot predicted preg rates across temperature gradient
        """ 
        if self.modelNumber == 3:
            self.getMeanRho()
        else:       
            self.getPredPregRates()
        P.figure(figsize=(11,9))
        monNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
            'Sep', 'Oct', 'Nov', 'Dec']
        P.subplot(2, 1, 1)
        ax1 = P.gca()
        mon = self.meanMonthTrapRate[:, 0]
        lns1 = ax1.plot(mon, self.meanMonthTrapRate[:, 5], color = 'k', 
            label = 'Mean temperature', linewidth = 3.0)
        lns5 = ax1.plot(mon, (self.meanMonthTrapRate[:, 4] / 10.0), color = 'k', 
            label = 'Mean rainfall', ls = '--',  linewidth = 2) 
        # second axis
        ax2 = ax1.twinx()
#        print('pregRateCal', self.pregRateCal[:, 0])
        lns3 = ax2.plot(mon, self.pregRateCal[:, 0], color = 'r', 
            label = 'Predicted pregnacy rate', linewidth = 3.0)
        lns2 = ax2.plot(mon, self.obserPregRate, color = 'b', 
            label = 'Observed pregnacy rate', linewidth = 3.0)
#        lns3 = ax2.plot(mon, self.pregRateCal[:, 1], color = 'b', label = 'Mean rainfall', linewidth = 3.0)
        lns = lns1 + lns5 + lns2 + lns3
        labs = [l.get_label() for l in lns]
#        ax1.set_xlim(minDate, maxDate)
        for tick in ax1.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax1.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        ax1.set_xlabel('Months', fontsize = 17)
        ax1.set_ylabel('Mean temperature C$^\circ$', fontsize = 17)
        ax2.set_ylabel('Mean pregnancy rate', fontsize = 17)
        ymax0 = np.max(self.meanMonthTrapRate[:, 5]) + 5.0
        ymaxAX2 = np.max(self.pregRateCal[:, 0]) + 0.2
        ax1.set_ylim([0.0, ymax0])
        ax2.set_ylim([0.0, 1.0])
        ax1.legend(lns, labs, loc = 'upper right')
        P.xticks(mon, monNames)
        ####    Make second plot temp gradient vs preg rate
        P.subplot(2,1,2)
        ax1 = P.gca()
        lns1 = ax1.plot(self.tempGrad, self.pregRateTemp[:,0], color = 'k', 
            label = 'Temperature C$^\circ$', linewidth = 3.0)
        lns2 = ax1.plot(self.tempGrad, self.pregRateTemp[:,1], color = 'k', 
            ls = '--', label = '95% CI', linewidth = 1.0)
        lns3 = ax1.plot(self.tempGrad, self.pregRateTemp[:,2], color = 'k', 
            ls = '--', linewidth = 1.0)
        lns = lns1 + lns2
        labs = [l.get_label() for l in lns]
        for tick in ax1.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax1.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        ax1.set_xlabel('Temperature C$^\circ$', fontsize = 17)
        ax1.set_ylabel('Pregnacy Rate', fontsize = 17)
        ymax1 = np.max(self.pregRateTemp[:,2] + 0.03)
        ax1.set_ylim([0.0, ymax1])
        ax1.legend(lns, labs, loc = 'upper right')
        figFname = os.path.join(self.outputDataPath, 'predictedPregRate.png')
        P.savefig(figFname, format='png', dpi = 1000)
        P.show()                

    def getMeanRho(self):
        """
        ## get mean estimated rho values for calendar months
        ## across both habitats
        """
        zeroFemsMask = self.datNFemales > 0
        self.pregRateCal = np.zeros((12, 3))
        self.pregRateTemp = np.zeros((12, 3))        
        self.obserPregRate = np.zeros(12)
        # get mean estimated rho values for calendar months
        for i in range(1, 13):
            monthMask = self.datMonth == i
            rho_i = self.Rho2D[:, monthMask]
            meanrho_i = np.mean(rho_i)
 #           print('i', i, 'meanrho', meanrho_i)
            quantsrho_i = mquantiles(rho_i, prob=[0.025, 0.975])
            self.pregRateCal[(i - 1), 0] = meanrho_i
            self.pregRateCal[(i - 1), 1:] = quantsrho_i
            ## get observed preg rate distribution by month
            mask_i = monthMask & zeroFemsMask
            pr_i = np.sum(self.datNPregnant[mask_i]) / np.sum(self.datNFemales[mask_i])
            self.obserPregRate[(i - 1)] = pr_i  
        ## get distribution for samps, loop thru temp gradient
        nSamps = 500
        nTemps = 200
        self.pregRateTemp = np.zeros((nTemps, 3))  
        self.tempGrad = np.linspace(5.0, 30.0, num = nTemps)
        covariatesT = np.ones((nTemps, 3))
        covariatesT[:,1] = np.log(self.tempGrad + 1.0)
        covariatesT[:,2] = (np.log(self.tempGrad + 1.0))**2     
        postSamps = np.random.choice(self.nMcmc, nSamps, replace = True) 
        alphaSamps = self.gibbsobj.alphagibbs[postSamps, :3]  
        for i in range(nTemps):
            logitRho_i = np.dot(alphaSamps, covariatesT[i])
            rho = inv_logit(logitRho_i)
            self.pregRateTemp[i, 0] = np.mean(rho)
            self.pregRateTemp[i, 1:] = mquantiles(rho, prob=[0.025, 0.975])

    def getPredPregRates(self):
        """
        ## get predicted preg rates for temp gradient and calendar months
        """
        nSamps = 500
        nTemps = 200
        self.tempGrad = np.linspace(5.0, 30.0, num = nTemps)
        postSamps = np.random.choice(self.nMcmc, nSamps, replace = True)
        alphaSamps = self.gibbsobj.alphagibbs[postSamps, :-2]
        self.pregRateTemp = np.zeros((nTemps, 3))
        covariatesT = np.ones((nTemps, 4))
        covariatesT[:,1] = np.log(self.tempGrad + 1.0)
        covariatesT[:,2] = (np.log(self.tempGrad + 1.0))**2
        covariatesT[:,3] = (np.log(self.tempGrad + 1.0))**3         
        ## get distribution for samps, loop thru temp gradient
        for i in range(nTemps):
            logitRho_i = np.dot(alphaSamps, covariatesT[i])
            rho = inv_logit(logitRho_i)
            self.pregRateTemp[i, 0] = np.mean(rho)
            self.pregRateTemp[i, 1:] = mquantiles(rho, prob=[0.025, 0.975])
        ## get preg rates for mean temps for calendar months
        calendarTCov = np.ones((12, 4))
        calT = self.meanMonthTrapRate[:,-1]
        calendarTCov[:,1] = np.log(calT + 1)
        calendarTCov[:,2] = (np.log(calT + 1))**2
        calendarTCov[:,3] = (np.log(calT + 1))**3
        self.pregRateCal = np.zeros((12, 3))
        self.obserPregRate = np.zeros(12)
        zeroFemsMask = self.datNFemales > 0
        for i in range(12):
            logitRho_i = np.dot(alphaSamps, calendarTCov[i])
            rho = inv_logit(logitRho_i)
            self.pregRateCal[i, 0] = np.mean(rho)
            self.pregRateCal[i, 1:] = mquantiles(rho, prob=[0.025, 0.975])
            ## get observed preg rate distribution by month
            monMask = self.datMonth == (i + 1)
            mask_i = monMask & zeroFemsMask
            pr_i = np.sum(self.datNPregnant[mask_i]) / np.sum(self.datNFemales[mask_i])
            self.obserPregRate[i] = pr_i     # np.mean(pr_i)
#            self.obserPregRate[i, 1:] = mquantiles(pr_i, prob=[0.025, 0.975])
 #       print('obs preg Rate', self.obserPregRate)



    def plotMeanTraprate_RainTemp(self):
        """
        plot monthly means of traprates,rainfall and temperature
        """
        P.figure(figsize=(11,9))
        monNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
            'Sep', 'Oct', 'Nov', 'Dec']
        ax1 = P.gca()
        mon = self.meanMonthTrapRate[:, 0]
        lns1 = ax1.plot(mon, self.meanMonthTrapRate[:, 1], color = 'k', label = 'Mean lowland trap rate', linewidth = 3)
        lns2 = ax1.plot(mon, self.meanMonthTrapRate[:, 2], color = 'k', 
                ls = '--', label = 'Mean upland trap rate', linewidth = 2.5)
#        lns2 = ax1.plot(mon, self.meanMonthTrapRate[:, 2], color = 'k', 
#                ls = '--', label = '95% CI trap rate', linewidth = 1.0)
#        lns3 = ax1.plot(mon, self.meanMonthTrapRate[:, 3], color = 'k', ls = '--', linewidth = 1.0)
        lns5 = ax1.plot(mon, self.meanMonthTrapRate[:, 5], color = 'r', label = 'Mean temperature', linewidth = 1.0)
        # second axis
        ax2 = ax1.twinx()
        lns4 = ax2.plot(mon, self.meanMonthTrapRate[:, 4], color = 'b', label = 'Mean rainfall', linewidth = 1.0)

        lns = lns1 + lns2 + lns5 + lns4
        labs = [l.get_label() for l in lns]
#        ax1.set_xlim(minDate, maxDate)
        for tick in ax1.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax1.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        ax1.set_xlabel('Months', fontsize = 17)
        ax2.set_ylabel('Mean rainfall (mm)', fontsize = 17)
        ymax = np.max(self.meanMonthTrapRate[:, 3])
        ax1.set_ylabel('Mean trap rate & temperature', fontsize = 17)
        ax1.set_ylim([-1.0, ymax])
        ax1.legend(lns, labs, loc = 'upper right')
        P.xticks(mon, monNames)
        figFname = 'monthMeanTR_rainTmp.png'
        P.savefig(figFname, format='png', dpi = 1000)
        P.show()                


    def getCorrelations3_9(self):
        """
        ## get correlations of pregrates
        """
        monSeq = np.arange(4, 10)       # April - Sept
        nMon = len(monSeq)
        nCorr = self.nDatYear * nMon
        keepMask = np.zeros(nCorr, dtype = bool)
        self.t_1MarSept = np.zeros((nCorr, 2))
        cc = 0
        for i in self.uDatYear:
            yrMask = self.datYear == i
            for j in monSeq:
                m0 = (self.datMonth == (j - 1)) & yrMask
                m1 = (self.datMonth == j) & yrMask
                nfem0 = np.sum(self.datNFemales[m0])
                nfem1 = np.sum(self.datNFemales[m1])
                if (nfem0 > 0) & (nfem1 > 0):
                    npreg0 = np.sum(self.datNPregnant[m0])
                    npreg1 = np.sum(self.datNPregnant[m1])
                    self.t_1MarSept[cc, 0] = npreg0 / nfem0 
                    self.t_1MarSept[cc, 1] = npreg1 / nfem1
                    keepMask[cc] = 1
                cc += 1
        self.t_1MarSept = self.t_1MarSept[keepMask]
        print('corr t_1 Mar-Sept', np.corrcoef(self.t_1MarSept[:, 0], self.t_1MarSept[:, 1]))
        P.figure()
        P.scatter(self.t_1MarSept[:, 0], self.t_1MarSept[:, 1])
        P.show()


    def getCorrelations4_5(self):
        """
        ## get correlations of pregrates
        """
        nMon = 1
        nCorr = self.nDatYear * nMon
        keepMask45 = np.zeros(nCorr, dtype = bool)
        self.cor45 = np.zeros((nCorr, 2))
        cc = 0
        for i in self.uDatYear:
            yrMask = self.datYear == i
            m4 = (self.datMonth == 4) & yrMask
            m5 = (self.datMonth == 5) & yrMask
            nfem4 = np.sum(self.datNFemales[m4])
            nfem5 = np.sum(self.datNFemales[m5])
            if (nfem4 > 0) & (nfem5 > 0):
                npreg4 = np.sum(self.datNPregnant[m4])
                npreg5 = np.sum(self.datNPregnant[m5])
                self.cor45[cc, 0] = npreg4 / nfem4 
                self.cor45[cc, 1] = npreg5 / nfem5
                keepMask45[cc] = 1
                cc += 1
        self.cor45 = self.cor45[keepMask45]
        print('corr 4_5', np.corrcoef(self.cor45[:, 0], self.cor45[:, 1]))
        P.figure()
        P.scatter(self.cor45[:, 0], self.cor45[:, 1])
        P.show()

    def getCorrelations8_9(self):
        """
        ## get correlations of pregrates
        """
        nMon = 1
        nCorr = self.nDatYear * nMon
        keepMask89 = np.zeros(nCorr, dtype = bool)
        self.cor89 = np.zeros((nCorr, 2))
        cc = 0
        for i in self.uDatYear:
            yrMask = self.datYear == i
            m8 = (self.datMonth == 8) & yrMask
            m9 = (self.datMonth == 9) & yrMask
            nfem8 = np.sum(self.datNFemales[m8])
            nfem9 = np.sum(self.datNFemales[m9])
            if (nfem8 > 0) & (nfem9 > 0):
                npreg8 = np.sum(self.datNPregnant[m8])
                npreg9 = np.sum(self.datNPregnant[m9])
                self.cor89[cc, 0] = npreg8 / nfem8 
                self.cor89[cc, 1] = npreg9 / nfem9
                keepMask89[cc] = 1
                cc += 1
        self.cor89 = self.cor89[keepMask89]
        print('corr 8_9', np.corrcoef(self.cor89[:, 0], self.cor89[:, 1]))
        P.figure()
        P.scatter(self.cor89[:, 0], self.cor89[:, 1])
        P.show()

    def getCorrelations4_5_8_9(self):
        nMon = 1
        nCorr = self.nDatYear * nMon
        keepMask4589 = np.zeros(nCorr, dtype = bool)
        self.cor4589 = np.zeros((nCorr, 2))
        cc = 0
        i45 = [4,5]
        i89 = [8,9]
        for i in self.uDatYear:
            yrMask = self.datYear == i
            m45 = (np.in1d(self.datMonth, i45)) & yrMask
            m89 = (np.in1d(self.datMonth, i89)) & yrMask
            nfem45 = np.sum(self.datNFemales[m45])
            nfem89 = np.sum(self.datNFemales[m89])
            if (nfem45 > 0) & (nfem89 > 0):
                npreg45 = np.sum(self.datNPregnant[m45])
                npreg89 = np.sum(self.datNPregnant[m89])
                self.cor4589[cc, 0] = npreg45 / nfem45 
                self.cor4589[cc, 1] = npreg89 / nfem89
                keepMask4589[cc] = 1
                cc += 1
        self.cor4589 = self.cor4589[keepMask4589]
        print('corr 45_89', np.corrcoef(self.cor4589[:, 0], self.cor4589[:, 1]))
        P.figure()
        P.scatter(self.cor4589[:, 0], self.cor4589[:, 1])
        P.show()


    def getCorrelations4_5_6_7(self):
        nMon = 1
        nCorr = self.nDatYear * nMon
        keepMask4567 = np.zeros(nCorr, dtype = bool)
        self.cor4567 = np.zeros((nCorr, 2))
        cc = 0
        i45 = [4,5]
        i67 = [6,7]
        for i in self.uDatYear:
            yrMask = self.datYear == i
            m45 = (np.in1d(self.datMonth, i45)) & yrMask
            m67 = (np.in1d(self.datMonth, i67)) & yrMask
            nfem45 = np.sum(self.datNFemales[m45])
            nfem67 = np.sum(self.datNFemales[m67])
            if (nfem45 > 0) & (nfem67 > 0):
                npreg45 = np.sum(self.datNPregnant[m45])
                npreg67 = np.sum(self.datNPregnant[m67])
                self.cor4567[cc, 0] = npreg45 / nfem45 
                self.cor4567[cc, 1] = npreg67 / nfem67
                keepMask4567[cc] = 1
                cc += 1
        self.cor4567 = self.cor4567[keepMask4567]
        print('corr 45_67', np.corrcoef(self.cor4567[:, 0], self.cor4567[:, 1]))
        P.figure()
        P.scatter(self.cor4567[:, 0], self.cor4567[:, 1])
        P.show()


    def getCorrelations6_7_8_9(self):
        nMon = 1
        nCorr = self.nDatYear * nMon
        keepMask6789 = np.zeros(nCorr, dtype = bool)
        self.cor6789 = np.zeros((nCorr, 2))
        cc = 0
        i67 = [4,5]
        i89 = [8,9]
        for i in self.uDatYear:
            yrMask = self.datYear == i
            m67 = (np.in1d(self.datMonth, i67)) & yrMask
            m89 = (np.in1d(self.datMonth, i89)) & yrMask
            nfem67 = np.sum(self.datNFemales[m67])
            nfem89 = np.sum(self.datNFemales[m89])
            if (nfem67 > 0) & (nfem89 > 0):
                npreg67 = np.sum(self.datNPregnant[m67])
                npreg89 = np.sum(self.datNPregnant[m89])
                self.cor6789[cc, 0] = npreg67 / nfem67 
                self.cor6789[cc, 1] = npreg89 / nfem89
                keepMask6789[cc] = 1
                cc += 1
        self.cor6789 = self.cor6789[keepMask6789]
        print('corr 67_89', np.corrcoef(self.cor6789[:, 0], self.cor6789[:, 1]))
        P.figure()
        P.scatter(self.cor6789[:, 0], self.cor6789[:, 1])
        P.show()

            


    def getTrapNAN(self):
        """
        get trap data with nan inserted
        """
        self.trapRate = self.nTrapped.copy()
        zeroTrapMask = self.nTrapSet == 0
        trapMask = self.nTrapSet > 0
        self.trapRate[trapMask] = self.nTrapped[trapMask] / self.nTrapSet[trapMask] * 100.0
        self.trapRate = self.trapRate.astype(np.float64)
        self.trapRate[zeroTrapMask] = np.nan
#        self.trapRate = self.trapRate.astype(np.float64)
 

    @staticmethod    
    def quantileFX(a):
        return mquantiles(a, prob=[0.025, 0.5, 0.975], axis = 0)

    def getPopNames(self):
        """
        get strings to id session population size in table
        """
        nPop = np.shape(self.gibbsobj.Ngibbs)[1]    
        pop = np.arange(nPop)
        popName = 'N0'
        for i in range(1,nPop):
            aa = 'N'+str(i)
            popName = np.append(popName,aa)
        self.popName = popName.copy()

    def makeTableFX(self):
        resultTable = np.zeros(shape = (4, self.npara))
        resultTable[0:3, :] = np.round(self.quantileFX(self.results), 3)
        resultTable[3, :] = np.round(np.mean(self.results, axis = 0), 3)
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Names', 'Low CI', 'Median', 'High CI', 'Mean'])
        if self.modelNumber == 3:
            self.names = np.array(['Surv_irr', 'Surv_nonIrr', 'Intercept', 'Temp', 'Temp**2',
                    'vegetables', 'trees', 'AR1_logitRho', 'AR2_logitRho', 'detectInter', 'detectRain',
                    'sigma_Irr', 'sigma_nonIrr', 'logitRho_1_Irr', 'logitRho_1_nonIrr',
                    'logitRho_2_Irr', 'logitRho_2_nonIrr', 'sigmaRho'])
        else:
            self.names = np.array(['Surv_irr', 'Surv_nonIrr', 'Intercept', 'Temp', 'Temp**2',
                    'Temp**3', 'vegetables', 'trees', 'detectInter', 'detectRain',
                    'sigma_Irr', 'sigma_nonIrr'])
        for i in range(self.npara):
            name = self.names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)
        self.summaryTable = resultTable.copy()


    def plotFX(self):
        """
        plot diagnostic trace plots
        """
        ngShp = np.shape(self.gibbsobj.Ngibbs)
        col = (ngShp[1] - 3)
        nG = self.gibbsobj.Ngibbs
#        print('nG', nG[-5:, -7:], 'ngshp', ngShp, 'col', col, 'nsess', self.nsessions)
        cc = 0
        nfigures = np.int(np.ceil(self.npara/6.0)) + 2
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            if i == 0:
                for j in range(6):
                    col_j = self.followSess[j]
#                    gibbsdat = self.gibbsobj.Ngibbs[:, col_j]
                    gibbsdat = self.gibbsobj.logit_rhogibbs[:, col_j]
#                    print('col_j', col_j, gibbsdat, gibbsdat[:10])
                    P.subplot(2,3,j+1)
                    P.plot(gibbsdat)
                    P.title('Irr logit rho sess'+str(col_j))
            if i == 1:
                for j in range(6):
                    col_j = self.followSess[j] + self.nsessions - 10
#                    gibbsdat = self.gibbsobj.Ngibbs[:, col_j]
                    gibbsdat = self.gibbsobj.logit_rhogibbs[:, col_j]
#                    print('col_j', col_j, 'gibbsdat', gibbsdat[:10])
                    P.subplot(2,3,j+1)
                    P.plot(gibbsdat)
                    P.title('Non-irr logit rho sess'+str(col_j))
            if i > 1:
                for j in range(6):
                    P.subplot(2,3,j+1)
                    if cc < self.npara:
                        P.plot(self.results[0:self.nMcmc, cc])
                        P.title(self.names[cc])
                    cc = cc + 1
                P.show(block=lastFigure)

    def abPara(self):
        """
        get parameters from gibbs
        """
#        self.apara = np.mean(self.gibbsobj.agibbs, axis = 0)
        self.bpara = np.mean(self.gibbsobj.bgibbs, axis = 0)
        self.alphapara = np.mean(self.gibbsobj.alphagibbs, axis = 0)
        self.gpara = np.mean(self.gibbsobj.ggibbs, axis = 0)
        self.harmonics = np.zeros(len(self.months))
        self.pGrowth = np.zeros(len(self.months))
        self.lambdaPred = np.exp(np.dot(self.detectCovariates, self.gpara))
        expTerm = self.meanN * self.lambdaPred
        self.theta = 1.0 - np.exp(-expTerm)
        # loop thru habitat
        for i in range(2):
            mask_i = self.habIndx == i
            tArray_i = self.tArray[mask_i]
            harmonics_i =  np.dot(tArray_i, self.alphapara)
            self.harmonics[mask_i] = harmonics_i.copy()
#            self.pGrowth[mask_i] = self.apara[i] + harmonics_i
        

    def getNPred(self):
        """
        calc npred for all sessions
        """
        # get parameters
        self.abPara()
        self.nPred = np.zeros(len(self.months))
        # loop thru habitat
        for i in range(2):
            mask_i = self.habIndx == i
            npred_i = self.nPred[mask_i]
            N_i = self.meanN[mask_i]
            npred_i[0] = N_i[0]
#            a = self.apara[i]
            b = self.bpara[i]
            harmonics_i = self.harmonics[mask_i]
            # loop thru sessions
            for j in range(1, self.nsessions):
                xt_1 = np.log(N_i[j - 1])
                mu_j = xt_1 + (b * xt_1) + harmonics_i[j]
                npred_i[j] = np.exp(mu_j)
            self.nPred[mask_i] = npred_i

#        prArr = np.hstack([np.expand_dims(self.meanN, 1), np.expand_dims(self.nTrapped, 1),
#            np.expand_dims(self.nTrapped * 2.5, 1), np.expand_dims(self.nPred, 1)])
#        print('N, ntrap, ntrap*2.5, npred', prArr[:50])


    def getDates(self):
        """
        make dates array
        """
        self.dates = []
        mmm = self.months[self.habMask0]
        yyy = self.years[self.habMask0]
        for month, year in zip(mmm, yyy):
            date = datetime.date(int(year), int(month), 1)
            self.dates.append(date)

    def nPredPlot(self):
        """
        plot predicted N and trap rate over time
        """
        minDate = datetime.date(2010, 1, 1)
        maxDate = datetime.date(2013, 12, 31)
#        minDate = datetime.date(1987, 1, 1)
#        maxDate = datetime.date(2015, 1, 1)
        P.figure(figsize=(14, 11))
        for i in range(2):
            mask_i = self.habIndx == i
            N_Hab = self.meanN[mask_i]
            tRate_i = self.trapRate[mask_i] / 100.0
            npred_i = self.nPred[mask_i] 
            ax1 = P.subplot(2, 1, i + 1)
#            lns1 = ax1.plot(self.dates, N_Hab, color = 'k', label = 'Pop. Size', linewidth = 1.25)
            lns3 = ax1.plot(self.dates, tRate_i, color = 'k', label = 'Trap rate', linewidth = 3.0)
#            ax2 = ax1.twinx()
#            lns2 = ax2.plot(self.dates, npred_i, color = 'r', label = 'Predicted Pop.', linewidth = 2.0)
            lns = lns3
            labs = [l.get_label() for l in lns]
            if i == 0:
                lTit = 'Irrigated'
                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            else:
                lTit = 'Upland'
                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            ax1.set_ylim([-.02, 0.25])
#            ax2.set_ylim([-20, 140.])
            ax1.set_xlim(minDate, maxDate)
            ax = P.gca()
            for tick in ax.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            for tick in ax.yaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            if i == 1:
                ax1.set_xlabel('Time', fontsize = 17)
            ax1.set_ylabel("Trap rate", fontsize = 17)
#            ax2.set_ylabel('Predicted N', fontsize = 17)
        figFname = 'TrapRateYear.png'
        P.savefig(figFname, format='png')
        P.show()                



    def plotMakeFigures(self):
        """
        plot growth rate over time
        """
#        minDate = datetime.date(2003, 1, 1)
#        maxDate = datetime.date(2007, 12, 31)
#        minDate = datetime.date(1990, 1, 1)
#        maxDate = datetime.date(2010, 12, 31)
        minDate = datetime.date(1987, 1, 1)
        maxDate = datetime.date(2015, 12, 1)
        P.figure(figsize=(14, 11))
        for i in range(2):
            mask_i = self.habIndx == i
            ax1 = P.subplot(2, 1, i + 1)
#            pGrowth = self.pGrowth[mask_i]
#            harmonics = self.harmonics[mask_i]
            rain_i = self.rainDat[mask_i]
            rainT_1 = self.Raint_1[mask_i] 
            npred_i = self.nPred[mask_i]
            tRate_i = self.trapRate[mask_i]
#            pRice = self.tArray[mask_i, 7]
#            pVeg = self.tArray[mask_i, 8]
#            pTrees = self.tArray[mask_i, 9]
#            pRice = self.tArray[mask_i,6 ]
            pVeg = self.tArray[mask_i, 6]
            pTrees = self.tArray[mask_i, 7]
#            lns1 = ax1.plot(self.dates, npred_i, color = 'k', label = 'Population size', linewidth = 3.0)
#            lns1 = ax1.plot(self.dates, tRate_i, color = 'k', label = 'Trap rate (x100)', linewidth = 2.0)
            lns1 = ax1.plot(self.dates, tRate_i, color = 'k', linewidth = 2.0)
            
###            ax2 = ax1.twinx()
###            lns2 = ax2.plot(self.dates, rain_i, color = 'b', label = 'Rainfall (mm)', linewidth = 1.0)
#            lns3 = ax2.plot(self.dates, pRice , color = 'y', label = '%Rice', linewidth = 2.0)
#            lns4 = ax2.plot(self.dates, pVeg , color = 'r', label = '%Veg', linewidth = 2.0)
#            lns5 = ax2.plot(self.dates, pTrees , color = 'g', label = 'Trees crop', linewidth = 4.0)
###            lns = lns1 # + lns2
###            labs = [l.get_label() for l in lns]
###            if i == 0:
###                lTit = 'Lowland'
###                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
###            else:
###                lTit = 'Upland'
###                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            ax1.set_ylim(0.0, 35.0)
            ax1.set_xlim(minDate, maxDate)
#            ax2.set_ylim([-0.02, 1.05])
###            ax2.set_ylim([0.0, 400.0])

            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            for tick in ax1.yaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            ax1.set_xlabel('Time', fontsize = 17)
            ax1.set_ylabel('Monthly trap rate (x 100)', fontsize = 17)
###            ax2.set_ylabel("Monthly rainfall (mm)", fontsize = 17)
        figFname = 'trapRate_No_Rain.png'
        P.savefig(figFname, format='png', dpi = 1000)
        P.show()


    def plotGrowthRate(self):
        """
        plot growth rate over time
        """
#        minDate = datetime.date(2009, 1, 1)
#        maxDate = datetime.date(2014, 12, 31)


        minDate = datetime.date(2011, 1, 1)
        maxDate = datetime.date(2013, 12, 31)
#        minDate = datetime.date(1987, 1, 1)
#        maxDate = datetime.date(2015, 1, 1)
        P.figure(figsize=(14, 11))
        for i in range(2):
            mask_i = self.habIndx == i
            ax1 = P.subplot(2, 1, i + 1)
#            pGrowth = self.pGrowth[mask_i]
            harmonics = self.harmonics[mask_i]
#            pRice = self.tArray[mask_i, 6]
            pVeg = self.tArray[mask_i, 6]
            pTrees = self.tArray[mask_i, 7]
#            lns1 = ax1.plot(self.dates, pGrowth, color = 'k', label = 'Pop growth', linewidth = 3.0)
            lns2 = ax1.plot(self.dates, harmonics, color = 'b', label = 'Pop growth', linewidth = 3.0)
            ax2 = ax1.twinx()
            lns3 = ax2.plot(self.dates, pRice , color = 'y', label = '%Rice', linewidth = 1.0)
            lns4 = ax2.plot(self.dates, pVeg , color = 'r', label = '%Veg', linewidth = 1.0)
            lns = lns2 + lns3 + lns4
            labs = [l.get_label() for l in lns]
            if i == 0:
                lTit = 'Irrigated'
                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            else:
                lTit = 'Non-irri2gated'
                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            ax1.legend(lns, labs, loc = 'upper right')
            ax1.set_ylim(-1.3, 2.0)
            ax1.set_xlim(minDate, maxDate)
            ax2.set_ylim([0, 1.1])

            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            for tick in ax1.yaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            ax1.set_xlabel('Time', fontsize = 17)
            ax1.set_ylabel('Pop. growth rate', fontsize = 17)
            ax2.set_ylabel("Percent vegetables", fontsize = 17)
        P.show()
 

    def plotNpredDetect(self):
        """
        plot predicted N and trap rate over time and pdetection
        """
#        minDate = datetime.date(2009, 1, 1)
#        maxDate = datetime.date(2014, 12, 31)

#        minDate = datetime.date(1987, 1, 1)
#        maxDate = datetime.date(2005, 12, 31)
        minDate = datetime.date(1987, 1, 1)
        maxDate = datetime.date(2015, 1, 1)
        P.figure(figsize=(14, 11))
        for i in range(2):
            mask_i = self.habIndx == i
            N_Hab = self.meanN[mask_i]
            tRate_i = (self.trapRate[mask_i] / 100.0)
            npred_i = self.nPred[mask_i]
            theta_i = self.theta[mask_i] 
            nTrapped_i = self.nTrapped[mask_i]
            ax1 = P.subplot(2, 1, i + 1)
            lns1 = ax1.plot(self.dates, N_Hab, color = 'g', label = 'Pop. Size', linewidth = 1.25)
            lns2 = ax1.plot(self.dates, npred_i, color = 'b', label = 'Predicted Pop.', linewidth = 3.0)
            ax2 = ax1.twinx()
            lns3 = ax2.plot(self.dates, tRate_i, color = 'r', label = 'Trap rate', linewidth = 1.0)
            lns4 = ax2.plot(self.dates, theta_i, color = 'k', label = 'Prob. Capture', linewidth = 1.0)
            lns = lns1 + lns2 + lns3 + lns4
            labs = [l.get_label() for l in lns]
            if i == 0:
                lTit = 'Irrigated'
                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            else:
                lTit = 'Non-irrigated'
                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            ax1.set_ylim([0, 210])
            ax2.set_ylim([0, 1.0])
            ax1.set_xlim(minDate, maxDate)
            ax = P.gca()
            for tick in ax.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            for tick in ax.yaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            if i == 1:
                ax1.set_xlabel('Time', fontsize = 17)
            ax1.set_ylabel('Predicted pop.', fontsize = 17)
    #        P.xticks([datetime.date(2009, 1, 1), datetime.date(2011, 1, 1), datetime.date(2013, 1, 1),
    #                    datetime.date(2015, 1, 1), datetime.date(2017, 1, 1), datetime.date(2019, 1, 1)])
            ax2.set_ylabel("Trap rate and Prob. capt.", fontsize = 17)
        P.show()                


    def plotNpredPGrowth(self):
        """
        plot predicted N and trap rate over time and pdetection
        """
#        minDate = datetime.date(1997, 1, 1)
#        maxDate = datetime.date(2005, 12, 31)
        minDate = datetime.date(2009, 1, 1)
        maxDate = datetime.date(2014, 12, 31)

#        minDate = datetime.date(1987, 1, 1)
#        maxDate = datetime.date(1988, 12, 31)
#        minDate = datetime.date(1987, 1, 1)
#        maxDate = datetime.date(2015, 1, 1)
        P.figure(figsize=(14, 11))
        for i in range(2):
            mask_i = self.habIndx == i
            npred_i = self.nPred[mask_i]
            pGrowth_i = self.harmonics[mask_i]
#            nTrapped_i = self.nTrapped[mask_i]
            ax1 = P.subplot(2, 1, i + 1)
#            lns1 = ax1.plot(self.dates, N_Hab, color = 'k', label = 'Pop. Size', linewidth = 1.25)
            lns2 = ax1.plot(self.dates, npred_i, color = 'b', label = 'Predicted Pop.', linewidth = 3.0)
            ax2 = ax1.twinx()
            lns3 = ax2.plot(self.dates, pGrowth_i, color = 'r', label = 'Pop. growth rate', linewidth = 1.0)
            lns = lns2 + lns3
            labs = [l.get_label() for l in lns]
            if i == 0:
                lTit = 'Irrigated'
                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            else:
                lTit = 'Non-irrigated'
                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            ax1.set_ylim([0, 110])
            ax2.set_ylim([0, 2.5])
            ax1.set_xlim(minDate, maxDate)
            ax = P.gca()
            for tick in ax.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            for tick in ax.yaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            ax1.set_ylabel('Predicted pop.', fontsize = 17)
    #        P.xticks([datetime.date(2009, 1, 1), datetime.date(2011, 1, 1), datetime.date(2013, 1, 1),
    #                    datetime.date(2015, 1, 1), datetime.date(2017, 1, 1), datetime.date(2019, 1, 1)])
            ax2.set_ylabel("Pop. growth rate", fontsize = 17)
        P.show()


    def plotRainDetect(self):
        """
        plot rainfall and pdetection
        """
        minDate = datetime.date(1988, 1, 1)
        maxDate = datetime.date(1992, 12, 31)
#        minDate = datetime.date(1987, 1, 1)
#        maxDate = datetime.date(2015, 1, 1)
        P.figure(figsize=(14, 11))
        for i in range(2):
            mask_i = self.habIndx == i
            tRate_i = (self.trapRate[mask_i] / 100.0)
            theta_i = self.theta[mask_i] 
            rain_i = self.rainDat[mask_i]
            nTrapSet_i = self.nTrapSet[mask_i]
            ax1 = P.subplot(2, 1, i + 1)
#            lns1 = ax1.plot(self.dates, nTrapSet_i, color = 'k', label = 'Number of set traps', linewidth = 1.25)
            lns2 = ax1.plot(self.dates, rain_i, color = 'b', label = 'Rainfall', linewidth = 3.0)
            ax2 = ax1.twinx()
            lns4 = ax2.plot(self.dates, theta_i, color = 'r', label = 'Prob. Capture', linewidth = 3.0)
            lns = lns2 + lns4
            labs = [l.get_label() for l in lns]
            if i == 0:
                lTit = 'Irrigated'
                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            else:
                lTit = 'Upland'
                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
#            ax1.set_ylim([50, 820])
            ax2.set_ylim([0, 1.0])
            ax1.set_xlim(minDate, maxDate)
            ax = P.gca()
            for tick in ax.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            for tick in ax.yaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            if i == 1:
                ax1.set_xlabel('Time', fontsize = 17)
            ax1.set_ylabel('Rainfall (mm) and no. traps', fontsize = 17)
    #        P.xticks([datetime.date(2009, 1, 1), datetime.date(2011, 1, 1), datetime.date(2013, 1, 1),
    #                    datetime.date(2015, 1, 1), datetime.date(2017, 1, 1), datetime.date(2019, 1, 1)])
            ax2.set_ylabel("Prob. capture", fontsize = 17)
        figFname = 'pCaptRainfall.png'
        P.savefig(figFname, format='png')
        P.show()                



########            Write data to file
########
    def writeToFileFX(self, sumTableName):
        (m, n) = self.summaryTable.shape
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Names', 'U12'), ('Low CI', np.float),
                    ('Median', np.float), ('High CI', np.float), ('Mean', np.float)])
        # copy data over
        structured['Low CI'] = self.summaryTable[:, 0]
        structured['Median'] = self.summaryTable[:, 1]
        structured['High CI'] = self.summaryTable[:, 2]
        structured['Mean'] = self.summaryTable[:, 3]
        structured['Names'] = self.names
        tableFname = os.path.join(self.outputDataPath, sumTableName)
        np.savetxt(tableFname, structured, fmt=['%s', '%.4f', '%.4f', '%.4f', '%.4f'],
                 comments='', header='Names Low_CI Median High_CI Mean')

def main():
    # paths and data to read in

    #########################       HAVE TO SET THE MODEL NAME HERE

    modelNumber = 3

    #########################

    modelName = 'Model' + str(modelNumber) 
    
    gibbsName = ('gibbs_' + modelName + '.pkl')
    outputDataPath = os.path.join(os.getenv('CHINAPROJDIR', default='.'), 
            'Results_' + modelName)
    print('############################################')
    print('Model name:', modelName)
    print('gibbs Pickle Name:', gibbsName)
    print('path name', outputDataPath)
    inputGibbs = os.path.join(outputDataPath, gibbsName)
    fileobj = open(inputGibbs, 'rb')
    gibbsobj = pickle.load(fileobj)
    fileobj.close()

    resultsobj = ResultsProcessing(gibbsobj, outputDataPath)

    resultsobj.getSummaryMeans()
#    resultsobj.plotMeanTraprate_RainTemp()

    ## plot of predicted preg rate against obs and temp/rain.
    resultsobj.plotPredictPregRate()

    ## plot of correlations of preg rate
#    resultsobj.getCorrelations3_9()
#    resultsobj.getCorrelations4_5()
#    resultsobj.getCorrelations8_9()
#    resultsobj.getCorrelations4_5_8_9()
#    resultsobj.getCorrelations4_5_6_7()
#    resultsobj.getCorrelations6_7_8_9()
    

##    resultsobj.getPopNames()              # not used

    resultsobj.makeTableFX()

#    resultsobj.getTrapNAN()
#    resultsobj.getNPred()
#    resultsobj.getDates()

    ##### diagnostic trace plots
    resultsobj.plotFX()

#    resultsobj.plotMakeFigures()
##    resultsobj.nPredPlot() 

##    resultsobj.plotGrowthRate()   
##    resultsobj.plotNpredPGrowth()
##    resultsobj.plotNpredDetect()
##    resultsobj.plotRainDetect()

    sumTableName = ('sumTable_' + modelName + '.txt')
    print('Summary Table Name:', sumTableName)

    resultsobj.writeToFileFX(sumTableName)


if __name__ == '__main__':
    main()

