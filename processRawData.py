#!/usr/bin/env python

import os
import numpy as np
import pickle
import pylab as P
import datetime
from scipy.stats.mstats import mquantiles


# special codes for sex
CODE_fem = 1
CODE_male = 0


class RawData(object):
    def __init__(self, params):
        """
        Object to read in trap, morph and environmental data
        """
        # rodent capture data, trap id, week, avail etc
        # Apodemus agarius
        self.params = params

        self.trap = np.genfromtxt(self.params.rawTrapFName,  delimiter=',', names=True,
            dtype=['i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'f8', 'i8','i8','i8','i8',
                    'i8','i8','i8','i8','i8','i8','i8','i8','i8', 'i8','i8'])

        self.trapMask = self.trap['setTraps'] == 0
        self.trapDataMask = self.trap['setTraps'] > 0
        self.nTrapDat = len(self.trapMask)
#        print('ntrapdat', self.nTrapDat)
        self.setTraps = self.trap['setTraps']
        self.Apoagr = self.trap['Apodemus_agrarius']
        self.trapRate = np.empty(self.nTrapDat)
        self.trapRate[self.trapDataMask] = self.Apoagr[self.trapDataMask] / self.setTraps[self.trapDataMask] * 100.0
        self.trapNAN = self.trapRate.copy()
        self.trapNAN[self.trapMask] = np.nan
        self.setTrapsNAN = self.setTraps.copy()
        self.setTrapsNAN = self.setTrapsNAN.astype(np.float64)
        self.setTrapsNAN[self.trapMask] = np.nan
        self.yr = self.trap['year']
        self.mo = self.trap['month']
        self.habitat = self.trap['habitat']
        self.monthRun = self.trap['monthRun']
        self.minMonthRun = 13
        self.maxMonthRun = np.max(self.monthRun)
        self.startMonth = self.minMonthRun + self.params.reproMonthsBack + self.params.weatherMonthsBack     
        # rain and temp data from Yuqing
        self.rain = np.genfromtxt(self.params.rawRainFName,  delimiter=',', names=True,
            dtype=['i8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8','f8','f8','f8', 'f8','f8'])
        self.rainYear = self.rain['year']
        self.temperature = np.genfromtxt(self.params.rawTemperatureFName,  delimiter=',', names=True,
            dtype=['i8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8','f8','f8','f8', 'f8','f8'])

#        # morph data from Yuqing
        self.morph = np.genfromtxt(self.params.rawMorphFName,  delimiter=',', names=True,
            dtype=['i8', 'i8', 'i8', 'i8', 'i8', 'S10', 'f8', 'f8','f8','f8','f8', 'i8'])
        self.morphYear = self.morph['year']
        self.morphMonth = self.morph['month']
        self.morphHabitat = self.morph['habitat']
        self.morphMass = self.morph['mass']
        self.morphSex = self.morph['sex']
        self.morphNFetus = self.morph['nEmbryo']
        nMorph = len(self.morphSex)
        self.morphFemaleBool = np.ones(nMorph, dtype = bool)
        self.morphFemaleBool[self.morphSex == b'Male'] = 0
#        print('sex', self.morphFemaleBool[:50], 'mass', self.morphMass[:50])

        # Repro data by month
        self.reproDat = np.genfromtxt(self.params.rawReproDatFName,  delimiter=',', names=True,
            dtype=['i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8'])
        self.reproMonth = self.reproDat['month']
        self.reproYear = self.reproDat['year']
        self.reproHabitat = (self.reproDat['habitat'] - 1)
        self.reproFemales = self.reproDat['nFemales']
        self.reproNPregnant = self.reproDat['nPregnant']
        self.reproPromTesticle = self.reproDat['promTesticle']

        # croptype data from Yuqing
        self.crop = np.genfromtxt(self.params.rawCropFName,  delimiter=',', names=True,
            dtype=['i8', 'i8', 'i8', 'f8', 'f8', 'f8', 'f8', 'f8'])
        self.cropYear = self.crop['year']
        self.cropMonth = self.crop['month']
        self.cropHabitat = self.crop['habitat']
        self.cropRice = self.crop['rice']
        self.cropRapeWheat = self.crop['rapeWheat']
        self.cropVegetables = self.crop['vegetables']
        self.cropTrees = self.crop['trees']
        self.cropDryCrop = self.crop['dryCrop']
        self.nCropDat = len(self.cropMonth)

        ###############
        # Run functions
        ################

        self.rainTempArray()
#        self.trapRainDatFX()
        self.captureModelDatFX()
#        self.monthRunReproDatFX()
        self.reproModelRainTemp()

#        self.trapCropDatFX()
#        self.getReproFemales()
#        self.laggedCovariatesFX()
#        if self.params.modelNumber == 1:
#            self.modelFXWrapper()
##        self.trapMorphFX()
#        self.getLaggedRain()
#        self.plotFX(rodentpath)

#        self.getMCMCData()
        self.makeCVPoints()
        self.makeCovariates()
#        self.visualData()
    #################
    # define functions
    #################
    def rainTempArray(self):
        """
        make 2-d rain data array
        """
        self.nyear = len(self.rainYear)
        #empty 2-d array of rainfall
        self.rainDat = np.empty(self.nyear*12)              # data for one habitat only.
        self.temperatureDat = np.empty(self.nyear*12)       # data for one habitat only.
        # array of years
        self.rainYearDat = np.repeat(self.rainYear, 12)
        self.temperatureYearDat = np.repeat(self.rainYear, 12)
        mondat = np.arange(1,13)
        # array of months
        self.rainMonthDat = np.tile(mondat, self.nyear)
        self.rainMonthRun = np.arange(self.minMonthRun, (self.maxMonthRun +1))

        self.temperatureMonthDat = np.tile(mondat, self.nyear)
        for i in range(12):
            istr = str(i + 1)
            # get rain from month i from string headers in columns
            raintmp = self.rain[istr]
            masktmp = self.rainMonthDat == (i + 1)
            self.rainDat[masktmp] = raintmp
            # temperature data
            temperaturetmp = self.temperature[istr]
            self.temperatureDat[masktmp] = temperaturetmp 
#        print('tempdat', self.temperatureDat[:100],
#            'tempYear', self.temperatureYearDat[:100],
#            'tempMonth', self.temperatureMonthDat[:100],
#            'len tempdat: ', len(self.temperatureDat),
#            'len monthrun', len(self.rainMonthRun))

    def trapRainDatFX(self):
        """
        # get rain data for each date in trapping data
        """
        nRainDat = len(self.rainDat)
        self.trapRainDat = np.zeros(self.nTrapDat) # empty array to populate
        self.trapTemperatureDat = np.zeros(self.nTrapDat) # empty array to populate
        for i in range(nRainDat):
            montmp = self.rainMonthDat[i]
            yeartmp = self.rainYearDat[i]
            raintmp = self.rainDat[i]
            masktmp = (self.mo == montmp) & (self.yr == yeartmp)
            self.trapRainDat[masktmp] = raintmp
            temperaturetmp = self.temperatureDat[i]            
            self.trapTemperatureDat[masktmp] = temperaturetmp
#        print('trapraindat', self.trapRainDat[345:], 'len trapRain', len(self.trapRainDat))
#        print('self.mo and yr', self.mo[345:], self.yr[345:])
        
    def captureModelDatFX(self):
        """
        ## get trapped and ntraps, and detection variables for capture model
        ## sort by habitat, year, month
        """
        captureModelMask = self.monthRun >= self.minMonthRun
        habMask1 = self.habitat == 1
        habMask2 = self.habitat == 2
        monHab1Mask = captureModelMask & habMask1
        monHab2Mask = captureModelMask & habMask2
        self.nsessions = np.sum(monHab1Mask)
        self.datSessions = np.tile(np.arange(self.nsessions), 2)
        self.datNTrapped = np.append(self.Apoagr[monHab1Mask], self.Apoagr[monHab2Mask])
        self.datNSetTraps = np.append(self.setTraps[monHab1Mask], self.setTraps[monHab2Mask])
        self.datMonthRun = np.append(self.monthRun[monHab1Mask], self.monthRun[monHab2Mask])
        self.datMonth = np.append(self.mo[monHab1Mask], self.mo[monHab2Mask])
        self.datYear = np.append(self.yr[monHab1Mask], self.yr[monHab2Mask])
        self.datHabitat = np.repeat([0, 1], self.nsessions)   # 0 = lowland and 1 = upland
        self.nDat = len(self.datHabitat)
        self.reproMask = self.datMonthRun >= self.startMonth
        self.t_1ExtractMask = ((self.datMonthRun >= (self.startMonth - 1)) &
                              (self.datMonthRun < self.maxMonthRun))
        self.t_2Mask = self.datMonthRun <= (self.maxMonthRun - 2)
        print('nDat', self.nDat)

    def monthRunReproDatFX(self):
        """
        ## get month Run data for Repro data
        ## sort by habitat, year and month
        """
        self.nReproDat = len(self.reproMonth)
        self.reproMonthRun = np.zeros(self.nReproDat, dtype = int)
        for i in range(self.nReproDat):
            mon_i = self.reproMonth[i]
            year_i = self.reproYear[i]
            trapDatMask = (self.mo == mon_i) & (self.yr == year_i)
            monrun = self.monthRun[trapDatMask][0]
            self.reproMonthRun[i] = monrun
#            if i < self.nReproDat/1.5:
#                print('mon', mon_i, 'year', year_i, 'monRun', monrun)

    def reproModelRainTemp(self):
        """
        ## get rain, temp, nFem capt, and n fem pregnant for pregnant model
        ## sort by habitat, year and month
        ## for monthRun 13 - 360
        """
        self.datRain = np.tile(self.rainDat, 2)
        self.datTemperature = np.tile(self.temperatureDat, 2)
        self.datRice = np.zeros(self.nDat)
        self.datRapeWheat = np.zeros(self.nDat)
        self.datVegetables = np.zeros(self.nDat)
        self.datTrees = np.zeros(self.nDat)
        self.datDryCrop = np.zeros(self.nDat)  
        self.uDatYear = np.unique(self.datYear)
        self.nDatYear = len(self.uDatYear)    
        cc = 0
        ## loop thru habitats
        for i in range(2):
            cropHabMask = self.cropHabitat == i
            ## loop thru years
            for j in self.uDatYear:
                cropYearMask = self.cropYear == j
                ## loop thru months
                for k in range(1, 13):
                    cropMonthMask = self.cropMonth == k
                    maskCrop = cropHabMask & cropYearMask & cropMonthMask
                    ####### Populate crop arrays
                    self.datRice[cc] = self.cropRice[maskCrop]
                    self.datRapeWheat[cc] = self.cropRapeWheat[maskCrop]
                    self.datVegetables[cc] = self.cropVegetables[maskCrop]
                    self.datTrees[cc] = self.cropTrees[maskCrop]
                    self.datDryCrop[cc] = self.cropDryCrop[maskCrop]
                    cc += 1
#                    if cc <440:
#                        print('cc', cc,'hab', i, 'yr', j, 'mon', k, 
#                            'rice', self.datRice[cc -1],
#                            'veg', self.datVegetables[cc - 1])


    def makeCVPoints(self):
        """
        id mon, years, sessions to be held out as cross validation points
        """
        atmp = np.arange(1, 13)
        self.monthCV = np.tile(atmp, 2)
        self.monthCV = self.monthCV[1:]        
        yrtmp = np.arange(1992, 2016)
        yrtmp[yrtmp <= 1994] = yrtmp[yrtmp <= 1994] - 3
        self.yearCV = yrtmp[yrtmp != 2003]
        self.cvDates = []
        for month, year in zip(self.monthCV, self.yearCV):
            date = datetime.date(int(year), int(month), 15)
            self.cvDates.append(date)
        self.nCV = len(self.monthCV)
#        print('ncv', self.nCV, 'mon cv', self.monthCV, 'len yr', (self.yearCV))
#        print('cvDates', self.cvDates)
        self.cvPointMask = np.zeros(self.nDat, dtype = bool)
        for i in np.arange(self.nCV):
            mmask = self.datMonth == self.monthCV[i]
            ymask = self.datYear== self.yearCV[i]
            mymask = mmask & ymask
            self.cvPointMask[mymask] = 1


    def makeCovariates(self):
        """
        make covariates for population model
        """
        self.Xt_1 = np.zeros((self.nsessions, 2))                     # lagged Xt estimates
        # 2-d array of covariates at time t
        temperature = np.log(self.datTemperature + 1.0)
        temperature2 = temperature**2
        temperature3 = temperature**3       
        if self.params.modelNumber == 4:
            temperature[self.reproMask] = temperature[t_2Mask] 
            temperature2[self.reproMask] = temperature2[t_2Mask]

        growthIntercept = np.ones(self.nDat)
        if self.params.modelNumber <= 4:
            ## polynomial weather effect
            self.tArray = np.hstack([np.expand_dims(growthIntercept,1),
#                                np.expand_dims(np.log(self.rainLagCov + 1.0), 1),
                                np.expand_dims(temperature, 1),
                                np.expand_dims(temperature2, 1), 
                                np.expand_dims(self.datVegetables, 1), 
                                np.expand_dims(self.datTrees, 1)])
#                                np.expand_dims(self.dryCrop, 1)])
#                                np.expand_dims(self.rapeWheat, 1),
        elif np.in1d(self.params.modelNumber, [5, 6]):
            ## a single temperature covariate
            self.tArray = np.hstack([np.expand_dims(growthIntercept,1),
#                                np.expand_dims(np.log(self.rainLagCov + 1.0), 1),
                                np.expand_dims(temperature, 1),
                                np.expand_dims(self.datVegetables, 1), 
                                np.expand_dims(self.datTrees, 1)])
#                                np.expand_dims(self.dryCrop, 1)])
#                                np.expand_dims(self.rapeWheat, 1),
        # 2-d array of covariates for detection model
        self.detectIntercept = np.ones(self.nDat)
        lograin = np.log(self.datRain + 1.0)
        lograin2 = lograin**2.0
#        self.detectCovariates = np.hstack([np.expand_dims(self.detectIntercept, 1),
#                                        np.expand_dims(lograin, 1),
#                                        np.expand_dims(lograin2, 1)])
        self.detectCovariates = np.hstack([np.expand_dims(self.detectIntercept, 1),
                                        np.expand_dims(lograin, 1)])
#        print('tArray', self.tArray[:60])



#############################################
#############################################
#############################################


    def getReproFemales(self):
        """
        ## calc proportion of repro females for each calendar month
        """
        self.propReproductive = np.zeros(12)
        pregMask = self.morphNFetus > 0
        self.pregMass = self.morphMass[pregMask]
        pregMassQuants = mquantiles(self.pregMass, prob=[0.025, 0.05, 0.10])
        reproMask = self.morphMass >= pregMassQuants[0]
        print('pregnant mass quants', pregMassQuants)
        for i in range(12):
            monthMask = self.morphMonth == (i + 1)
            femMonthMask = monthMask & self.morphFemaleBool
            nFem = np.sum(femMonthMask)
            nRepro = np.sum(femMonthMask & reproMask)
            self.propReproductive[i] = nRepro / nFem
            print('month', i+1, 'tot fem capt', nFem, 'prop repro', self.propReproductive[i])            
        ## monthly means and sd prop repro
        uyear = np.unique(self.morphYear)
        propRepro_2d = np.zeros((len(uyear), 12))
        print('uyear', uyear)
        for i in range(12):
            monthMask = self.morphMonth == (i + 1)
            femMonthMask = monthMask & self.morphFemaleBool
            for j in range(len(uyear)):
                yearMask = self.morphYear == uyear[j]            
                yearMonthMask = monthMask & yearMask
                femYearMonthMask = yearMonthMask & self.morphFemaleBool                
                nFem = np.sum(femYearMonthMask)
                if nFem == 0:
                    propRepro_2d[j, i] = np.nan
                else:
                    nRepro = np.sum(femYearMonthMask & reproMask)
                    propRepro_2d[j, i] = nRepro / nFem
        self.meanPropRepro = np.nanmean(propRepro_2d, axis = 0)
        self.sdPropRepro = np.nanstd(propRepro_2d, axis = 0)
        for i in range(12):
            print('mon', i, 'mean prop repro', self.meanPropRepro[i], 'sd prop repro', self.sdPropRepro[i])
        nFetusDat = self.morphNFetus[pregMask]
        print('mean, median and sd', np.mean(nFetusDat), np.median(nFetusDat),
            np.std(nFetusDat))



    def trapMorphFX(self):
        """
        # get morph data for each month in trapping data
        """
                # convert string trap types to integers
        self.morphSexInt = np.zeros(len(self.morphYear), dtype = int)
        self.morphSexInt[self.morphSex == b'Female'] = 1

        nMorphDat = len(self.morphYear)
        maskFemale = self.morphSexInt == 1
        self.trapMorphDat = np.empty(self.nTrapDat) # empty array to populate with morph data
        uyear = np.unique(self.morphYear)
        for i in uyear:
            maskYear = self.morphYear == i
            umon = np.unique(self.morphMonth[maskYear])
            for j in umon:
                maskMonth = self.morphMonth == j
                maskYearMon = maskYear & maskMonth
                uhab = np.unique(self.morphHabitat[maskYearMon])
                for k in uhab:      # habitat 1 and 2
                    maskHab = self.morphHabitat == k
                    maskYearMonHabSex = maskYearMon & maskHab & maskFemale
                    femPresent = sum(self.morphSexInt[maskYearMonHabSex])
                    if femPresent > 0:
                        meanMass = np.mean(self.morphMass[maskYearMonHabSex])
                        # make trapDat mask
                        trapDatMask = (self.yr == i) & (self.mo == j) & (self.habitat == k)
                        self.trapMorphDat[trapDatMask] = meanMass
        morphMask = self.trapMorphDat < 5
        self.morphNAN = self.trapMorphDat.copy()
        self.morphNAN[morphMask] = np.nan
#        print('self.morphNAN', self.morphNAN[0:50])

    def subPlotFX(self, iii, ll, covar, covarLabel):
        """
        sub-plot function
        """
        dates = []
        mmm = self.mo[self.habitat == iii]
        yyy = self.yr[self.habitat == iii]
        minDate = datetime.date(1987, 6, 1)
        maxDate = datetime.date(1988, 6, 1)
#        minDate = datetime.date(1987, 1, 1)
#        maxDate = datetime.date(2014, 1, 1)
        for month, year in zip(mmm, yyy):
            date = datetime.date(int(year), int(month), 1)
            dates.append(date)
        tr = self.trapNAN[self.habitat == iii]
#        raindat = self.trapRainDat[self.habitat == iii]
        raindat = covar[self.habitat == iii]
        ax1 = P.subplot(3,1,iii+1)
        lns1 = ax1.plot(dates, tr, color = 'k', label = ll, linewidth = 1.25)
        ax2 = ax1.twinx()
        lns2 = ax2.plot(dates,raindat, color = 'r', label = covarLabel, linewidth = .65)
#        lns2 = ax2.plot(dates,raindat, color = 'g', label = covarLabel, linewidth = .65)
        setTraps = self.setTrapsNAN[self.habitat == iii]
        lns3 = ax2.plot(dates, setTraps, color = 'b', label = 'No. traps', linewidth = 1.0)
        lns = lns1 + lns2 + lns3
        labs = [l.get_label() for l in lns]
        if iii == 0:
            ax1.legend(lns, labs, loc = 'upper right')
        else:
            ax1.legend(loc = 'upper right')            
        ax1.set_ylim([0, 35])
#        ax1.set_ylim([0, 20])
        ax1.set_xlim(minDate, maxDate)
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        if iii == 2:
            ax1.set_xlabel('Time', fontsize = 17)
        ax1.set_ylabel('Trap Rate x 100', fontsize = 17)
#        P.xticks([datetime.date(2009, 1, 1), datetime.date(2011, 1, 1), datetime.date(2013, 1, 1),
#                    datetime.date(2015, 1, 1), datetime.date(2017, 1, 1), datetime.date(2019, 1, 1)])
        ax2.set_ylabel("Rainfall & No. traps", fontsize = 17)  


    def plotFX(self, rodentpath):
        """
        plot pop time series
        """
        P.figure(figsize=(14, 11))
        self.subPlotFX(0, "Dwelling trap rate", self.trapRainDat, "Rainfall")
        self.subPlotFX(1, "Irrigated rice trap rate", self.trapRainDat, "Rainfall")
        self.subPlotFX(2, "Non-irrigated trap rate", self.trapRainDat, "Rainfall")
#        self.subPlotFX(0, "Dwelling trapped", self.trapTemperatureDat, "Temperature")
#        self.subPlotFX(1, "Irrigated rice trapped", self.trapTemperatureDat, "Temperature")
#        self.subPlotFX(2, "Non-irrigated trapped", self.trapTemperatureDat, "Temperature")
#        self.subPlotFX(0, "Dwelling", self.morphNAN, "Mean female mass")
#        self.subPlotFX(1, "Irrigated rice", self.morphNAN, "Mean female mass")
#        self.subPlotFX(2, "Non-irrigated farm", self.morphNAN, "Mean female mass")
        MiceFname = os.path.join(rodentpath,'apoagr_Rain.png')
#        MiceFname = os.path.join(rodentpath,'apoagr_Temperature.png')
        P.savefig(MiceFname, format='png')
        P.show()


    def pengshanPlotFX(self):
        """
        Pengshan plot function
        """
#        self.pengAnoSquTrap
        dates = []
        mmm = self.pengMonth.copy()
        yyy = self.pengYear.copy()
#        minDate = datetime.date(2005, 1, 1)
#        maxDate = datetime.date(2008, 1, 1)
        minDate = datetime.date(1999, 1, 1)
        maxDate = datetime.date(2004, 1, 1)
        for month, year in zip(mmm, yyy):
            date = datetime.date(int(year), int(month), 1)
            dates.append(date)
        sid = np.array(['Apodemus agrarius', 'Anourosorex squamipes'])
        P.subplot(2,1,1)
        self.pengSubPlotFX(0, dates, minDate, maxDate, self.pengApoAgrTrap, sid[0])
        P.subplot(2,1,2)
        self.pengSubPlotFX(1, dates, minDate, maxDate, self.pengAnoSquTrap, sid[1])
        P.show()

    def pengSubPlotFX(self, iii, dates, minDate, maxDate, tr, lll):
        """
        sub-plot function
        """
        P.plot(dates, tr, color = 'k', label = lll, linewidth = 1)

#        P.ylim([0, 140])
        if iii == 1:
            P.xlabel('Time', fontsize = 17)
        P.ylabel('Trap Rate x 100', fontsize = 17)
        P.legend(loc='upper right')
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        P.xlim(minDate, maxDate)

  
    def visualData(self):
        """
        visualise data
        """
        trapset = self.nTrapSetAll * 1.0
        trapset[trapset == 0] = np.nan
        traprate = self.nTrappedAll / trapset
        # mask Model data
        modelMask = np.invert(self.cvPointMask)
        umonths = np.unique(self.monthsAll)
        minCaptModel = np.zeros((12, 2))
        maxCaptModel = np.zeros((12, 2))
        # prediction  masks
        self.trapRatePredArray = np.zeros((2, 12, 2))
        for i in range(2):
            habMask = self.habIndxAll == i
            for j in range(12):
                # get pred data
                monthMask = self.monthsAll == umonths[j]
                maskPred_ij = habMask & monthMask & self.cvPointMask
                self.trapRatePredArray[i, j] = (traprate[maskPred_ij]) * 100.0
                # get model data
                maskModel_ij = habMask & monthMask & modelMask 
                trapRate_ij = traprate[maskModel_ij] 
                minCaptModel[(j-1), i] = np.nanmin(trapRate_ij) * 100.
                maxCaptModel[(j-1), i] = np.nanmax(trapRate_ij) * 100. 
        self.plotTRSeries(minCaptModel, maxCaptModel, umonths)

    def plotTRSeries(self, minCaptModel, maxCaptModel, umonths):
        """
        plot min and max trap rates
        """
        P.figure(figsize=(14, 11))
        for i in range(2):
            ax1 = P.subplot(2, 1, i + 1)
            lns1 = ax1.plot(umonths, minCaptModel[:, i], color = 'b', label = 'min/max traprate*100', linewidth = 3.0)
            lns2 = ax1.plot(umonths, maxCaptModel[:, i], color = 'b', linewidth = 3.0)
            lns3 = ax1.plot(umonths, self.trapRatePredArray[i, :, 0], 'ro', ms = 8.0, label = 'Prediction points')
            lns4 = ax1.plot(umonths, self.trapRatePredArray[i, :, 1], 'ro', ms = 8.0)
            lns = lns1 + lns3
            labs = [l.get_label() for l in lns]
            if i == 0:
                lTit = 'Irrigated'
                ax1.legend(lns, labs, title = lTit, loc = 'upper left')
            else:
                lTit = 'Upland'
                ax1.legend(lns, labs, title = lTit, loc = 'upper left')
#            ax1.set_xlim([-.5, 12.5])
            ax1.set_xlim(-0.5, 12.5)
            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            for tick in ax1.yaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            ax1.set_ylabel('Traprate * 100', fontsize = 17)
            ax1.set_xlabel('Months', fontsize = 17)

    #        P.xticks([datetime.date(2009, 1, 1), datetime.date(2011, 1, 1), datetime.date(2013, 1, 1),
    #                    datetime.date(2015, 1, 1), datetime.date(2017, 1, 1), datetime.date(2019, 1, 1)])
        figFname = 'nCaptPredN.png'
        P.savefig(figFname, format='png')
        P.show()




class ApodemusData(object):
    def __init__(self, rawdata):
        """
        Pickle structure from explore.py
        """
        self.nsessions = rawdata.nsessions      # n sessions per habitat
        self.datSessions = rawdata.datSessions
        self.datNTrapped = rawdata.datNTrapped
        self.datNSetTraps = rawdata.datNSetTraps
        self.datMonthRun = rawdata.datMonthRun  # from 13 up
        self.datMonth = rawdata.datMonth
        self.datYear = rawdata.datYear
        self.datHabitat = rawdata.datHabitat    # 0 = lowland and 1 = upland
        self.datStartMonth = rawdata.startMonth
        self.datMinMonthRun = rawdata.minMonthRun
        self.datMaxMonthRun = rawdata.maxMonthRun
        self.reproMask = rawdata.reproMask
        self.t_1ExtractMask = rawdata.t_1ExtractMask
        self.t_2Mask = rawdata.t_2Mask
        ## repro variables
        self.datNFemales = rawdata.reproFemales
        self.datNPregnant = rawdata.reproNPregnant
        self.datPromTesticle = rawdata.reproPromTesticle
        ## weather and crops
        self.nDat = rawdata.nDat                # total data
        self.datRain = rawdata.datRain
        self.datTemperature = rawdata.datTemperature
        self.datRice = rawdata.datRice
        self.datRapeWheat = rawdata.datRapeWheat
        self.datVegetables = rawdata.datVegetables
        self.datTrees = rawdata.datTrees
        self.datDryCrop = rawdata.datDryCrop
        self.uDatYear = rawdata.uDatYear
        self.nDatYear = rawdata.nDatYear
        # Cross validation data
        self.monthCV = rawdata.monthCV
        self.yearCV = rawdata.yearCV
        self.cvDates = rawdata.cvDates
        self.nCV = rawdata.nCV
        self.cvPointMask = rawdata.cvPointMask
        # Covariates
        self.tArray = rawdata.tArray
        self.detectCovariates = rawdata.detectCovariates






        # pickle apodemusdata to be used in apodemus.py
#        outManipdata = os.path.join(rodentpath,'out_manipdata.pkl')
#        fileobj = open(outManipdata, 'wb')
#        pickle.dump(apodemusdata, fileobj)
#        fileobj.close()




