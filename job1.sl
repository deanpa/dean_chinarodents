#!/bin/bash
#SBATCH -J model1
#SBATCH -A landcare00040
#SBATCH --mail-type=end
#SBATCH --mail-user=andersond@landcareresearch.co.nz
#SBATCH --time=00:59:00
#SBATCH --mem-per-cpu=3000  
#SBATCH -C wm
#SBATCH -o job-%j.%N.out
#SBATCH -e job-%j.%N.err

module load Python-GDAL/2.1.3-gimkl-2017a-GEOS-3.5.1-Python-3.6.0

srun model1.py

#srun startApodemus.py --basicdata=True
 
#srun startApodemus.py


