#!/usr/bin/env python

import os
import numpy as np
import pickle
import pylab as P
import prettytable
import datetime
from apodemus import ApodemusData

# special codes for sex
CODE_fem = 1
CODE_male = 0


class RawData(object):
    def __init__(self, trapFname, rainFname, temperatureFname,
                CropFname, rodentpath):
#    def __init__(self, trapFname, rainFname, temperatureFname, morphFname, 
#                pengshanFname, CropFname, rodentpath):
        """
        Object to read in trap, morph and environmental data
        """
        # rodent capture data, trap id, week, avail etc
        # Apodemus agarius
        self.trap = np.genfromtxt(trapFname,  delimiter=',', names=True,
            dtype=['i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'f8', 'i8','i8','i8','i8',
                    'i8','i8','i8','i8','i8','i8','i8','i8','i8', 'i8','i8'])

        self.trapMask = self.trap['setTraps'] == 0
        self.trapDataMask = self.trap['setTraps'] > 0
#        self.trapRate = self.trap['trapRate100']
        self.nTrapDat = len(self.trapMask)
        print('ntrapdat', self.nTrapDat)
        self.setTraps = self.trap['setTraps']
        self.Apoagr = self.trap['Apodemus_agrarius']
        self.trapRate = np.empty(self.nTrapDat)
        self.trapRate[self.trapDataMask] = self.Apoagr[self.trapDataMask] / self.setTraps[self.trapDataMask] * 100.0
        self.trapNAN = self.trapRate.copy()
        self.trapNAN[self.trapMask] = np.nan
        self.setTrapsNAN = self.setTraps.copy()
        self.setTrapsNAN = self.setTrapsNAN.astype(np.float64)
        self.setTrapsNAN[self.trapMask] = np.nan
        self.yr = self.trap['year']
        self.mo = self.trap['month']
        self.habitat = self.trap['habitat']
        self.monthRun = self.trap['monthRun']
        # rain and temp data from Yuqing
        self.rain = np.genfromtxt(rainFname,  delimiter=',', names=True,
            dtype=['i8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8','f8','f8','f8', 'f8','f8'])
        self.rainYear = self.rain['year']
        self.temperature = np.genfromtxt(temperatureFname,  delimiter=',', names=True,
            dtype=['i8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8','f8','f8','f8', 'f8','f8'])

#        # morph data from Yuqing
#        self.morph = np.genfromtxt(morphFname,  delimiter=',', names=True,
#            dtype=['i8', 'i8', 'i8', 'i8', 'i8', 'S10', 'f8', 'f8','f8','f8','f8', 'i8'])
#        self.morphYear = self.morph['year']
#        self.morphMonth = self.morph['month']
#        self.morphHabitat = self.morph['habitat']
#        self.morphMass = self.morph['mass']
#        self.morphSex = self.morph['sex']

        # croptype data from Yuqing
        self.crop = np.genfromtxt(CropFname,  delimiter=',', names=True,
            dtype=['i8', 'i8', 'i8', 'f8', 'f8', 'f8', 'f8', 'f8'])
        self.cropYear = self.crop['year']
        self.cropMonth = self.crop['month']
        self.cropHabitat = self.crop['habitat']
        self.cropRice = self.crop['rice']
        self.cropRapeWheat = self.crop['rapeWheat']
        self.cropVegetables = self.crop['vegetables']
        self.cropTrees = self.crop['trees']
        self.cropDryCrop = self.crop['dryCrop']

#        # data from Pengshan county, Sichuan Province
#        self.pengTrap = np.genfromtxt(pengshanFname,  delimiter=',', names=True,
#            dtype=['i8', 'i8', 'i8', 'i8', 'f8', 'i8', 'i8', 'i8','i8','i8','i8',
#                    'i8','i8','i8','i8','i8','i8','i8','i8','i8', 'i8','i8','i8', 'i8','i8'])
#        self.pengshanTrapSet = self.pengTrap['trapSet']
#        self.pengApoAgrTrap = (self.pengTrap['apoagr_Female'] + self.pengTrap['apoagr_Male']) / self.pengshanTrapSet
#        self.pengAnoSquTrap = (self.pengTrap['anosqu_Female'] + self.pengTrap['anosqu_Male']) / self.pengshanTrapSet
#        self.pengMonth = self.pengTrap['month']
#        self.pengYear = self.pengTrap['year']

        ###############
        # Run functions
        ################

        self.rainTempArray()
        self.trapRainDatFX()
        self.trapCropDatFX()
#        self.pengshanPlotFX()
#        self.trapMorphFX()
        self.getLaggedRain()
        self.plotFX(rodentpath)

        self.getMCMCData()
        self.makeCVPoints()

        self.visualData()
    #################
    # define functions
    #################
    def rainTempArray(self):
        """
        make 2-d rain data array
        """
        self.nyear = len(self.rainYear)
        #empty 2-d array of rainfall
        self.rainDat = np.empty(self.nyear*12)
        self.temperatureDat = np.empty(self.nyear*12)
        # array of years
        self.rainYearDat = np.repeat(self.rainYear, 12)
        self.temperatureYearDat = np.repeat(self.rainYear, 12)
        mondat = np.arange(1,13)
        # array of months
        self.rainMonthDat = np.tile(mondat, self.nyear)
        self.temperatureMonthDat = np.tile(mondat, self.nyear)
        for i in range(12):
            istr = str(i + 1)
            # get rain from month i from string headers in columns
            raintmp = self.rain[istr]
            masktmp = self.rainMonthDat == (i + 1)
            # rain data
            self.rainDat[masktmp] = raintmp
            # temperature data
            temperaturetmp = self.temperature[istr]
            self.temperatureDat[masktmp] = temperaturetmp 


    def trapRainDatFX(self):
        """
        # get rain data for each date in trapping data
        """
        nRainDat = len(self.rainDat)
        self.trapRainDat = np.zeros(self.nTrapDat) # empty array to populate
        self.trapTemperatureDat = np.zeros(self.nTrapDat) # empty array to populate

        for i in range(nRainDat):
            montmp = self.rainMonthDat[i]
            yeartmp = self.rainYearDat[i]
            raintmp = self.rainDat[i]
            masktmp = (self.mo == montmp) & (self.yr == yeartmp)
            self.trapRainDat[masktmp] = raintmp
            temperaturetmp = self.temperatureDat[i]            
            self.trapTemperatureDat[masktmp] = temperaturetmp
#        print('trapraindat', self.trapRainDat[345:])
#        print('self.mo and yr', self.mo[345:], self.yr[345:])


    def trapCropDatFX(self):
        # get crop data corresponding to trapping data
        nCropDat = len(self.cropYear)
        self.trapCropRice = np.zeros(self.nTrapDat)
        self.trapCropRapeWheat = np.zeros(self.nTrapDat)
        self.trapCropVegetables = np.zeros(self.nTrapDat)
        self.trapCropTrees = np.zeros(self.nTrapDat)
        self.trapCropDryCrop = np.zeros(self.nTrapDat)
        for i in range(nCropDat):
            montmp = self.cropMonth[i]
            yeartmp = self.cropYear[i]
            habtmp = self.cropHabitat[i] + 1
            masktmp = ((self.mo == montmp) & (self.yr == yeartmp) & 
                    (self.habitat == habtmp)) 
            self.trapCropRice[masktmp] = self.cropRice[i]
            self.trapCropRapeWheat[masktmp] = self.cropRapeWheat[i]
            self.trapCropVegetables[masktmp] = self.cropVegetables[i]
            self.trapCropTrees[masktmp] = self.cropTrees[i]
            self.trapCropDryCrop[masktmp] = self.cropDryCrop[i]

#            print('i', i, 'mon', montmp, 'yr', yeartmp, 'hab', habtmp, 'summask', np.sum(masktmp),
#                    'trapveg', self.trapCropVegetables[masktmp])

    def getLaggedRain(self):
        """
        get rain in previous months
        """
        self.Raint_1 = np.zeros(self.nTrapDat)
        self.rainT10_8 = np.zeros(self.nTrapDat)
        self.tempT10_8 = np.zeros(self.nTrapDat)
        self.rainT9_7 = np.zeros(self.nTrapDat)
        self.tempT9_7 = np.zeros(self.nTrapDat)
        self.rainT8_6 = np.zeros(self.nTrapDat)
        self.tempT8_6 = np.zeros(self.nTrapDat)
        self.rainT7_5 = np.zeros(self.nTrapDat)
        self.tempT7_5 = np.zeros(self.nTrapDat)
        self.rainT6_4 = np.zeros(self.nTrapDat)
        self.tempT6_4 = np.zeros(self.nTrapDat)
        self.rainT5_3 = np.zeros(self.nTrapDat)
        self.tempT5_3 = np.zeros(self.nTrapDat)
        self.rainT4_2 = np.zeros(self.nTrapDat)
        self.tempT4_2 = np.zeros(self.nTrapDat)
        self.rainT3_1 = np.zeros(self.nTrapDat)
        self.tempT3_1 = np.zeros(self.nTrapDat)

        maxMonRun = np.max(self.monthRun)
        print('maxMonRun', maxMonRun)
        self.minMonRunAll = 13      
        self.maskT10_8 = self.monthRun >= (self.minMonRunAll + 10)
        self.maskT9_7 = self.monthRun >= (self.minMonRunAll + 9)
        self.maskT8_6 = self.monthRun >= (self.minMonRunAll + 8)
        self.maskT7_5 = self.monthRun >= (self.minMonRunAll + 7)
        self.maskT6_4 = self.monthRun >= (self.minMonRunAll + 6)
        self.maskT5_3 = self.monthRun >= (self.minMonRunAll + 5)
        self.maskT4_2 = self.monthRun >= (self.minMonRunAll + 4)
        self.maskT3_1 = self.monthRun >= (self.minMonRunAll + 3)

        # habitat 1 irrigated mask
        habitatMask = self.habitat == 1
        # start on second month with data
        for i in range((self.minMonRunAll + 1), (maxMonRun + 1)):
            trapRainDatTmp = self.trapRainDat[self.monthRun == (i-1)]
            self.Raint_1[self.monthRun == i] = trapRainDatTmp[0]
            fillInMask = self.monthRun == i

            # make rain and temp variable lagged 10 - 8 months  
            if i >= self.minMonRunAll + 10:        
                monRunSubset = np.arange((i - 10), (i - 8))
                maskTmp = np.in1d(self.monthRun, monRunSubset)
                maskFullTmp = maskTmp &  habitatMask
                self.rainT10_8[fillInMask] = np.mean(self.trapRainDat[maskFullTmp])
                self.tempT10_8[fillInMask] = np.mean(self.trapTemperatureDat[maskFullTmp])

            # make rain and temp variable lagged 9 - 7 months  
            if i >= self.minMonRunAll + 9:        
                monRunSubset = np.arange((i - 9), (i - 6))
                maskTmp = np.in1d(self.monthRun, monRunSubset)
                maskFullTmp = maskTmp &  habitatMask
                self.rainT9_7[fillInMask] = np.mean(self.trapRainDat[maskFullTmp])
                self.tempT9_7[fillInMask] = np.mean(self.trapTemperatureDat[maskFullTmp])
#                if i > 357:
#                    print('monRun', i, 'mons', monRunSubset, 'TempDat[mask]', self.trapTemperatureDat[maskFullTmp], 'tempT9_7', self.tempT9_7[fillInMask])

            # make rain and temp variable lagged 8 - 6 months  
            if i >= self.minMonRunAll + 8:        
                monRunSubset = np.arange((i - 8), (i - 5))
                maskTmp = np.in1d(self.monthRun, monRunSubset)
                maskFullTmp = maskTmp &  habitatMask
                self.rainT8_6[fillInMask] = np.mean(self.trapRainDat[maskFullTmp])
                self.tempT8_6[fillInMask] = np.mean(self.trapTemperatureDat[maskFullTmp])
            # make rain and temp variable lagged 7 - 5 months  
            if i >= self.minMonRunAll + 7:        
                monRunSubset = np.arange((i - 7), (i - 4))
                maskTmp = np.in1d(self.monthRun, monRunSubset)
                maskFullTmp = maskTmp &  habitatMask
                self.rainT7_5[fillInMask] = np.mean(self.trapRainDat[maskFullTmp])
                self.tempT7_5[fillInMask] = np.mean(self.trapTemperatureDat[maskFullTmp])
            # make rain and temp variable lagged 6 - 4 months  
            if i >= self.minMonRunAll + 6:        
                monRunSubset = np.arange((i - 6), (i - 3))
                maskTmp = np.in1d(self.monthRun, monRunSubset)
                maskFullTmp = maskTmp &  habitatMask
                self.rainT6_4[fillInMask] = np.mean(self.trapRainDat[maskFullTmp])
                self.tempT6_4[fillInMask] = np.mean(self.trapTemperatureDat[maskFullTmp])
            # make rain and temp variable lagged 5 - 3 months  
            if i >= self.minMonRunAll + 5:        
                monRunSubset = np.arange((i - 5), (i - 2))
                maskTmp = np.in1d(self.monthRun, monRunSubset)
                maskFullTmp = maskTmp &  habitatMask
                self.rainT5_3[fillInMask] = np.mean(self.trapRainDat[maskFullTmp])
                self.tempT5_3[fillInMask] = np.mean(self.trapTemperatureDat[maskFullTmp])
            # make rain and temp variable lagged 4 - 2 months  
            if i >= self.minMonRunAll + 4:        
                monRunSubset = np.arange((i - 4), (i - 1))
                maskTmp = np.in1d(self.monthRun, monRunSubset)
                maskFullTmp = maskTmp &  habitatMask
                self.rainT4_2[fillInMask] = np.mean(self.trapRainDat[maskFullTmp])
                self.tempT4_2[fillInMask] = np.mean(self.trapTemperatureDat[maskFullTmp])
            # make rain and temp variable lagged 3 - 1 months  
            if i >= self.minMonRunAll + 3:        
                monRunSubset = np.arange((i - 3), i)
                maskTmp = np.in1d(self.monthRun, monRunSubset)
                maskFullTmp = maskTmp &  habitatMask
                self.rainT3_1[fillInMask] = np.mean(self.trapRainDat[maskFullTmp])
                self.tempT3_1[fillInMask] = np.mean(self.trapTemperatureDat[maskFullTmp])
#                print('monRun', i, 'mons', monRunSubset, 'TempDat[mask]', self.trapTemperatureDat[maskFullTmp], 'tempT3_1', self.tempT3_1[fillInMask])


    def trapMorphFX(self):
        """
        # get morph data for each month in trapping data
        """
                # convert string trap types to integers
        self.morphSexInt = np.zeros(len(self.morphYear), dtype = int)
        self.morphSexInt[self.morphSex == b'Female'] = 1

        nMorphDat = len(self.morphYear)
        maskFemale = self.morphSexInt == 1
        self.trapMorphDat = np.empty(self.nTrapDat) # empty array to populate with morph data
        uyear = np.unique(self.morphYear)
        for i in uyear:
            maskYear = self.morphYear == i
            umon = np.unique(self.morphMonth[maskYear])
            for j in umon:
                maskMonth = self.morphMonth == j
                maskYearMon = maskYear & maskMonth
                uhab = np.unique(self.morphHabitat[maskYearMon])
                for k in uhab:      # habitat 1 and 2
                    maskHab = self.morphHabitat == k
                    maskYearMonHabSex = maskYearMon & maskHab & maskFemale
                    femPresent = sum(self.morphSexInt[maskYearMonHabSex])
                    if femPresent > 0:
                        meanMass = np.mean(self.morphMass[maskYearMonHabSex])
                        # make trapDat mask
                        trapDatMask = (self.yr == i) & (self.mo == j) & (self.habitat == k)
                        self.trapMorphDat[trapDatMask] = meanMass
        morphMask = self.trapMorphDat < 5
        self.morphNAN = self.trapMorphDat.copy()
        self.morphNAN[morphMask] = np.nan
#        print('self.morphNAN', self.morphNAN[0:50])


    def subPlotFX(self, iii, ll, covar, covarLabel):
        """
        sub-plot function
        """
        dates = []
        mmm = self.mo[self.habitat == iii]
        yyy = self.yr[self.habitat == iii]
        minDate = datetime.date(1987, 6, 1)
        maxDate = datetime.date(1988, 6, 1)
#        minDate = datetime.date(1987, 1, 1)
#        maxDate = datetime.date(2014, 1, 1)
        for month, year in zip(mmm, yyy):
            date = datetime.date(int(year), int(month), 1)
            dates.append(date)
        tr = self.trapNAN[self.habitat == iii]
#        raindat = self.trapRainDat[self.habitat == iii]
        raindat = covar[self.habitat == iii]
        ax1 = P.subplot(3,1,iii+1)
        lns1 = ax1.plot(dates, tr, color = 'k', label = ll, linewidth = 1.25)
        ax2 = ax1.twinx()
        lns2 = ax2.plot(dates,raindat, color = 'r', label = covarLabel, linewidth = .65)
#        lns2 = ax2.plot(dates,raindat, color = 'g', label = covarLabel, linewidth = .65)
        setTraps = self.setTrapsNAN[self.habitat == iii]
        lns3 = ax2.plot(dates, setTraps, color = 'b', label = 'No. traps', linewidth = 1.0)
        lns = lns1 + lns2 + lns3
        labs = [l.get_label() for l in lns]
        if iii == 0:
            ax1.legend(lns, labs, loc = 'upper right')
        else:
            ax1.legend(loc = 'upper right')            
        ax1.set_ylim([0, 35])
#        ax1.set_ylim([0, 20])
        ax1.set_xlim(minDate, maxDate)
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        if iii == 2:
            ax1.set_xlabel('Time', fontsize = 17)
        ax1.set_ylabel('Trap Rate x 100', fontsize = 17)
#        P.xticks([datetime.date(2009, 1, 1), datetime.date(2011, 1, 1), datetime.date(2013, 1, 1),
#                    datetime.date(2015, 1, 1), datetime.date(2017, 1, 1), datetime.date(2019, 1, 1)])
        ax2.set_ylabel("Rainfall & No. traps", fontsize = 17)  


    def plotFX(self, rodentpath):
        """
        plot pop time series
        """
        P.figure(figsize=(14, 11))
        self.subPlotFX(0, "Dwelling trap rate", self.trapRainDat, "Rainfall")
        self.subPlotFX(1, "Irrigated rice trap rate", self.trapRainDat, "Rainfall")
        self.subPlotFX(2, "Non-irrigated trap rate", self.trapRainDat, "Rainfall")
#        self.subPlotFX(0, "Dwelling trapped", self.trapTemperatureDat, "Temperature")
#        self.subPlotFX(1, "Irrigated rice trapped", self.trapTemperatureDat, "Temperature")
#        self.subPlotFX(2, "Non-irrigated trapped", self.trapTemperatureDat, "Temperature")
#        self.subPlotFX(0, "Dwelling", self.morphNAN, "Mean female mass")
#        self.subPlotFX(1, "Irrigated rice", self.morphNAN, "Mean female mass")
#        self.subPlotFX(2, "Non-irrigated farm", self.morphNAN, "Mean female mass")
        MiceFname = os.path.join(rodentpath,'apoagr_Rain.png')
#        MiceFname = os.path.join(rodentpath,'apoagr_Temperature.png')
        P.savefig(MiceFname, format='png')
        P.show()


    def pengshanPlotFX(self):
        """
        Pengshan plot function
        """
#        self.pengAnoSquTrap
        dates = []
        mmm = self.pengMonth.copy()
        yyy = self.pengYear.copy()
#        minDate = datetime.date(2005, 1, 1)
#        maxDate = datetime.date(2008, 1, 1)
        minDate = datetime.date(1999, 1, 1)
        maxDate = datetime.date(2004, 1, 1)
        for month, year in zip(mmm, yyy):
            date = datetime.date(int(year), int(month), 1)
            dates.append(date)
        sid = np.array(['Apodemus agrarius', 'Anourosorex squamipes'])
        P.subplot(2,1,1)
        self.pengSubPlotFX(0, dates, minDate, maxDate, self.pengApoAgrTrap, sid[0])
        P.subplot(2,1,2)
        self.pengSubPlotFX(1, dates, minDate, maxDate, self.pengAnoSquTrap, sid[1])
        P.show()

    def pengSubPlotFX(self, iii, dates, minDate, maxDate, tr, lll):
        """
        sub-plot function
        """
        P.plot(dates, tr, color = 'k', label = lll, linewidth = 1)

#        P.ylim([0, 140])
        if iii == 1:
            P.xlabel('Time', fontsize = 17)
        P.ylabel('Trap Rate x 100', fontsize = 17)
        P.legend(loc='upper right')
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        P.xlim(minDate, maxDate)

    ##############################
    ##############################
    #   Organise data for mcmc
    #

    def getMCMCData(self):
        """
        get months and years and sessions for each data entry
        """
        yrMask = (self.yr > 1986)
        Mask1 = (self.habitat == 1) & yrMask
        Mask2 = (self.habitat == 2) & yrMask
        Mask12 = (self.habitat > 0) & yrMask
        self.habMask1 = Mask1[Mask12]
        self.habMask2 = Mask2[Mask12]
        self.monthsAll = np.append(self.mo[Mask1], self.mo[Mask2])
        self.yearsAll = np.append(self.yr[Mask1], self.yr[Mask2])
        self.nsessionsAll = np.sum(self.habMask1)
        self.sessionsAll = np.tile(np.arange(self.nsessionsAll), 2)
        self.nDatAll = len(self.yearsAll)
        self.nTrappedAll = np.append(self.Apoagr[Mask1], self.Apoagr[Mask2])
        self.habIndxAll = np.zeros(self.nDatAll, dtype = int)
        self.habIndxAll[self.nsessionsAll:] = 1 
        self.nTrapSetAll = np.append(self.setTraps[Mask1], self.setTraps[Mask2])        
        self.rainDatAll = np.append(self.trapRainDat[Mask1], self.trapRainDat[Mask2])
        self.temperatureDatAll = np.append(self.trapTemperatureDat[Mask1], self.trapTemperatureDat[Mask2])
        self.Raint_1All = np.append(self.Raint_1[Mask1], self.Raint_1[Mask2])
        self.monthRunAll = np.append(self.monthRun[Mask1], self.monthRun[Mask2])
        self.riceAll = np.append(self.trapCropRice[Mask1], self.trapCropRice[Mask2])
        self.rapeWheatAll = np.append(self.trapCropRapeWheat[Mask1], self.trapCropRapeWheat[Mask2])
        self.vegetablesAll = np.append(self.trapCropVegetables[Mask1], self.trapCropVegetables[Mask2])
        self.treesAll = np.append(self.trapCropTrees[Mask1], self.trapCropTrees[Mask2])
        self.dryCropAll = np.append(self.trapCropDryCrop[Mask1], self.trapCropDryCrop[Mask2])
        # update lagged covariate data
        self.rainT10_8All = np.append(self.rainT10_8[Mask1], self.rainT10_8[Mask2]) 
        self.tempT10_8All = np.append(self.tempT10_8[Mask1], self.tempT10_8[Mask2])
        self.maskT10_8All = np.append(self.maskT10_8[Mask1], self.maskT10_8[Mask2])
        self.rainT9_7All = np.append(self.rainT9_7[Mask1], self.rainT9_7[Mask2]) 
        self.tempT9_7All = np.append(self.tempT9_7[Mask1], self.tempT9_7[Mask2])
        self.maskT9_7All = np.append(self.maskT9_7[Mask1], self.maskT9_7[Mask2])
        self.rainT8_6All = np.append(self.rainT8_6[Mask1], self.rainT8_6[Mask2]) 
        self.tempT8_6All = np.append(self.tempT8_6[Mask1], self.tempT8_6[Mask2])
        self.maskT8_6All = np.append(self.maskT8_6[Mask1], self.maskT8_6[Mask2])
        self.rainT7_5All = np.append(self.rainT7_5[Mask1], self.rainT7_5[Mask2]) 
        self.tempT7_5All = np.append(self.tempT7_5[Mask1], self.tempT7_5[Mask2])
        self.maskT7_5All = np.append(self.maskT7_5[Mask1], self.maskT7_5[Mask2])
        self.rainT6_4All = np.append(self.rainT6_4[Mask1], self.rainT6_4[Mask2]) 
        self.tempT6_4All = np.append(self.tempT6_4[Mask1], self.tempT6_4[Mask2])
        self.maskT6_4All = np.append(self.maskT6_4[Mask1], self.maskT6_4[Mask2])
        self.rainT5_3All = np.append(self.rainT5_3[Mask1], self.rainT5_3[Mask2]) 
        self.tempT5_3All = np.append(self.tempT5_3[Mask1], self.tempT5_3[Mask2])
        self.maskT5_3All = np.append(self.maskT5_3[Mask1], self.maskT5_3[Mask2])
        self.rainT4_2All = np.append(self.rainT4_2[Mask1], self.rainT4_2[Mask2]) 
        self.tempT4_2All = np.append(self.tempT4_2[Mask1], self.tempT4_2[Mask2])
        self.maskT4_2All = np.append(self.maskT4_2[Mask1], self.maskT4_2[Mask2])
        self.rainT3_1All = np.append(self.rainT3_1[Mask1], self.rainT3_1[Mask2]) 
        self.tempT3_1All = np.append(self.tempT3_1[Mask1], self.tempT3_1[Mask2])
        self.maskT3_1All = np.append(self.maskT3_1[Mask1], self.maskT3_1[Mask2])


    def makeCVPoints(self):
        """
        id mon, years, sessions to be held out as cross validation points
        """
        atmp = np.arange(1, 13)
        self.monthCV = np.tile(atmp, 2)
        self.monthCV = self.monthCV[1:]
        yrtmp = np.arange(1992, 2016)
        yrtmp[yrtmp <= 1994] = yrtmp[yrtmp <= 1994] - 3
        self.yearCV = yrtmp[yrtmp != 2003]
        self.cvDates = []
        for month, year in zip(self.monthCV, self.yearCV):
            date = datetime.date(int(year), int(month), 15)
            self.cvDates.append(date)
        self.nCV = len(self.monthCV)
        print('ncv', self.nCV, 'mon cv', self.monthCV, 'len yr', (self.yearCV))
        print('cvDates', self.cvDates)
        self.cvPointMask = np.zeros(self.nDatAll, dtype = bool)
        for i in np.arange(self.nCV):
            mmask = self.monthsAll == self.monthCV[i]
            ymask = self.yearsAll == self.yearCV[i]
            mymask = mmask & ymask
            self.cvPointMask[mymask] = 1

    def visualData(self):
        """
        visualise data
        """
        trapset = self.nTrapSetAll * 1.0
        trapset[trapset == 0] = np.nan
        traprate = self.nTrappedAll / trapset
        # mask Model data
        modelMask = np.invert(self.cvPointMask)
        umonths = np.unique(self.monthsAll)
        minCaptModel = np.zeros((12, 2))
        maxCaptModel = np.zeros((12, 2))
        # prediction  masks
        self.trapRatePredArray = np.zeros((2, 12, 2))
        for i in range(2):
            habMask = self.habIndxAll == i
            for j in range(12):
                # get pred data
                monthMask = self.monthsAll == umonths[j]
                maskPred_ij = habMask & monthMask & self.cvPointMask
                self.trapRatePredArray[i, j] = (traprate[maskPred_ij]) * 100.0
                # get model data
                maskModel_ij = habMask & monthMask & modelMask 
                trapRate_ij = traprate[maskModel_ij] 
                minCaptModel[(j-1), i] = np.nanmin(trapRate_ij) * 100.
                maxCaptModel[(j-1), i] = np.nanmax(trapRate_ij) * 100. 
        self.plotTRSeries(minCaptModel, maxCaptModel, umonths)

    def plotTRSeries(self, minCaptModel, maxCaptModel, umonths):
        """
        plot min and max trap rates
        """
        P.figure(figsize=(14, 11))
        for i in range(2):
            ax1 = P.subplot(2, 1, i + 1)
            lns1 = ax1.plot(umonths, minCaptModel[:, i], color = 'b', label = 'min/max traprate*100', linewidth = 3.0)
            lns2 = ax1.plot(umonths, maxCaptModel[:, i], color = 'b', linewidth = 3.0)
            lns3 = ax1.plot(umonths, self.trapRatePredArray[i, :, 0], 'ro', ms = 8.0, label = 'Prediction points')
            lns4 = ax1.plot(umonths, self.trapRatePredArray[i, :, 1], 'ro', ms = 8.0)
            lns = lns1 + lns3
            labs = [l.get_label() for l in lns]
            if i == 0:
                lTit = 'Irrigated'
                ax1.legend(lns, labs, title = lTit, loc = 'upper left')
            else:
                lTit = 'Upland'
                ax1.legend(lns, labs, title = lTit, loc = 'upper left')
#            ax1.set_xlim([-.5, 12.5])
            ax1.set_xlim(-0.5, 12.5)
            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            for tick in ax1.yaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            ax1.set_ylabel('Traprate * 100', fontsize = 17)
            ax1.set_xlabel('Months', fontsize = 17)

    #        P.xticks([datetime.date(2009, 1, 1), datetime.date(2011, 1, 1), datetime.date(2013, 1, 1),
    #                    datetime.date(2015, 1, 1), datetime.date(2017, 1, 1), datetime.date(2019, 1, 1)])
        figFname = 'nCaptPredN.png'
        P.savefig(figFname, format='png')
        P.show()




########            Main function
#######
def main():
    rodentpath = os.getenv('CHINAPROJDIR', default = '.')
    # paths and data to read in
    TrapFile = os.path.join(rodentpath,'YuqingCommunity.csv')
    RainFile = os.path.join(rodentpath,'rainYuqing.csv')
    TemperatureFile = os.path.join(rodentpath,'tempYuqing.csv')
#    MorphFile = os.path.join(rodentpath,'Morph_Apodemus_agrarius.csv')
    CropFile = os.path.join(rodentpath,'cropType_Deng.csv')
#    # data from Pengshan
#    PengshanFile = os.path.join(rodentpath,'PengshanSichuan.csv')

    # initiate rawdata class and object
#    rawdata = RawData(TrapFile, RainFile, TemperatureFile, MorphFile, 
#            PengshanFile, CropFile, rodentpath)
    rawdata = RawData(TrapFile, RainFile, TemperatureFile, 
            CropFile, rodentpath)

    apodemusdata = ApodemusData(rawdata)

    # pickle apodemusdata to be used in apodemus.py
    outManipdata = os.path.join(rodentpath,'out_manipdata.pkl')
    fileobj = open(outManipdata, 'wb')
    pickle.dump(apodemusdata, fileobj)
    fileobj.close()



if __name__ == '__main__':
    main()



