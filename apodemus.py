#!/usr/bin/env python

import os
from scipy import stats
from scipy.special import gammaln
from scipy.special import gamma
import numpy as np
#from numba import jit
import params
import pickle
import pylab as P


def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))


class BasicData(object):
    def __init__(self, apodemusdata, params):
        """
        Object to read in apodemus data
        Import updatable params from params
        """
        #############################################
        ############ Run basicdata functions
        self.getParams(params)
        self.getVariables(apodemusdata)
        self.makeLaggedData()
#        self.make2D_DataArrays()
        self.makeCovariates()
        self.getInitialXt()
        self.getInitialDetection()
        ############## End run of basicdata functions
        #############################################

    def getParams(self, params):
        """
        # move params parameters into basicdata
        """
        self.params = params
        self.sigma = self.params.sigma
        self.sigmaSqrt = np.sqrt(self.sigma)
#        self.a = self.params.a                  # pop growth by season and hab
        self.b = self.params.b                     # density dependence by hab
#        self.c = self.params.c                                        # rain coefficient in pop model
        self.alpha = self.params.alpha 
        self.g = self.params.g                # covariates on detection (Rain)
 
    def getVariables(self, apodemusdata):
        """
        get variables from apodemusdata
        """ 
        # move apodemusdata variables into basicdata
        self.apodemusdata = apodemusdata
        # move variables
        self.nsessionsAll = self.apodemusdata.nsessionsAll
        self.sessionsAll = self.apodemusdata.sessionsAll
        self.nDatAll = self.apodemusdata.nDatAll
        self.nTrappedAll = self.apodemusdata.nTrappedAll
        self.habIndxAll = self.apodemusdata.habIndxAll        # 0 = irrigated; 1 = non-irregated
        self.habMask0All = self.habIndxAll == 0
        self.habMask1All = self.habIndxAll == 1
        self.nTrapSetAll = self.apodemusdata.nTrapSetAll
        self.rainDatAll = self.apodemusdata.rainDatAll                    # [self.habIndx == 0]
        self.temperatureDatAll = self.apodemusdata.temperatureDatAll      # [self.habIndx == 0]  
        self.Raint_1All = self.apodemusdata.Raint_1All                    # [self.habIndx == 0]
        self.monthsAll = self.apodemusdata.monthsAll
        self.yearsAll = self.apodemusdata.yearsAll
        self.monthRunAll = self.apodemusdata.monthRunAll                  # [self.habIndx == 0]
        self.minMonRunAll = self.apodemusdata.minMonRunAll
        # cropping data
        self.riceAll = self.apodemusdata.riceAll
        self.rapeWheatAll = self.apodemusdata.rapeWheatAll
        self.vegetablesAll = self.apodemusdata.vegetablesAll
        self.treesAll = self.apodemusdata.treesAll
        self.dryCropAll = self.apodemusdata.dryCropAll
        # weather data and masks lagged
        self.rainT10_8All = self.apodemusdata.rainT10_8All
        self.tempT10_8All = self.apodemusdata.tempT10_8All
        self.maskT10_8All = self.apodemusdata.maskT10_8All
        self.rainT9_7All = self.apodemusdata.rainT9_7All
        self.tempT9_7All = self.apodemusdata.tempT9_7All
        self.maskT9_7All = self.apodemusdata.maskT9_7All
        self.rainT8_6All = self.apodemusdata.rainT8_6All
        self.tempT8_6All = self.apodemusdata.tempT8_6All
        self.maskT8_6All = self.apodemusdata.maskT8_6All
        self.rainT7_5All = self.apodemusdata.rainT7_5All
        self.tempT7_5All = self.apodemusdata.tempT7_5All
        self.maskT7_5All = self.apodemusdata.maskT7_5All
        self.rainT6_4All = self.apodemusdata.rainT6_4All
        self.tempT6_4All = self.apodemusdata.tempT6_4All
        self.maskT6_4All = self.apodemusdata.maskT6_4All
        self.rainT5_3All = self.apodemusdata.rainT5_3All
        self.tempT5_3All = self.apodemusdata.tempT5_3All
        self.maskT5_3All = self.apodemusdata.maskT5_3All
        self.rainT4_2All = self.apodemusdata.rainT4_2All
        self.tempT4_2All = self.apodemusdata.tempT4_2All
        self.maskT4_2All = self.apodemusdata.maskT4_2All
        self.rainT3_1All = self.apodemusdata.rainT3_1All
        self.tempT3_1All = self.apodemusdata.tempT3_1All
        self.maskT3_1All = self.apodemusdata.maskT3_1All
        # crossvalidation data
        self.monthCV = self.apodemusdata.monthCV
        self.yearCV = self.apodemusdata.yearCV
        self.cvDates = self.apodemusdata.cvDates
        self.nCV = self.apodemusdata.nCV
        self.cvPointMask = self.apodemusdata.cvPointMask


    def makeLaggedData(self):
        """
        make lagged data from lag variables from params
        """
        self.lagMaskDict = {'self.maskT10_8All' : self.maskT10_8All,
            'self.maskT9_7All' : self.maskT9_7All,
            'self.maskT8_6All' : self.maskT8_6All, 
            'self.maskT7_5All' : self.maskT7_5All, 
            'self.maskT6_4All' : self.maskT6_4All, 'self.maskT5_3All' : self.maskT5_3All, 
            'self.maskT4_2All' : self.maskT4_2All, 'self.maskT3_1All' : self.maskT3_1All}
        self.lagRainDict = {'self.rainT10_8All' : self.rainT10_8All,
            'self.rainT9_7All' : self.rainT9_7All,
            'self.rainT8_6All' : self.rainT8_6All, 
            'self.rainT7_5All' : self.rainT7_5All, 
            'self.rainT6_4All' : self.rainT6_4All, 'self.rainT5_3All' : self.rainT5_3All, 
            'self.rainT4_2All' : self.rainT4_2All, 'self.rainT3_1All' : self.rainT3_1All}
        self.lagTempDict = {'self.tempT10_8All' : self.tempT10_8All,
            'self.tempT9_7All' : self.tempT9_7All,
            'self.tempT8_6All' : self.tempT8_6All, 
            'self.tempT7_5All' : self.tempT7_5All, 
            'self.tempT6_4All' : self.tempT6_4All, 'self.tempT5_3All' : self.tempT5_3All, 
            'self.tempT4_2All' : self.tempT4_2All, 'self.tempT3_1All' : self.tempT3_1All}
        self.lagCovMaskAll = self.lagMaskDict[self.params.maskTrapDatName]
        self.rainLagCovAll = self.lagRainDict[self.params.rainCovName]
        self.tempLagCovAll = self.lagTempDict[self.params.tempCovName]
        # mask out data for analysis
        self.maskOutData()

    def maskOutData(self):
        """
        mask out data from lagging covariates
        """
        # mask out last year, to be used for prediction
###        maskYear = self.yearsAll < self.params.predictYear
        #update lagCovMask
        self.lagCovMask = self.lagCovMaskAll.copy()
        self.rainLagCov = self.rainLagCovAll[self.lagCovMask]
        self.tempLagCov = self.tempLagCovAll[self.lagCovMask]
        self.nDat = len(self.rainLagCov)
        self.months = self.monthsAll[self.lagCovMask]
        self.years = self.yearsAll[self.lagCovMask]
        self.Raint_1 = self.Raint_1All[self.lagCovMask]
        self.rice = self.riceAll[self.lagCovMask]
        self.vegetables = self.vegetablesAll[self.lagCovMask]
        self.rapeWheat = self.rapeWheatAll[self.lagCovMask]
        self.trees = self.treesAll[self.lagCovMask]
        self.dryCrop = self.dryCropAll[self.lagCovMask]
        self.rainDat = self.rainDatAll[self.lagCovMask]
        self.temperatureDat = self.temperatureDatAll[self.lagCovMask]
        self.monthRun = self.monthRunAll[self.lagCovMask]
        self.sessions = self.sessionsAll[self.lagCovMask]
        self.nsessions = len(np.unique(self.sessions))
        self.minSession = np.min(self.sessions)
        self.maxSession = np.max(self.sessions)
#        sessTmp = np.arange(self.nsessions)
#        self.sessions = np.tile(sessTmp, 2)
        self.nTrapped = self.nTrappedAll.copy()             # ntrapped for analysis
        self.nTrapped[self.cvPointMask] = 0                 # make cv pt zero
        self.nTrapped = self.nTrapped[self.lagCovMask]      # mask out data
        self.habIndx = self.habIndxAll[self.lagCovMask]
        self.nTrapSet = self.nTrapSetAll.copy()
        self.nTrapSet[self.cvPointMask] = 0
        self.nTrapSet = self.nTrapSet[self.lagCovMask]

#        print('lagCovMask', self.lagCovMask[:30], 'lmask2', self.lagCovMask[-5:], 
#            'len mask', len(self.lagCovMask), 'sum Mask', np.sum(self.lagCovMask))

#        print('nDat', self.nDat, 'nsess', self.nsessions, 
#            'nsessAll', len(self.sessionsAll))
            
#        print('len ntrap', len(self.nTrapped), 'ntrapped', self.nTrappedAll[-10:])  
#        print('cvpt', len(self.cvPointMask), len(self.lagCovMask))
#        print('trapset', self.nTrapSet[self.cvPointMask], 'trapped', self.nTrapped[self.cvPointMask])



    def makeCovariates(self):
        """
        make covariates for population model
        """
#        self.irr = self.habIndx.copy()
#        self.nonIrr = 1 - self.habIndx          # indx for non irrigated
        self.sin1 = np.sin(2.0 * np.pi * self.months / 12.0)
        self.cos1 = np.cos(2.0 * np.pi * self.months / 12.0)
        self.sin2 = np.sin(4.0 * np.pi * self.months / 12.0)
        self.cos2 = np.cos(4.0 * np.pi * self.months / 12.0)
        self.Xt_1 = np.zeros((self.nsessions, 2))                     # lagged Xt estimates
        # 2-d array of covariates at time t
        growthIntercept = np.ones(self.nDat)
        self.tArray = np.hstack([np.expand_dims(growthIntercept,1),
                                np.expand_dims(self.sin1, 1),
                                np.expand_dims(self.cos1, 1),
                                np.expand_dims(self.sin2, 1),
                                np.expand_dims(self.cos2, 1),
#                                np.expand_dims(np.log(self.rainLagCov + 1.0), 1),
                                np.expand_dims(np.log(self.tempLagCov + 1.0), 1),
                                np.expand_dims(self.rice, 1), 
                                np.expand_dims(self.vegetables, 1), 
                                np.expand_dims(self.trees, 1)])
#                                np.expand_dims(self.dryCrop, 1)])
#                                np.expand_dims(self.rapeWheat, 1),
        # 2-d array of covariates for detection model
        self.detectIntercept = np.ones(self.nDat)
        lograin = np.log(self.rainDat + 1.0)
        lograin2 = lograin**2.0
#        self.detectCovariates = np.hstack([np.expand_dims(self.detectIntercept, 1),
#                                        np.expand_dims(lograin, 1),
#                                        np.expand_dims(lograin2, 1)])
        self.detectCovariates = np.hstack([np.expand_dims(self.detectIntercept, 1),
                                        np.expand_dims(lograin, 1)])
#        selCol = np.array([0,0,7,8,9])
#        printArr = self.tArray[:,selCol]
#        printArr[:,0] = self.months
#        printArr[:,1] = self.habIndx
#        print('printArr', printArr[200:400])
#        corWeather = np.corrcoef(self.rainLagCov, self.tempLagCov)
#        print('corWeather', corWeather)

    def getInitialXt(self):
        """
        get initial xt and N 
        """
#        self.Xt = np.zeros(self.nDat)
        startN = np.array([275, 265])
        startXt = np.log(startN)
        # lagged xt_1
#        self.Xt_1 = np.zeros((self.nsessions, 2))
        self.mu = np.zeros(self.nDat)
        self.N = self.nTrapped * 2.5
        self.N[self.nTrapped == 0] = 50
#        self.N = np.zeros(self.nDat)
        self.Xt = np.log(self.N)
        self.harmonics = np.dot(self.tArray, self.alpha)

        # loop thru habitat
        for i in range(2):
            # loop thru sessions
            xtHab = self.Xt[self.habIndx == i]
            xtHab[0] = startXt[i]
            NHab = self.N[self.habIndx == i]
            NHab[0] = startN[i]
            muHab = self.mu[self.habIndx == i]
            nTrappedHab = self.nTrapped[self.habIndx == i]
            harmonicsHab = self.harmonics[self.habIndx == i]

            for j in range(1, self.nsessions):
                xt_1 = xtHab[(j - 1)] 
                muHab[j] = (xt_1 + (self.b[i] * xt_1) + harmonicsHab[j]) 
                NHab[j] = np.ceil(np.exp(np.random.normal(muHab[j], 1.5)))
                if NHab[j] > self.params.maxN:
                    NHab[j] = 500

                if (NHab[j] <= nTrappedHab[j]):
                    NHab[j] = nTrappedHab[j] + 10
                xtHab[j] = np.log(NHab[j])
#            self.Xt[self.habIndx == i] = xtHab.copy()
#            self.N[self.habIndx == i] = NHab.copy()
            self.mu[self.habIndx == i] = muHab.copy()
        self.N = self.N.astype(int)
#        printArr = np.round(np.hstack([np.expand_dims(self.N, 1), 
#                                        np.expand_dims(self.Xt, 1),
#                                        np.expand_dims(self.mu, 1),
#                                        np.expand_dims(self.nTrapped, 1), 
#                                        np.expand_dims(self.nTrapSet, 1),
#                                        np.expand_dims(self.harmonics, 1)]), 2)


    def getInitialDetection(self):
        """
        get initial lambda and theta 
        """
#        self.nonZeroTrapMask = self.nTrapSet > 0
#        self.lambdaPred = np.zeros(self.nDat)
        self.lambdaPred = np.exp(np.dot(self.detectCovariates, self.g))
        expTerm = self.N * self.lambdaPred
        self.theta = 1.0 - np.exp(-expTerm)
#        prArr = np.hstack([self.lambdaPred, self.theta])
#        print('lam and th', prArr[0:50])
        self.detectPMF = stats.binom.logpmf(self.nTrapped, self.nTrapSet, self.theta)
                                

class MCMC(object):
    def __init__(self, params, basicdata):
        """
        Class to run mcmc
        """
        #############################################
        ############ Run MCMC functions
        self.storageArrays(params, basicdata)
        self.mcmcFX()
        ############## End run of MCMC functions
        #############################################

    def storageArrays(self, params, basicdata):
        """
        make storage arrays for parameters
        """
        self.basicdata = basicdata
        self.params = params
        self.nAlpha = len(self.basicdata.alpha)
        self.bgibbs = np.zeros((self.params.ngibbs, 2)) 
#        self.agibbs = np.zeros((self.params.ngibbs, 2)) 
        self.ggibbs = np.zeros((self.params.ngibbs, self.params.n_gPara)) 
        self.alphagibbs = np.zeros((self.params.ngibbs, self.nAlpha)) 
        self.siggibbs = np.zeros((self.params.ngibbs, 2))
        self.Ngibbs = np.zeros((self.params.ngibbs, self.basicdata.nDat)) 
#        self.NHab0gibbs = np.zeros((self.params.ngibbs, self.basicdata.nsessions), dtype = int)
#        self.NHab1gibbs = np.zeros((self.params.ngibbs, self.basicdata.nsessions), dtype = int)
#        self.followSess = np.array([5, 23, 100, 174, 228, 300])
#        self.meanNgibbs = np.zeros((self.basicdata.nsessions, 2))
#        self.nSample = np.array([-6, -5, -4, -3, -2, -1, 1, 2, 3, 4, 5, 6])
#        self.ab_Intercept = np.expand_dims(np.ones(self.basicdata.nsessions - 1), 1)
  

    def a_UpdateFX(self):
        """
        update growth rate parameters
        """
        for i in range(2):
            maskHab = self.basicdata.habIndx == i
            xtHab = self.basicdata.Xt[maskHab]
            muHab = self.basicdata.mu[maskHab]
            harmonicsHab = self.basicdata.harmonics[maskHab]
            ypred = xtHab[1:] - (muHab[1:] - self.basicdata.a[i])
            self.basicdata.a[i] = self.ab_bupdate(i, ypred, self.ab_Intercept, 
                                self.params.ab_vinvert, self.params.ab_ppart) 
            xt_1 = xtHab[:-1]
            DDeffect = (self.basicdata.b[i] * xt_1)
#            print('shp xt_1', xt_1.shape, 'a', self.basicdata.a, 'b', self.basicdata.b[i],
#                    'shp harm', self.basicdata.harmonics[1:].shape, 'ddefect', DDeffect.shape) 
            muHab[1:] = (xt_1 + self.basicdata.a[i] + DDeffect + harmonicsHab[1:])
#            print('new mu shp', muHab.shape)
            self.basicdata.mu[maskHab] = muHab

    def ab_bupdate(self, i, y, x, vinvert, ppart):
        """
        Gibbs updater of Normal/Normal parameters
        """
        xTranspose = np.transpose(x)
        xCrossProd = np.dot(xTranspose, x)
        sinv = 1/self.basicdata.sigma[i]
        sx = np.multiply(xCrossProd, sinv)
        xyCrossProd = np.dot(xTranspose, y)
        sy = np.multiply(xyCrossProd, sinv)
#        print('sx', sx, 'vinvert', vinvert, 'xcross', xCrossProd)
        bigv = np.linalg.inv(sx + vinvert)
        smallv = sy + ppart
        meanVariate = np.dot(bigv, smallv).flatten()
#        print('mean shape', meanVariate.shape)
        para = np.random.multivariate_normal(meanVariate, bigv)
        para =  np.transpose(para)
        return(para)

    def b_UpdateFX(self):
        """
        update growth rate parameters
        """
        for i in range(2):
            maskHab = self.basicdata.habIndx == i
            xtHab = self.basicdata.Xt[maskHab]
            muHab = self.basicdata.mu[maskHab]
            harmonicsHab = self.basicdata.harmonics[maskHab]
            xt_1 = xtHab[:-1]
            bpart = self.basicdata.b[i] * xt_1
            ypred = xtHab[1:] - (muHab[1:] - bpart)
            self.basicdata.b[i] = self.ab_bupdate(i, ypred, xt_1,
                                self.params.ab_vinvert, self.params.ab_ppart)
#            print('shp xt_1', xt_1.shape, 'a', self.basicdata.a, 'b', self.basicdata.b[i],
#                    'shp harm', self.basicdata.harmonics[1:].shape)
            DDeffect = (self.basicdata.b[i] * xt_1)
#            print('shp xt_1', xt_1.shape, 'a', self.basicdata.a, 'b', self.basicdata.b[i],
#                    'shp harm', self.basicdata.harmonics[1:].shape, 'ddefect', DDeffect.shape) 
            muHab[1:] = (xt_1 + DDeffect + harmonicsHab[1:])
#            print('new mu shp', muHab.shape)
            self.basicdata.mu[maskHab] = muHab



    def alpha_UpdateFX2(self):
        """
        update harmonics and rain for growth rate parameters
        """
        oneDownMask = self.basicdata.sessions < self.basicdata.maxSession
        xt_1 = self.basicdata.Xt[oneDownMask]
 
        predMask = self.basicdata.sessions > self.basicdata.minSession

        mu = self.basicdata.mu[predMask]
        Xt = self.basicdata.Xt[predMask]
        paraIndx = self.basicdata.habIndx[predMask]
        sigArray = self.basicdata.sigmaSqrt[paraIndx]
#        aPara = self.basicdata.a[paraIndx]
        bPara = self.basicdata.b[paraIndx]
        for i in range(self.nAlpha):
            alpha_s = self.basicdata.alpha.copy()
            alpha_s[i] = np.random.normal(self.basicdata.alpha[i], self.params.alpha_search[i])
            harmonics_s = np.dot(self.basicdata.tArray, alpha_s)
            mu_s = xt_1 + (bPara * xt_1) + harmonics_s[predMask]
            a_pdf = stats.norm.logpdf(Xt, mu, sigArray)
            a_pdf_s = stats.norm.logpdf(Xt, mu_s, sigArray)
            sum_pdf = np.sum(a_pdf)
            sum_pdf_s = np.sum(a_pdf_s)
            alphaPriors = stats.norm.logpdf(self.basicdata.alpha[i], self.params.alpha_Prior[0],
                    self.params.abPrior[1])
            alphaPriors_s = stats.norm.logpdf(alpha_s[i], self.params.alpha_Prior[0], 
                    self.params.alpha_Prior[1])
            pnow = sum_pdf + alphaPriors
            pnew = sum_pdf_s + alphaPriors_s
            pdiff = pnew - pnow
            if pdiff > 10.0:
                rValue = 1.0
                zValue = 0.0
            elif pdiff < -10.0:
                rValue = 0.0
                zValue = 1.0
            else:
                rValue = np.exp(pnew - pnow)        # calc importance ratio
                zValue = np.random.uniform(0.0, 1.0, size = None)
            if (rValue > zValue):
                self.basicdata.alpha[i] = alpha_s[i]
                self.basicdata.harmonics = harmonics_s.copy()
                self.basicdata.mu[predMask] = mu_s.copy()
        


    def N_UpdateFX(self):
        """
        update N estimates
        """
#        print('N', self.basicdata.N)
#        xt_1 = self.basicdata.Xt[:-1]
        # loop thru sessions
#        print('nDat', self.basicdata.nDat, 'nsess', self.basicdata.nsessions, 
#            'Sess', self.basicdata.sessions)
#        pDat = np.zeros((self.basicdata.nDat, 2))
#        pDat[:,0] = self.basicdata.habIndx
#        pDat[:,1] = self.basicdata.sessions
#        print('pDat', pDat[330:370])
        for i in range(self.basicdata.nDat):
            ns = self.proposeN(i)
            xt_s = np.log(ns)
            Xt = self.basicdata.Xt[i]
            # proposed prob of trap
            expTerm_s = ns * self.basicdata.lambdaPred[i]
            theta_s = 1.0 - np.exp(-expTerm_s)
            detectPMF_s = stats.binom.logpmf(self.basicdata.nTrapped[i], 
                    self.basicdata.nTrapSet[i], theta_s)
            detectPMF = self.basicdata.detectPMF[i]            
#            if i < 10:
#                print('i', i, 'N', self.basicdata.N[i], 'ns', ns, 'ntrapped', self.basicdata.nTrapped[i], 
#                    'theta', self.basicdata.theta[i] ,'pmf', detectPMF,
#                    'pmf_s', detectPMF_s, )
            habIndx_i = self.basicdata.habIndx[i]
#            aPara = self.basicdata.a[habIndx_i]
            bPara = self.basicdata.b[habIndx_i]
            sigPara = self.basicdata.sigmaSqrt[habIndx_i]
#            if self.basicdata.sessions[i] < (self.basicdata.nsessions - 1):
            if self.basicdata.sessions[i] < (self.basicdata.maxSession):
                Xt_up1 = self.basicdata.Xt[i + 1]
                mu_up1 = self.basicdata.mu[i + 1]
                mu_up1_s = (xt_s + (bPara * xt_s) + self.basicdata.harmonics[i + 1])
                up1_pdf = stats.norm.logpdf(Xt_up1, mu_up1, sigPara)
                up1_pdf_s = stats.norm.logpdf(Xt_up1, mu_up1_s, sigPara)
#            if self.basicdata.sessions[i] > 0:
            if self.basicdata.sessions[i] > self.basicdata.minSession:
                mu_i = self.basicdata.mu[i]
                pdf_i = stats.norm.logpdf(Xt, mu_i, sigPara)
                pdf_i_s = stats.norm.logpdf(xt_s, mu_i, sigPara)
            # Calc pnow and pnew
            zValue = np.random.uniform(0.0, 1.0)
#            if self.basicdata.sessions[i] == 0:
            if self.basicdata.sessions[i] == self.basicdata.minSession:
                pnow = detectPMF + up1_pdf
                pnew = detectPMF_s + up1_pdf_s
                rValue = np.exp(pnew - pnow)        # calc importance ratio
                if rValue > zValue:
                    self.basicdata.N[i] = ns
                    self.basicdata.mu[(i+1)] = mu_up1_s
                    self.basicdata.Xt[i] = xt_s
                    self.basicdata.detectPMF[i] = detectPMF_s
                    self.basicdata.theta[i] = theta_s
#            if ((self.basicdata.sessions[i] > 0) & (self.basicdata.sessions[i] < (self.basicdata.nsessions - 1))):
            if ((self.basicdata.sessions[i] > self.basicdata.minSession) & (self.basicdata.sessions[i] < 
                (self.basicdata.maxSession))):
                pnow = detectPMF + up1_pdf + pdf_i
                pnew = detectPMF_s + up1_pdf_s + pdf_i_s
#                if i > 680:
#                    print('i', i, 'pmf', detectPMF, 'pmf_s', detectPMF_s, 'up pdf', up1_pdf, 'up pdf_s', up1_pdf_s, 
#                        'pdf_i', pdf_i, 'pdf_i_s', pdf_i_s, 'pnow', pnow, 'pnew', pnew)
#                    print('iiiii', i, 'Xt', Xt, 'xt_s', xt_s, 'mu_i', mu_i, 
#                        'Xt_up1', Xt_up1, 'mu_up1', mu_up1, 'mu_up1_s', mu_up1_s, 'sigPara', sigPara)
                rValue = np.exp(pnew - pnow)        # calc importance ratio
                if rValue > zValue:
                    self.basicdata.N[i] = ns
                    self.basicdata.mu[(i+1)] = mu_up1_s
                    self.basicdata.Xt[i] = xt_s
                    self.basicdata.detectPMF[i] = detectPMF_s
                    self.basicdata.theta[i] = theta_s
#            if self.basicdata.sessions[i] == (self.basicdata.nsessions - 1):
            if self.basicdata.sessions[i] == (self.basicdata.maxSession):
                pnow = detectPMF + pdf_i
                pnew = detectPMF_s + pdf_i_s
                rValue = np.exp(pnew - pnow)        # calc importance ratio
                if rValue > zValue:
                    self.basicdata.N[i] = ns
                    self.basicdata.Xt[i] = xt_s
                    self.basicdata.detectPMF[i] = detectPMF_s
                    self.basicdata.theta[i] = theta_s

    def proposeN(self, i):
        """
        propose new Nx for updating
        """
        randNew = np.random.choice(self.params.nSample, 1)
        ns = self.basicdata.N[i] + randNew
        if ns < 1:
            ns = self.basicdata.N[i] + 3
        if ns <= self.basicdata.nTrapped[i]:
            ns = self.basicdata.N[i] + 3
        if ns >= self.params.maxN:
            ns = self.basicdata.N[i] - 3
#        if i < 5:
#            print('N', self.basicdata.N[i], 'rand', randNew, 'ns', ns) 
        return ns



    def sig_UpdateFX(self):
        """
        update sigma variance parameters
        """
        ## loop thru habitat
        for i in range(2):
            self.vupdate(i)
        self.basicdata.sigmaSqrt = np.sqrt(self.basicdata.sigma)
            
    def vupdate(self, i):
        """
        Gibbs updater of variance parameter
        """
        maskHab = self.basicdata.habIndx == i
        xtHab = self.basicdata.Xt[maskHab]
        muHab = self.basicdata.mu[maskHab]
        nobs = self.basicdata.nsessions - 1
        predDiff = xtHab[1:] - muHab[1:]
        sx = np.sum(predDiff**2.0)
        u1 = self.params.sigmaPriors[0] + np.multiply(0.5, nobs)
        u2 = self.params.sigmaPriors[1] + np.multiply(0.5, sx)               # rate parameter
        isg = np.random.gamma(u1, 1.0 / u2, size = None)            # formulation using shape and scale
        self.basicdata.sigma[i] = 1.0 / isg


    def g_UpdateFX(self):
        """
        update detection parameters
        """
        for i in range(self.params.n_gPara):
            g_s = self.basicdata.g.copy()
            g_s[i] = np.random.normal(self.basicdata.g[i], self.params.g_search[i], size = None)
            lambdaPred_s = np.exp(np.dot(self.basicdata.detectCovariates, g_s))
            expTerm = self.basicdata.N * lambdaPred_s
            theta_s = 1.0 - np.exp(-expTerm)
            detectPMF_s = stats.binom.logpmf(self.basicdata.nTrapped, 
                            self.basicdata.nTrapSet, theta_s)
            prior = stats.norm.logpdf(self.basicdata.g[i],
                    self.params.gPrior[0], self.params.gPrior[1])
            prior_s = stats.norm.logpdf(g_s[i],
                    self.params.gPrior[0], self.params.gPrior[1])
            pnow = np.sum(self.basicdata.detectPMF) + prior
            pnew = np.sum(detectPMF_s) + prior_s
            pdiff = pnew - pnow

#            print('i = ', i, 'pdiff', pdiff, 'self.basicdata.detectPMF', np.sum(self.basicdata.detectPMF),
#                'pmf_s', np.sum(detectPMF_s), 'prior', prior, 'prior_s', prior_s)

            if pdiff > 10.0:
                rValue = 1.0
                zValue = 0.0
            elif pdiff < -10.0:
                rValue = 0.0
                zValue = 1.0
            else:
                rValue = np.exp(pnew - pnow)        # calc importance ratio
                zValue = np.random.uniform(0.0, 1.0, size = None)
            if (rValue > zValue):
                self.basicdata.g[i] = g_s[i] 
                self.basicdata.detectPMF = detectPMF_s.copy()
                self.basicdata.theta = theta_s.copy()
                self.basicdata.lambdaPred = lambdaPred_s.copy()

        

########            Main mcmc function
########
    def mcmcFX(self):
        """
        update parameters
        """
        cc = 0
        for g in range(self.params.ngibbs * self.params.thinrate + self.params.burnin):

#            self.a_UpdateFX()

            self.b_UpdateFX()

            self.g_UpdateFX()

            self.alpha_UpdateFX2()

            self.sig_UpdateFX()

            self.N_UpdateFX()

            if g in self.params.keepseq:
                self.bgibbs[cc] = self.basicdata.b
#                self.agibbs[cc] = self.basicdata.a
                self.ggibbs[cc] = self.basicdata.g
                self.alphagibbs[cc] = self.basicdata.alpha
                self.siggibbs[cc] = self.basicdata.sigma
                self.Ngibbs[cc] = self.basicdata.N
#                self.NHab0gibbs[cc] = self.basicdata.N[:, 0] 
#                self.NHab1gibbs[cc] = self.basicdata.N[:, 1] 

                cc = cc + 1
        
#        P.figure()
#        P.plot(self.basicdata.N[:120], color = 'g', linewidth = 3.0)
#        P.plot(np.exp(self.basicdata.mu[:120]), color = 'k', linewidth = 3.0)
#        P.plot(np.exp(self.basicdata.Xt[:120]), color = 'b', linewidth = 3.0)
#        P.show()

#        print('Ngibbs', self.NHab0gibbs[:, self.followSess])
#        print('siggibbs', self.siggibbs)
#        print('agibbs', self.agibbs)
#        print('bgibbs', self.bgibbs)
#        print('alphagibbs', self.alphagibbs)
#        print('ggibbs', self.ggibbs)


########            Pickle results to directory
########
class Gibbs(object):
    def __init__(self, mcmcobj, basicdata, params):
        self.Ngibbs = mcmcobj.Ngibbs
#        self.NHab0gibbs = mcmcobj.NHab0gibbs
#        self.NHab1gibbs = mcmcobj.NHab1gibbs
        self.bgibbs = mcmcobj.bgibbs
#        self.agibbs = mcmcobj.agibbs
        self.ggibbs = mcmcobj.ggibbs
        self.alphagibbs = mcmcobj.alphagibbs
        self.siggibbs = mcmcobj.siggibbs
        self.nsessions = basicdata.nsessions   
        self.sessions = basicdata.sessions
        self.maxSession = basicdata.maxSession
        self.minSession = basicdata.minSession
        self.followSess = params.followSess
        self.months = basicdata.months
        self.years = basicdata.years
        self.nTrapped = basicdata.nTrapped
        self.habIndx = basicdata.habIndx        # 0 = irrigated; 1 = non-irregated
        self.habMask0 = self.habIndx == 0
        self.habMask1 = self.habIndx == 1
        self.nTrapSet = basicdata.nTrapSet
        self.rainDat = basicdata.rainDat                    # [self.habIndx == 0]
        self.temperatureDat = basicdata.temperatureDat      # [self.habIndx == 0]
        self.Raint_1 = basicdata.Raint_1
        self.tArray = basicdata.tArray
        self.detectCovariates = basicdata.detectCovariates
        # cropping data
        self.rice = basicdata.rice
        self.rapeWheat = basicdata.rapeWheat
        self.vegetables = basicdata.vegetables
        self.trees = basicdata.trees
        self.dryCrop = basicdata.dryCrop
        # weather data lagged
        self.rainLagCov = basicdata.rainLagCov
        self.tempLagCov = basicdata.tempLagCov
#        self.rainT9_7 = basicdata.rainT9_7
#        self.tempT9_7 = basicdata.tempT9_7
#        self.maskT9_7 = basicdata.maskT9_7
#        self.rainT8_6 = basicdata.rainT8_6
#        self.tempT8_6 = basicdata.tempT8_6
#        self.maskT8_6 = basicdata.maskT8_6
#        self.rainT7_5 = basicdata.rainT7_5
#        self.tempT7_5 = basicdata.tempT7_5
#        self.maskT7_5 = basicdata.maskT7_5
#        self.rainT6_4 = basicdata.rainT6_4
#        self.tempT6_4 = basicdata.tempT6_4
#        self.maskT6_4 = basicdata.maskT6_4
#        self.rainT5_3 = basicdata.rainT5_3
#        self.tempT5_3 = basicdata.tempT5_3
#        self.maskT5_3 = basicdata.maskT5_3
#        self.rainT4_2 = basicdata.rainT4_2
#        self.tempT4_2 = basicdata.tempT4_2
#        self.maskT4_2 = basicdata.maskT4_2
#        self.rainT3_1 = basicdata.rainT3_1
#        self.tempT3_1 = basicdata.tempT3_1
#        self.maskT3_1 = basicdata.maskT3_1
        # Complete data arrays for prediction
        self.rainLagCovAll = basicdata.rainLagCovAll
        self.tempLagCovAll = basicdata.tempLagCovAll
        self.monthsAll = basicdata.monthsAll
        self.yearsAll = basicdata.yearsAll
        self.Raint_1All = basicdata.Raint_1All
        self.riceAll = basicdata.riceAll
        self.vegetablesAll = basicdata.vegetablesAll
        self.rapeWheatAll = basicdata.rapeWheatAll
        self.treesAll = basicdata.treesAll
        self.dryCropAll = basicdata.dryCropAll
        self.rainDatAll = basicdata.rainDatAll
        self.temperatureDatAll = basicdata.temperatureDatAll
        self.monthRunAll = basicdata.monthRunAll
        self.nsessionsAll = basicdata.nsessionsAll
        self.sessionsAll = basicdata.sessionsAll
        self.nTrappedAll = basicdata.nTrappedAll
        self.habIndxAll = basicdata.habIndxAll
        self.nTrapSetAll = basicdata.nTrapSetAll
        # crossvalidation data
        self.monthCV = basicdata.monthCV
        self.yearCV = basicdata.yearCV
        self.cvDates = basicdata.cvDates
        self.nCV = basicdata.nCV
        self.cvPointMask = basicdata.cvPointMask


class ApodemusData(object):
    def __init__(self, rawdata):
        """
        Pickle structure from explore.py
        """
#        self.habMask1 = rawdata.habMask1
#        self.habMask2 = rawdata.habMask2
        self.monthsAll = rawdata.monthsAll
        self.yearsAll = rawdata.yearsAll
        self.nsessionsAll = rawdata.nsessionsAll
        self.sessionsAll = rawdata.sessionsAll
        self.nDatAll = rawdata.nDatAll
        self.nTrappedAll = rawdata.nTrappedAll
        self.habIndxAll = rawdata.habIndxAll
        self.nTrapSetAll = rawdata.nTrapSetAll
        self.rainDatAll = rawdata.rainDatAll
        self.temperatureDatAll = rawdata.temperatureDatAll
        self.Raint_1All = rawdata.Raint_1All
        self.monthRunAll = rawdata.monthRunAll
        self.minMonRunAll = rawdata.minMonRunAll
        # cropping data
        self.riceAll = rawdata.riceAll
        self.rapeWheatAll = rawdata.rapeWheatAll
        self.vegetablesAll = rawdata.vegetablesAll
        self.treesAll = rawdata.treesAll
        self.dryCropAll = rawdata.dryCropAll
        # weather data lagged
        self.rainT10_8All = rawdata.rainT10_8All
        self.tempT10_8All = rawdata.tempT10_8All
        self.maskT10_8All = rawdata.maskT10_8All
        self.rainT9_7All = rawdata.rainT9_7All
        self.tempT9_7All = rawdata.tempT9_7All
        self.maskT9_7All = rawdata.maskT9_7All
        self.rainT8_6All = rawdata.rainT8_6All
        self.tempT8_6All = rawdata.tempT8_6All
        self.maskT8_6All = rawdata.maskT8_6All
        self.rainT7_5All = rawdata.rainT7_5All
        self.tempT7_5All = rawdata.tempT7_5All
        self.maskT7_5All = rawdata.maskT7_5All
        self.rainT6_4All = rawdata.rainT6_4All
        self.tempT6_4All = rawdata.tempT6_4All
        self.maskT6_4All = rawdata.maskT6_4All
        self.rainT5_3All = rawdata.rainT5_3All
        self.tempT5_3All = rawdata.tempT5_3All
        self.maskT5_3All = rawdata.maskT5_3All
        self.rainT4_2All = rawdata.rainT4_2All
        self.tempT4_2All = rawdata.tempT4_2All
        self.maskT4_2All = rawdata.maskT4_2All
        self.rainT3_1All = rawdata.rainT3_1All
        self.tempT3_1All = rawdata.tempT3_1All
        self.maskT3_1All = rawdata.maskT3_1All
        # Cross validation data
        self.monthCV = rawdata.monthCV
        self.yearCV = rawdata.yearCV
        self.cvDates = rawdata.cvDates
        self.nCV = rawdata.nCV
        self.cvPointMask = rawdata.cvPointMask

#        # morph data from Yuqing
#        self.morphYear = rawdata.morphYear
#        self.morphMonth = rawdata.morphMonth
#        self.morphHabitat = rawdata.morphHabitat
#        self.morphMass = rawdata.morphMass
#        self.morphSex = rawdata.morphSex




########            Main function
#######
def main(params, basicfile = False):

    rodentpath = os.getenv('CHINAPROJDIR', default = '.')
    ## paths and data to read in
 
    ## initiate basicdata class and object when do not read in previous results
    if basicfile:
        print('Read in ', params.basicdataName)
        ## read in pickled results (basicdata) from a previous run
        inputBasicdata = os.path.join(rodentpath, params.basicdataName)
        fileobj = open(inputBasicdata, 'rb')
        basicdata = pickle.load(fileobj)
        fileobj.close()
    else:
        ## read in the pickled capt and trap data from 'manipCats.py'
        apodemusFile = os.path.join(rodentpath,'out_manipdata.pkl')
        fileobj = open(apodemusFile, 'rb')
        apodemusdata = pickle.load(fileobj)
        fileobj.close()
        ## initiate basicdata from script
        basicdata = BasicData(apodemusdata, params)

    ## initiate mcmc class and object
    mcmcobj = MCMC(params, basicdata)

    ## object created to pickle results
    gibbsobj = Gibbs(mcmcobj, basicdata, params)

    ## pickle basic data from present run to be used to initiate new runs
    outBasicdata = os.path.join(rodentpath, params.basicdataName)
    fileobj = open(outBasicdata, 'wb')
    pickle.dump(basicdata, fileobj)
    fileobj.close()

    ## pickle mcmc results for post processing in gibbsProcessing.py
    outGibbs = os.path.join(rodentpath, params.gibbsName)
    fileobj = open(outGibbs, 'wb')
    pickle.dump(gibbsobj, fileobj)
    fileobj.close()




if __name__ == '__main__':
    main(params)

