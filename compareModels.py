#!/usr/bin/env python

import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
from scipy import stats
import prettytable
import pickle
import os
import datetime
from predictRodent import DeviancePickle

class CompareModels(object):
    def __init__(self, rodentpath):
        """
        compare deviance results of crossvalidation
        """
        ##### Run functions
        self.idModelNames()
        self.readInPickles(rodentpath)
        self.getQuantiles()
        self.k_sTest()
        ########################

    def idModelNames(self):
        """
        id names of models to compare
        """
        # model 1
        self.monBack_M1 = 6
        covNames_M1 = 'Rain'
        self.laggedMonths_M1 = np.array([10,8])
        self.name_M1 = ('dev_' + covNames_M1 + str(self.laggedMonths_M1[0]) + '_' +
            str(self.laggedMonths_M1[1]) + '_Back' + str(self.monBack_M1) + '.pkl')
        print('Model 1 = ', self.name_M1) 
       # model 2
        self.monBack_M2 = 6
        covNames_M2 = 'Temp'
        self.laggedMonths_M2 = np.array([10,8])
        self.name_M2 = ('dev_' + covNames_M2 + str(self.laggedMonths_M2[0]) + '_' +
            str(self.laggedMonths_M2[1]) + '_Back' + str(self.monBack_M2) + '.pkl')
        print('Model 2 = ', self.name_M2)

    def readInPickles(self, rodentpath):
        """
        read pickles of deviance results from predictrodents.py
        """
        # model 1
        input_M1 = os.path.join(rodentpath, self.name_M1 )
        print('input_M1', input_M1)
        fileobj = open(input_M1, 'rb')
        model1_obj = pickle.load(fileobj)
        fileobj.close()
        self.captDev_M1 = model1_obj.captDeviance

        print('m1', self.captDev_M1[:10])
        

        # model 2
        input_M2 = os.path.join(rodentpath, self.name_M2 )
        fileobj = open(input_M2, 'rb')
        model2_obj = pickle.load(fileobj)
        fileobj.close()
        self.captDev_M2 = model2_obj.captDeviance
        print('m2', self.captDev_M2[:10])        

    def getQuantiles(self):
        """
        calc means and quantiles
        """
        self.quantsDev1 = mquantiles(self.captDev_M1, prob=[0.025, 0.5, 0.975], axis = 0)
        self.meanDev1 = np.mean(self.captDev_M1, axis = 0)
        print('Quantiles_M1', self.quantsDev1, 'Mean_M1 =', self.meanDev1)

        self.quantsDev2 = mquantiles(self.captDev_M2, prob=[0.025, 0.5, 0.975], axis = 0)
        self.meanDev2 = np.mean(self.captDev_M2, axis = 0)
        print('Quantiles_M2', self.quantsDev2, 'Mean_M2 =', self.meanDev2)

    def k_sTest(self):
        """
        Kolmogorov-Smirnov statistic on 2 samples
        """
        names = np.array(['lowland', 'upland', 'combined'])
        for i in range(3):
            ks = stats.ks_2samp(self.captDev_M1[:, i], self.captDev_M2[:, i])
            print('ks test for', names[i], ':', ks)


def main():

    # paths and data to read in
    rodentpath = os.getenv('CHINAPROJDIR', default='.')

    comparemodels = CompareModels(rodentpath)

if __name__ == '__main__':
    main()


