#!/bin/bash
#SBATCH -J t0Ric31
#SBATCH --mail-type=end
#SBATCH --mail-user=andersond@landcareresearch.co.nz
#SBATCH --time=199:00:00
#SBATCH --mem-per-cpu=3000  
#SBATCH -o job-%j.%N.out
#SBATCH -e job-%j.%N.err

srun startApodemus.py --basicdata=True 

#srun startApodemus.py


