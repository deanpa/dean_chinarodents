#!/usr/bin/env python


#from cats import Gibbs # need this because of pickled results from mcmc
#import params
import numpy as np
import pylab as P
#import matplotlib
#matplotlib.use('TkAgg')
#import matplotlib.pyplot as P
from scipy.stats.mstats import mquantiles
import prettytable
import pickle
import os
import datetime

def logit(x):
    """
    Function (logit) to convert probability to real number
    """
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    """
    Function to convert real number to probability
    """
    return np.exp(x) / (1 + np.exp(x))


class ResultsProcessing(object):
    def __init__(self, gibbsobj, rodentpath):

#        self.params = params
        self.rodentpath = rodentpath

        self.gibbsobj = gibbsobj
        self.ndat = len(self.gibbsobj.siggibbs)
        self.results = np.hstack([self.gibbsobj.bgibbs,
                                self.gibbsobj.alphagibbs,
                                self.gibbsobj.ggibbs,
                                self.gibbsobj.siggibbs])
        self.meanN = np.mean(self.gibbsobj.Ngibbs, axis = 0)
        self.npara = self.results.shape[1]
        self.nsessions = gibbsobj.nsessions
        self.months = self.gibbsobj.months
        self.years = self.gibbsobj.years
        self.nTrapped = self.gibbsobj.nTrapped
        self.habIndx = self.gibbsobj.habIndx        # 0 = irrigated; 1 = non-irregated
        self.habMask0 = self.habIndx == 0
        self.habMask1 = self.habIndx == 1
        self.nTrapSet = self.gibbsobj.nTrapSet
        self.rainDat = self.gibbsobj.rainDat                    # [self.habIndx == 0]
        self.temperatureDat = self.gibbsobj.temperatureDat      # [self.habIndx == 0]
        self.Raint_1 = self.gibbsobj.Raint_1
        self.tArray = self.gibbsobj.tArray
        self.detectCovariates = self.gibbsobj.detectCovariates

      
        
    def getSummaryMeans(self):
        """
        get mean trap rate and summaries for Results
        """
        trapSetNAN = self.nTrapSet.astype(float)
        trapSetNAN[trapSetNAN <= 0.0] = np.nan
        self.summaryTrapRate = self.nTrapped / trapSetNAN
        self.meanMonthTrapRate = np.empty((12,6))
        self.meanMonthTrapRate[:,0] =  np.arange(1.,13.)
        for i in range(12):
            monthMask = self.months == (i + 1)
            lowHabMonthMask = monthMask & self.habMask0
            highHabMonthMask = monthMask & self.habMask1
            # mean trap rate
            tr_i = self.summaryTrapRate[lowHabMonthMask] * 100.0
            tr_i_up = self.summaryTrapRate[highHabMonthMask] * 100.0
            self.meanMonthTrapRate[i,1] = np.nanmean(tr_i) 
            
            self.meanMonthTrapRate[i, 2] =  np.nanmean(tr_i_up)
            # traprate quantiles
#            self.meanMonthTrapRate[i, 2] =  np.nanpercentile(tr_i, 2.5)
            self.meanMonthTrapRate[i, 3] =  np.nanpercentile(tr_i, 97.5)
            # mean log rainfall 
            rain_i = self.rainDat[monthMask]
            self.meanMonthTrapRate[i,4] = np.nanmean(rain_i)
#            self.meanMonthTrapRate[i,4] = np.log(np.nanmean(rain_i) + 1.0)
            temp_i = self.temperatureDat[monthMask]
            # mean log temp
            self.meanMonthTrapRate[i,5] = np.nanmean(temp_i)
#            self.meanMonthTrapRate[i,5] = np.log(np.nanmean(temp_i) + 1.0)
#        print('meanMonthTrapRate', np.round(self.meanMonthTrapRate, 2))
        self.plotMeanTraprate_RainTemp()
        
        

    def plotMeanTraprate_RainTemp(self):
        """
        plot monthly means of traprates,rainfall and temperature
        """
        P.figure(figsize=(11,9))
        monNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
            'Sep', 'Oct', 'Nov', 'Dec']
        ax1 = P.gca()
        mon = self.meanMonthTrapRate[:, 0]
        lns1 = ax1.plot(mon, self.meanMonthTrapRate[:, 1], color = 'k', label = 'Mean lowland trap rate', linewidth = 3)
        lns2 = ax1.plot(mon, self.meanMonthTrapRate[:, 2], color = 'k', 
                ls = '--', label = 'Mean upland trap rate', linewidth = 2.5)
#        lns2 = ax1.plot(mon, self.meanMonthTrapRate[:, 2], color = 'k', 
#                ls = '--', label = '95% CI trap rate', linewidth = 1.0)
#        lns3 = ax1.plot(mon, self.meanMonthTrapRate[:, 3], color = 'k', ls = '--', linewidth = 1.0)
        lns5 = ax1.plot(mon, self.meanMonthTrapRate[:, 5], color = 'r', label = 'Mean temperature', linewidth = 1.0)
        # second axis
        ax2 = ax1.twinx()
        lns4 = ax2.plot(mon, self.meanMonthTrapRate[:, 4], color = 'b', label = 'Mean rainfall', linewidth = 1.0)

        lns = lns1 + lns2 + lns5 + lns4
        labs = [l.get_label() for l in lns]
#        ax1.set_xlim(minDate, maxDate)
        for tick in ax1.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax1.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        ax1.set_xlabel('Months', fontsize = 17)
        ax2.set_ylabel('Mean rainfall (mm)', fontsize = 17)
        ymax = np.max(self.meanMonthTrapRate[:, 3])
        ax1.set_ylabel('Mean trap rate & temperature', fontsize = 17)
        ax1.set_ylim([-1.0, ymax])
        ax1.legend(lns, labs, loc = 'upper right')
        P.xticks(mon, monNames)
        figFname = 'monthMeanTR_rainTmp.png'
        P.savefig(figFname, format='png', dpi = 1000)
        P.show()                


    def plotMeanTraprate_RainTemp222(self):
        """
        plot monthly means of traprates,rainfall and temperature
        """
        P.figure(figsize=(11,9))
        monNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
            'Sep', 'Oct', 'Nov', 'Dec']
        ax1 = P.gca()
        mon = self.meanMonthTrapRate[:, 0]
        lns1 = ax1.plot(mon, self.meanMonthTrapRate[:, 1], color = 'k', label = 'Mean trap rate', linewidth = 3)
        lns2 = ax1.plot(mon, self.meanMonthTrapRate[:, 2], color = 'k', 
                ls = '--', label = '95% CI trap rate', linewidth = 1.0)
        lns3 = ax1.plot(mon, self.meanMonthTrapRate[:, 3], color = 'k', ls = '--', linewidth = 1.0)
        lns4 = ax1.plot(mon, self.meanMonthTrapRate[:, 4], color = 'b', label = 'Mean log rainfall', linewidth = 1.0)
        lns5 = ax1.plot(mon, self.meanMonthTrapRate[:, 5], color = 'r', label = 'Mean log temperature', linewidth = 1.0)
        lns = lns1 + lns2 + lns4 + lns5
        labs = [l.get_label() for l in lns]
#        ax1.set_xlim(minDate, maxDate)
        for tick in ax1.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax1.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        ax1.set_xlabel('Months', fontsize = 17)
        ymax = np.max(self.meanMonthTrapRate[:, 3]) + 1
        ax1.set_ylabel('Monthly means', fontsize = 17)
        ax1.set_ylim([-1.0, ymax])
        ax1.legend(lns, labs, loc = 'upper right')
        P.xticks(mon, monNames)
        figFname = 'monthMeanTR_rainTmp.png'
#        P.savefig(figFname, format='png', dpi = 1000)
        P.show()                


    def getTrapNAN(self):
        """
        get trap data with nan inserted
        """
        self.trapRate = self.nTrapped.copy()
        zeroTrapMask = self.nTrapSet == 0
        trapMask = self.nTrapSet > 0
        self.trapRate[trapMask] = self.nTrapped[trapMask] / self.nTrapSet[trapMask] * 100.0
        self.trapRate = self.trapRate.astype(np.float64)
        self.trapRate[zeroTrapMask] = np.nan
#        self.trapRate = self.trapRate.astype(np.float64)
 

    @staticmethod    
    def quantileFX(a):
        return mquantiles(a, prob=[0.025, 0.5, 0.975], axis = 0)

    def getPopNames(self):
        """
        get strings to id session population size in table
        """
        nPop = np.shape(self.gibbsobj.Ngibbs)[1]    
        pop = np.arange(nPop)
        popName = 'N0'
        for i in range(1,nPop):
            aa = 'N'+str(i)
            popName = np.append(popName,aa)
        self.popName = popName.copy()

    def makeTableFX(self):
        resultTable = np.zeros(shape = (4, self.npara))
        resultTable[0:3, :] = np.round(self.quantileFX(self.results), 3)
        resultTable[3, :] = np.round(np.mean(self.results, axis = 0), 3)
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Names', 'Low CI', 'Median', 'High CI', 'Mean'])
#        self.names = np.array(['DD_irr', 'DD_nonIrr', 'growthIntercept', 'sin1', 'cos1',
#                    'sin2', 'cos2', 'rain_growth', 'temp_growth', 'rice', 'vegetables',
#                    'trees',
#                    'detectInter', 'detectRain',
#                    'sigma_Irr', 'sigma_nonIrr'])
#        self.names = np.array(['DD_irr', 'DD_nonIrr', 'growthIntercept', 'sin1', 'cos1',
#                    'sin2', 'cos2', 'rain_growth', 'rice', 'vegetables',
#                    'trees',
#                    'detectInter', 'detectRain',
#                    'sigma_Irr', 'sigma_nonIrr'])
        self.names = np.array(['DD_irr', 'DD_nonIrr', 'growthIntercept', 'sin1', 'cos1',
                    'sin2', 'cos2', 'Temp_growth', 'vegetables',
                    'trees',
                    'detectInter', 'detectRain',
                    'sigma_Irr', 'sigma_nonIrr'])
#        self.names = np.array(['DD_irr', 'DD_nonIrr', 'sin1', 'cos1',
#                    'sin2', 'cos2', 'temp_growth', 'rice', 'vegetables',
#                    'trees',
#                    'detectInter', 'detectRain',
#                    'sigma_Irr', 'sigma_nonIrr'])
        for i in range(self.npara):
            name = self.names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)
        self.summaryTable = resultTable.copy()


    def plotFX(self):
        """
        plot diagnostic trace plots
        """
        ngShp = np.shape(self.gibbsobj.Ngibbs)
        col = (ngShp[1] - 3)
        nG = self.gibbsobj.Ngibbs
#        print('nG', nG[-5:, -7:], 'ngshp', ngShp, 'col', col, 'nsess', self.nsessions)
        cc = 0
        nfigures = np.int(np.ceil(self.npara/6.0)) + 2
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            if i == 0:
                for j in range(6):
                    col_j = self.gibbsobj.followSess[j]
                    gibbsdat = self.gibbsobj.Ngibbs[:, col_j]
#                    print('col_j', col_j, gibbsdat, gibbsdat[:10])
                    P.subplot(2,3,j+1)
                    P.plot(gibbsdat)
                    P.title('Irr pop sess'+str(col_j))
            if i == 1:
                for j in range(6):
                    col_j = self.gibbsobj.followSess[j] + self.nsessions - 10
                    gibbsdat = self.gibbsobj.Ngibbs[:, col_j]
                    print('col_j', col_j, 'gibbsdat', gibbsdat[:10])
                    P.subplot(2,3,j+1)
                    P.plot(gibbsdat)
                    P.title('Non-irr pop sess'+str(col_j))
            if i > 1:
                for j in range(6):
                    P.subplot(2,3,j+1)
                    if cc < self.npara:
                        P.plot(self.results[0:self.ndat, cc])
                        P.title(self.names[cc])
                    cc = cc + 1
                P.show(block=lastFigure)

    def abPara(self):
        """
        get parameters from gibbs
        """
#        self.apara = np.mean(self.gibbsobj.agibbs, axis = 0)
        self.bpara = np.mean(self.gibbsobj.bgibbs, axis = 0)
        self.alphapara = np.mean(self.gibbsobj.alphagibbs, axis = 0)
        self.gpara = np.mean(self.gibbsobj.ggibbs, axis = 0)
        self.harmonics = np.zeros(len(self.months))
        self.pGrowth = np.zeros(len(self.months))
        self.lambdaPred = np.exp(np.dot(self.detectCovariates, self.gpara))
        expTerm = self.meanN * self.lambdaPred
        self.theta = 1.0 - np.exp(-expTerm)
        # loop thru habitat
        for i in range(2):
            mask_i = self.habIndx == i
            tArray_i = self.tArray[mask_i]
            harmonics_i =  np.dot(tArray_i, self.alphapara)
            self.harmonics[mask_i] = harmonics_i.copy()
#            self.pGrowth[mask_i] = self.apara[i] + harmonics_i
        

    def getNPred(self):
        """
        calc npred for all sessions
        """
        # get parameters
        self.abPara()
        self.nPred = np.zeros(len(self.months))
        # loop thru habitat
        for i in range(2):
            mask_i = self.habIndx == i
            npred_i = self.nPred[mask_i]
            N_i = self.meanN[mask_i]
            npred_i[0] = N_i[0]
#            a = self.apara[i]
            b = self.bpara[i]
            harmonics_i = self.harmonics[mask_i]
            # loop thru sessions
            for j in range(1, self.nsessions):
                xt_1 = np.log(N_i[j - 1])
                mu_j = xt_1 + (b * xt_1) + harmonics_i[j]
                npred_i[j] = np.exp(mu_j)
            self.nPred[mask_i] = npred_i

#        prArr = np.hstack([np.expand_dims(self.meanN, 1), np.expand_dims(self.nTrapped, 1),
#            np.expand_dims(self.nTrapped * 2.5, 1), np.expand_dims(self.nPred, 1)])
#        print('N, ntrap, ntrap*2.5, npred', prArr[:50])


    def getDates(self):
        """
        make dates array
        """
        self.dates = []
        mmm = self.months[self.habMask0]
        yyy = self.years[self.habMask0]
        for month, year in zip(mmm, yyy):
            date = datetime.date(int(year), int(month), 1)
            self.dates.append(date)

    def nPredPlot(self):
        """
        plot predicted N and trap rate over time
        """
        minDate = datetime.date(2010, 1, 1)
        maxDate = datetime.date(2013, 12, 31)
#        minDate = datetime.date(1987, 1, 1)
#        maxDate = datetime.date(2015, 1, 1)
        P.figure(figsize=(14, 11))
        for i in range(2):
            mask_i = self.habIndx == i
            N_Hab = self.meanN[mask_i]
            tRate_i = self.trapRate[mask_i] / 100.0
            npred_i = self.nPred[mask_i] 
            ax1 = P.subplot(2, 1, i + 1)
#            lns1 = ax1.plot(self.dates, N_Hab, color = 'k', label = 'Pop. Size', linewidth = 1.25)
            lns3 = ax1.plot(self.dates, tRate_i, color = 'k', label = 'Trap rate', linewidth = 3.0)
#            ax2 = ax1.twinx()
#            lns2 = ax2.plot(self.dates, npred_i, color = 'r', label = 'Predicted Pop.', linewidth = 2.0)
            lns = lns3
            labs = [l.get_label() for l in lns]
            if i == 0:
                lTit = 'Irrigated'
                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            else:
                lTit = 'Upland'
                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            ax1.set_ylim([-.02, 0.25])
#            ax2.set_ylim([-20, 140.])
            ax1.set_xlim(minDate, maxDate)
            ax = P.gca()
            for tick in ax.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            for tick in ax.yaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            if i == 1:
                ax1.set_xlabel('Time', fontsize = 17)
            ax1.set_ylabel("Trap rate", fontsize = 17)
#            ax2.set_ylabel('Predicted N', fontsize = 17)
        figFname = 'TrapRateYear.png'
        P.savefig(figFname, format='png')
        P.show()                



    def plotMakeFigures(self):
        """
        plot growth rate over time
        """
#        minDate = datetime.date(2003, 1, 1)
#        maxDate = datetime.date(2007, 12, 31)
#        minDate = datetime.date(1990, 1, 1)
#        maxDate = datetime.date(2010, 12, 31)
        minDate = datetime.date(1987, 1, 1)
        maxDate = datetime.date(2015, 12, 1)
        P.figure(figsize=(14, 11))
        for i in range(2):
            mask_i = self.habIndx == i
            ax1 = P.subplot(2, 1, i + 1)
#            pGrowth = self.pGrowth[mask_i]
#            harmonics = self.harmonics[mask_i]
            rain_i = self.rainDat[mask_i]
            rainT_1 = self.Raint_1[mask_i] 
            npred_i = self.nPred[mask_i]
            tRate_i = self.trapRate[mask_i]
#            pRice = self.tArray[mask_i, 7]
#            pVeg = self.tArray[mask_i, 8]
#            pTrees = self.tArray[mask_i, 9]
#            pRice = self.tArray[mask_i,6 ]
            pVeg = self.tArray[mask_i, 6]
            pTrees = self.tArray[mask_i, 7]
#            lns1 = ax1.plot(self.dates, npred_i, color = 'k', label = 'Population size', linewidth = 3.0)
#            lns1 = ax1.plot(self.dates, tRate_i, color = 'k', label = 'Trap rate (x100)', linewidth = 2.0)
            lns1 = ax1.plot(self.dates, tRate_i, color = 'k', linewidth = 2.0)
            
###            ax2 = ax1.twinx()
###            lns2 = ax2.plot(self.dates, rain_i, color = 'b', label = 'Rainfall (mm)', linewidth = 1.0)
#            lns3 = ax2.plot(self.dates, pRice , color = 'y', label = '%Rice', linewidth = 2.0)
#            lns4 = ax2.plot(self.dates, pVeg , color = 'r', label = '%Veg', linewidth = 2.0)
#            lns5 = ax2.plot(self.dates, pTrees , color = 'g', label = 'Trees crop', linewidth = 4.0)
###            lns = lns1 # + lns2
###            labs = [l.get_label() for l in lns]
###            if i == 0:
###                lTit = 'Lowland'
###                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
###            else:
###                lTit = 'Upland'
###                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            ax1.set_ylim(0.0, 35.0)
            ax1.set_xlim(minDate, maxDate)
#            ax2.set_ylim([-0.02, 1.05])
###            ax2.set_ylim([0.0, 400.0])

            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            for tick in ax1.yaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            ax1.set_xlabel('Time', fontsize = 17)
            ax1.set_ylabel('Monthly trap rate (x 100)', fontsize = 17)
###            ax2.set_ylabel("Monthly rainfall (mm)", fontsize = 17)
        figFname = 'trapRate_No_Rain.png'
        P.savefig(figFname, format='png', dpi = 1000)
        P.show()


    def plotGrowthRate(self):
        """
        plot growth rate over time
        """
#        minDate = datetime.date(2009, 1, 1)
#        maxDate = datetime.date(2014, 12, 31)


        minDate = datetime.date(2011, 1, 1)
        maxDate = datetime.date(2013, 12, 31)
#        minDate = datetime.date(1987, 1, 1)
#        maxDate = datetime.date(2015, 1, 1)
        P.figure(figsize=(14, 11))
        for i in range(2):
            mask_i = self.habIndx == i
            ax1 = P.subplot(2, 1, i + 1)
#            pGrowth = self.pGrowth[mask_i]
            harmonics = self.harmonics[mask_i]
#            pRice = self.tArray[mask_i, 6]
            pVeg = self.tArray[mask_i, 6]
            pTrees = self.tArray[mask_i, 7]
#            lns1 = ax1.plot(self.dates, pGrowth, color = 'k', label = 'Pop growth', linewidth = 3.0)
            lns2 = ax1.plot(self.dates, harmonics, color = 'b', label = 'Pop growth', linewidth = 3.0)
            ax2 = ax1.twinx()
            lns3 = ax2.plot(self.dates, pRice , color = 'y', label = '%Rice', linewidth = 1.0)
            lns4 = ax2.plot(self.dates, pVeg , color = 'r', label = '%Veg', linewidth = 1.0)
            lns = lns2 + lns3 + lns4
            labs = [l.get_label() for l in lns]
            if i == 0:
                lTit = 'Irrigated'
                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            else:
                lTit = 'Non-irri2gated'
                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            ax1.legend(lns, labs, loc = 'upper right')
            ax1.set_ylim(-1.3, 2.0)
            ax1.set_xlim(minDate, maxDate)
            ax2.set_ylim([0, 1.1])

            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            for tick in ax1.yaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            ax1.set_xlabel('Time', fontsize = 17)
            ax1.set_ylabel('Pop. growth rate', fontsize = 17)
            ax2.set_ylabel("Percent vegetables", fontsize = 17)
        P.show()
 

    def plotNpredDetect(self):
        """
        plot predicted N and trap rate over time and pdetection
        """
#        minDate = datetime.date(2009, 1, 1)
#        maxDate = datetime.date(2014, 12, 31)

#        minDate = datetime.date(1987, 1, 1)
#        maxDate = datetime.date(2005, 12, 31)
        minDate = datetime.date(1987, 1, 1)
        maxDate = datetime.date(2015, 1, 1)
        P.figure(figsize=(14, 11))
        for i in range(2):
            mask_i = self.habIndx == i
            N_Hab = self.meanN[mask_i]
            tRate_i = (self.trapRate[mask_i] / 100.0)
            npred_i = self.nPred[mask_i]
            theta_i = self.theta[mask_i] 
            nTrapped_i = self.nTrapped[mask_i]
            ax1 = P.subplot(2, 1, i + 1)
            lns1 = ax1.plot(self.dates, N_Hab, color = 'g', label = 'Pop. Size', linewidth = 1.25)
            lns2 = ax1.plot(self.dates, npred_i, color = 'b', label = 'Predicted Pop.', linewidth = 3.0)
            ax2 = ax1.twinx()
            lns3 = ax2.plot(self.dates, tRate_i, color = 'r', label = 'Trap rate', linewidth = 1.0)
            lns4 = ax2.plot(self.dates, theta_i, color = 'k', label = 'Prob. Capture', linewidth = 1.0)
            lns = lns1 + lns2 + lns3 + lns4
            labs = [l.get_label() for l in lns]
            if i == 0:
                lTit = 'Irrigated'
                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            else:
                lTit = 'Non-irrigated'
                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            ax1.set_ylim([0, 210])
            ax2.set_ylim([0, 1.0])
            ax1.set_xlim(minDate, maxDate)
            ax = P.gca()
            for tick in ax.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            for tick in ax.yaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            if i == 1:
                ax1.set_xlabel('Time', fontsize = 17)
            ax1.set_ylabel('Predicted pop.', fontsize = 17)
    #        P.xticks([datetime.date(2009, 1, 1), datetime.date(2011, 1, 1), datetime.date(2013, 1, 1),
    #                    datetime.date(2015, 1, 1), datetime.date(2017, 1, 1), datetime.date(2019, 1, 1)])
            ax2.set_ylabel("Trap rate and Prob. capt.", fontsize = 17)
        P.show()                


    def plotNpredPGrowth(self):
        """
        plot predicted N and trap rate over time and pdetection
        """
#        minDate = datetime.date(1997, 1, 1)
#        maxDate = datetime.date(2005, 12, 31)
        minDate = datetime.date(2009, 1, 1)
        maxDate = datetime.date(2014, 12, 31)

#        minDate = datetime.date(1987, 1, 1)
#        maxDate = datetime.date(1988, 12, 31)
#        minDate = datetime.date(1987, 1, 1)
#        maxDate = datetime.date(2015, 1, 1)
        P.figure(figsize=(14, 11))
        for i in range(2):
            mask_i = self.habIndx == i
            npred_i = self.nPred[mask_i]
            pGrowth_i = self.harmonics[mask_i]
#            nTrapped_i = self.nTrapped[mask_i]
            ax1 = P.subplot(2, 1, i + 1)
#            lns1 = ax1.plot(self.dates, N_Hab, color = 'k', label = 'Pop. Size', linewidth = 1.25)
            lns2 = ax1.plot(self.dates, npred_i, color = 'b', label = 'Predicted Pop.', linewidth = 3.0)
            ax2 = ax1.twinx()
            lns3 = ax2.plot(self.dates, pGrowth_i, color = 'r', label = 'Pop. growth rate', linewidth = 1.0)
            lns = lns2 + lns3
            labs = [l.get_label() for l in lns]
            if i == 0:
                lTit = 'Irrigated'
                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            else:
                lTit = 'Non-irrigated'
                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            ax1.set_ylim([0, 110])
            ax2.set_ylim([0, 2.5])
            ax1.set_xlim(minDate, maxDate)
            ax = P.gca()
            for tick in ax.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            for tick in ax.yaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            ax1.set_ylabel('Predicted pop.', fontsize = 17)
    #        P.xticks([datetime.date(2009, 1, 1), datetime.date(2011, 1, 1), datetime.date(2013, 1, 1),
    #                    datetime.date(2015, 1, 1), datetime.date(2017, 1, 1), datetime.date(2019, 1, 1)])
            ax2.set_ylabel("Pop. growth rate", fontsize = 17)
        P.show()


    def plotRainDetect(self):
        """
        plot rainfall and pdetection
        """
        minDate = datetime.date(1988, 1, 1)
        maxDate = datetime.date(1992, 12, 31)
#        minDate = datetime.date(1987, 1, 1)
#        maxDate = datetime.date(2015, 1, 1)
        P.figure(figsize=(14, 11))
        for i in range(2):
            mask_i = self.habIndx == i
            tRate_i = (self.trapRate[mask_i] / 100.0)
            theta_i = self.theta[mask_i] 
            rain_i = self.rainDat[mask_i]
            nTrapSet_i = self.nTrapSet[mask_i]
            ax1 = P.subplot(2, 1, i + 1)
#            lns1 = ax1.plot(self.dates, nTrapSet_i, color = 'k', label = 'Number of set traps', linewidth = 1.25)
            lns2 = ax1.plot(self.dates, rain_i, color = 'b', label = 'Rainfall', linewidth = 3.0)
            ax2 = ax1.twinx()
            lns4 = ax2.plot(self.dates, theta_i, color = 'r', label = 'Prob. Capture', linewidth = 3.0)
            lns = lns2 + lns4
            labs = [l.get_label() for l in lns]
            if i == 0:
                lTit = 'Irrigated'
                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
            else:
                lTit = 'Upland'
                ax1.legend(lns, labs, title = lTit, loc = 'upper right')
#            ax1.set_ylim([50, 820])
            ax2.set_ylim([0, 1.0])
            ax1.set_xlim(minDate, maxDate)
            ax = P.gca()
            for tick in ax.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            for tick in ax.yaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            if i == 1:
                ax1.set_xlabel('Time', fontsize = 17)
            ax1.set_ylabel('Rainfall (mm) and no. traps', fontsize = 17)
    #        P.xticks([datetime.date(2009, 1, 1), datetime.date(2011, 1, 1), datetime.date(2013, 1, 1),
    #                    datetime.date(2015, 1, 1), datetime.date(2017, 1, 1), datetime.date(2019, 1, 1)])
            ax2.set_ylabel("Prob. capture", fontsize = 17)
        figFname = 'pCaptRainfall.png'
        P.savefig(figFname, format='png')
        P.show()                



########            Write data to file
########
    def writeToFileFX(self, sumTableName):
        (m, n) = self.summaryTable.shape
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Names', 'U12'), ('Low CI', np.float),
                    ('Median', np.float), ('High CI', np.float), ('Mean', np.float)])
        # copy data over
        structured['Low CI'] = self.summaryTable[:, 0]
        structured['Median'] = self.summaryTable[:, 1]
        structured['High CI'] = self.summaryTable[:, 2]
        structured['Mean'] = self.summaryTable[:, 3]
        structured['Names'] = self.names
        tableFname = os.path.join(self.rodentpath, sumTableName)
        np.savetxt(tableFname, structured, fmt=['%s', '%.4f', '%.4f', '%.4f', '%.4f'],
                 comments='', header='Names Low_CI Median High_CI Mean')

#def main(params):
def main():
    # paths and data to read in
    rodentpath = os.getenv('CHINAPROJDIR', default='.')

    weatherVariable = 'tmp0Rice'    # 'vegTree' # 'tmp0Rice', Temp
    monthLag = np.array([6,4])

    pklName = ('gibbs' + weatherVariable + str(monthLag[0]) + '_' + str(monthLag[1]) + '.pkl')
    print('gibbs Pickle Name:', pklName)
#    inputGibbs = os.path.join(rodentpath, 'gibbsRainTemp6_4.pkl')
    inputGibbs = os.path.join(rodentpath, pklName)
    fileobj = open(inputGibbs, 'rb')
    gibbsobj = pickle.load(fileobj)
    fileobj.close()

    resultsobj = ResultsProcessing(gibbsobj, rodentpath)

    resultsobj.getSummaryMeans()
#    resultsobj.getPopNames()

    resultsobj.makeTableFX()

    resultsobj.getTrapNAN()
    resultsobj.getNPred()
    resultsobj.getDates()
    # diagnostic trace plots
    resultsobj.plotFX()

    resultsobj.plotMakeFigures()
#    resultsobj.nPredPlot() 

#    resultsobj.plotGrowthRate()   
#    resultsobj.plotNpredPGrowth()
#    resultsobj.plotNpredDetect()
#    resultsobj.plotRainDetect()

    sumTableName = ('sumTable' + weatherVariable + str(monthLag[0]) + '_' + str(monthLag[1]) + '.txt')
    print('Summary Table Name:', sumTableName)

    resultsobj.writeToFileFX(sumTableName)


if __name__ == '__main__':
    main()

