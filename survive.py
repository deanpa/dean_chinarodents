#!/usr/bin/env python

import os
from scipy import stats
from scipy.special import gammaln
from scipy.special import gamma
import numpy as np
#from numba import jit
import params
import pickle
import pylab as P



class TrialSurvive(object):
    def __init__(self):
        """
        ## trial survivorship function
        """
        self.delta = 0.05
        self.nTime = 48
        self.time = np.arange(self.nTime)
        self.sigma = 0.1
        self.rmax = 2.5
        self.N0 = 4
        self.N = np.zeros(self.nTime)

        ######  Run functions
        self.simulatePop()
        self.plotResults()
        #######################

    def simulatePop(self):
        """
        ## simulate population and survival
        """
        nRec = self.N0 * self.rmax
        S = np.exp(-self.N0 * self.delta)
        mu = (self.N0 + nRec) * S
        print(nRec, mu, np.exp(np.random.normal(np.log(mu), self.sigma, size=1)))
        self.N[0] = np.exp(np.random.normal(np.log(mu), self.sigma, size=1))
        for i in range(1, self.nTime):
            nRec = self.N[i - 1] * self.rmax
            S = np.exp(-self.N[i-1] * self.delta)
            mu = (self.N[i - 1] + nRec) * S
            self.N[i] = np.exp(np.random.normal(np.log(mu), self.sigma, size=1))
            print('time', i, 'N', self.N[i])
        
    def plotResults(self):
        P.figure()
        P.plot(self.time, self.N)
        P.show()
        


########            Main function
#######
def main():
    trialsurvive = TrialSurvive()

if __name__ == '__main__':
    main()

