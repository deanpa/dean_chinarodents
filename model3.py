#!/usr/bin/env python

#import sys
import os
import pickle
import processRawData
import mcmcApodemus
import numpy as np

class Params(object):
    def __init__(self):
        """
        Object to set initial parameters
        """
        ############################################################
        ###################################### USER MODIFY HERE ONLY
        ## Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 10 #8000   # 2500      # number of estimates to save for each parameter
        self.thinrate = 1   #20   # 200      # thin rate
        self.burnin = 0         # burn in number of iterations
        ## array of iteration id to keep
        self.keepseq = np.arange(self.burnin, ((self.ngibbs * self.thinrate) + self.burnin),
            self.thinrate)
        ###################################################
        ###################################################
        ## Give model name
        self.modelNumber = 3
        self.modelName = 'Model' + str(self.modelNumber)   
        ## If have not manipulated raw data, set to 'True'   
        self.processData = True    ## True or False
        ## If need to make BasicData
        self.makeBasicData = True
        print('#################################')
        print('Model Name:', self.modelName)
        print('notes: 2 degree polynomial, no lag in temp effect of temperature') 
        print('       AR2 for pregnacy rate')
        print('Process Data?:', self.processData)
        print('Make basicdata?:', self.makeBasicData)
        print('Keep MCMC iters (ngibbs):', self.ngibbs)
        print('Thin rate:', self.thinrate)
        print('Burn in:', self.burnin)
        print('#################################')
        ######################################
        # modify  variables used for population model
#        ## set the lag months for the two peaks
#        self.peakOneMonths = 2  # np.array([10,8])
#        self.peakTwoMonths = 2  # np.array([10,8])
#        self.nPeaks = 0      # 0 = 12 months; 1 = Apr-Sep; 2 = Apr-May & Aug-Sep
#        self.useHarmonics = False   # True or False

        ## pregnancy - recruitment lag
        self.reproMonthsBack = 2
        ## Weather effects on PR or Males testicles months back
        self.weatherMonthsBack = 0         
        ## Set covariates
        self.covNames = 'tmp0Rice'     # Rain, Temp, Rain0Rice, vegTree, tmp0Rice
        ## number of fetuses per female for all months
        self.nFetusPerPreg = 5.0     # equivalent to overall median
        
        ###################################################


        ###################################################
        ###############      Initial values parameters for updating
        self.sigma = np.array([1.0, 1.0])
        self.sigmaPriors = np.array([0.05, 0.1])       # gamma shape and rate!!!

        # Population model
        self.b = np.array([0.007, 0.009])              # density dependence by hab
        self.bPrior = np.array([0.05, 10.0])           # gamma prior: shape and scale 
        self.bJump = 0.2

        ## Parameters inter, Temp, Temp2, Veg, trees, AR1_PR, AR2_PR
        self.alpha = np.array([-15.0, 11.5, -1.75, -0.1, -0.1, -0.05, -0.76,])    
        # normal alpha priors
        self.alpha_Prior = np.array([0.0, np.sqrt(3000)])
#        self.alpha_search = np.array([.001, .001, 0.001, .001, .001, 0.001])
        self.alpha_search = np.array([0.01, 0.01, 0.01, 0.01, 0.01, 
            0.05, 0.05])
#        self.sigma_search = .1

        ## if have AR1 on rho
        if self.modelNumber == 3:
            ## Initial logitRhoStar parameters by habitat
            self.logitRho_1 = np.array([-3.5, -3.5])
            self.logitRho_2 = np.array([-3.0, -3.0])
            self.logitRho12_search = 0.01
            self.rho0Prior = np.array([-3.0, 2.0])
            self.rho_search = 1.0   #0.75      #0.3
            ## sigma RHO parameter
            self.sigmaRho = 1.0
            self.priorSigmaRho = np.array([0.05, 0.1])

            self.diag = np.diagflat(np.tile(1000., len(self.alpha)))
            self.vinvert = np.linalg.inv(self.diag)
            self.s1 = .1
            self.s2 = .1
            self.ppart = np.dot(self.vinvert, np.zeros(len(self.alpha)))


        # Detection model
        self.g = np.array([-7.75, .19])                # covariates on detection (Rain)
        self.n_gPara = len(self.g)
        # priors on rain detection coefficients
        self.gPrior = np.array([0.0, np.sqrt(100.)])
        self.ngcov = len(self.g)
        self.maxN =  600

        self.g_search = np.array([.07, .005])
        # no intercept

        self.followSess = np.array([5, 23, 100, 174, 260, 341])
#        self.nSample = np.arange(-10, 10, dtype = int)
#        self.nSample = self.nSample[self.nSample != 0]
        self.XtJump = 1.0

        ######################################
        ## set paths to scripts and data
        self.inputDataPath = os.path.join(os.getenv('CHINAPROJDIR', default='.'))
        self.outputDataPath = os.path.join(os.getenv('CHINAPROJDIR', default='.'), 
            'Results_' + self.modelName)
        if not os.path.isdir(self.outputDataPath):
            os.mkdir(self.outputDataPath)

        ## Raw data file names and paths
        self.rawTrapFName = os.path.join(self.inputDataPath,'YuqingCommunity.csv')
        self.rawRainFName = os.path.join(self.inputDataPath,'rainYuqing.csv')
        self.rawTemperatureFName = os.path.join(self.inputDataPath,'tempYuqing.csv')
        self.rawMorphFName = os.path.join(self.inputDataPath,'Morph_Apodemus_agrarius.csv')
        self.rawCropFName = os.path.join(self.inputDataPath,'cropType_Deng.csv')
        self.rawReproDatFName = os.path.join(self.inputDataPath,'reproHabitatData.csv')

 #       self.maskTrapDatName = ('self.maskT_' + self.modelName + '_All')
 #       self.rainCovName = ('self.rainT_' + self.modelName + '_All')
 #       self.tempCovName = ('self.tempT_' + self.modelName + '_All')
        #########

        self.gibbsName = ('gibbs_' + self.modelName + '.pkl')
        self.basicdataName = ('basicdata_' + self.modelName + '.pkl')
        self.manipdataName = ('manip_' + self.modelName + '.pkl')

        ######################################
        ######################################
        # modify  variables used for detection model
        self.xdatDictionary = {'intercept' : 0, 'Rain' : 1, 'RainSq' : 2}
        self.intercept = self.xdatDictionary['intercept']
        self.Rain = self.xdatDictionary['Rain']
        self.RainSq = self.xdatDictionary['RainSq']
        # array of index values to get variables from 2-d array
        self.xdatIndx = np.array([self.intercept, self.Rain, self.RainSq], dtype = int)
        ######################################
        ######################################



########            Main function
#######
def main():
    ## create instance of Params class
    params = Params()

    ## if raw data for model haven't been processed then run...
    if params.processData:
        rawdata = processRawData.RawData(params)
        apodemusdata = processRawData.ApodemusData(rawdata)
        # pickle apodemusdata to be used in mcmcApodemus.py
        outManipdata = os.path.join(params.outputDataPath, params.manipdataName)
        fileobj = open(outManipdata, 'wb')
        pickle.dump(apodemusdata, fileobj)
        fileobj.close()
        ## make basicdata
        basicdata = mcmcApodemus.BasicData(params, apodemusdata) 
    ## if need to make basicdata but not process apodemusdata then...
    elif params.makeBasicData:
        ## read in manipulated data from pickle
        apodemusFile = os.path.join(params.outputDataPath, params.manipdataName)
        fileobj = open(apodemusFile, 'rb')
        apodemusdata = pickle.load(fileobj)
        fileobj.close()
        ## initate basicdata Class
        basicdata = mcmcApodemus.BasicData(params, apodemusdata)
    ## If don't need to process rawdata and don't need to make basicdata:
    else:
        ## read in basicedata from pickle, not apodemusdata
        inputBasicdata = os.path.join(params.outputDataPath, params.basicdataName)
        fileobj = open(inputBasicdata, 'rb')
        basicdata = pickle.load(fileobj)
        fileobj.close()


    ## initiate mcmc class and object
    mcmcobj = mcmcApodemus.MCMC(params, basicdata)

    ## object created to pickle results
    gibbsobj = mcmcApodemus.Gibbs(mcmcobj, basicdata, params)

    ## pickle basic data from present run to be used to initiate new runs
    outBasicdata = os.path.join(params.outputDataPath, params.basicdataName)
    fileobj = open(outBasicdata, 'wb')
    pickle.dump(basicdata, fileobj)
    fileobj.close()

    ## pickle mcmc results for post processing in gibbsProcessing.py
    outGibbs = os.path.join(params.outputDataPath, params.gibbsName)
    fileobj = open(outGibbs, 'wb')
    pickle.dump(gibbsobj, fileobj)
    fileobj.close()


if __name__ == '__main__':
    main()

