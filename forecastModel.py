#!/usr/bin/env python

#######################################################
#######################################################
#######################################################
# This Python script contains functions for forecasting striped field mouse 
# (Apodemus agrarius) abundance in agricultural fields in Yuqing County, 
# Guizhou, China

# Copyright (C) 2017 Dean Anderson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#######################################################
#######################################################
#######################################################


import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
from scipy import stats
import prettytable
import pickle
import os
import datetime
import forecastParameters
#import pandas as pd

def logit(x):
    """
    Function (logit) to convert probability to real number
    """
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    """
    Function to convert real number to probability
    """
    return np.exp(x) / (1 + np.exp(x))


class PredictProcessing(object):
    def __init__(self, gibbsobj, params):
#    def __init__(self, gibbsobj, rodentpath, params):

        #####################################
        #####################################
        # Run functions

#        self.getData(gibbsobj, rodentpath, params)
        self.getData(gibbsobj, params)
        self.readForecastData()
        self.getDates()
        self.getLaggedRainTempData()
        self.getCropTrapData()
        self.getSinCosCovariates()
        self.makeDetectionCovariates()
        self.makeGrowthCovariate()
        
        self.simWrapper()
        self.getPlotMonths()
        self.trapRateTableFX()
        self.writeTrapRateTable()
        self.plotTrapRate()
   
        self.NTableFX()
        self.writeNTable()
        self.plotNForecast()

        #####################################
        #####################################

#    def getData(self, gibbsobj, rodentpath, params):
#        """
#        read in data from mcmc
#        """
#        self.rodentpath = rodentpath
    def getData(self, gibbsobj, params):
        """
        read in data from mcmc
        """
#        self.rodentpath = rodentpath
        self.params = params
        self.gibbsobj = gibbsobj
        self.ndat = len(self.gibbsobj.siggibbs)
        self.results = np.hstack([self.gibbsobj.bgibbs,
                                self.gibbsobj.alphagibbs,
                                self.gibbsobj.ggibbs,
                                self.gibbsobj.siggibbs])
#        self.agibbs = self.gibbsobj.agibbs
        self.bgibbs = self.gibbsobj.bgibbs
        self.alphagibbs = self.gibbsobj.alphagibbs
        self.nAlpha = np.shape(self.alphagibbs)[1]
        self.ggibbs = self.gibbsobj.ggibbs
        self.siggibbs = self.gibbsobj.siggibbs
        self.nMcmc = len(self.siggibbs)
        self.Ngibbs = self.gibbsobj.Ngibbs

        print('Number of MCMC iterations', self.ndat)
#        # prediction data arrays

#        self.rainLagCovAll = self.gibbsobj.rainLagCovAll
#        self.tempLagCovAll = self.gibbsobj.tempLagCovAll
        self.monthsAll = self.gibbsobj.monthsAll
#        self.yearsAll = self.gibbsobj.yearsAll
#        self.Raint_1All = self.gibbsobj.Raint_1All
#        self.riceAll = self.gibbsobj.riceAll
#        self.vegetablesAll = self.gibbsobj.vegetablesAll
#        self.treesAll = self.gibbsobj.treesAll
#        self.rainDatAll = self.gibbsobj.rainDatAll
#        self.temperatureDatAll = self.gibbsobj.temperatureDatAll
#        self.monthRunAll = self.gibbsobj.monthRunAll
#        self.nsessionsAll = self.gibbsobj.nsessionsAll
#        self.sessionsAll = self.gibbsobj.sessionsAll
        self.nTrappedAll = self.gibbsobj.nTrappedAll
        self.habIndxAll = self.gibbsobj.habIndxAll
        self.nTrapSetAll = self.gibbsobj.nTrapSetAll
#        self.nDatAll = len(self.nTrappedAll)

        self.habIndx = self.gibbsobj.habIndx
        self.years = self.gibbsobj.years
        self.months = self.gibbsobj.months
#        self.sessions = self.gibbsobj.sessions
#        self.nsessions = self.gibbsobj.nsessions
        self.rainDat = self.gibbsobj.rainDat
#        self.rainLagCov = self.gibbsobj.rainLagCov
#        self.tempLagCov = self.gibbsobj.tempLagCov

    def readForecastData(self):
        """
        get weather, crop and trapping forecast data from *.csv files
        """
        #### WEATHER DATA
#        fnamePath = os.path.join(self.rodentpath, self.params.weatherFname)
        fnamePath = os.path.join(self.params.rodentInputPath, self.params.weatherFname)
        weather = np.genfromtxt(fnamePath, names=True, delimiter = ',',
            dtype=['i8', 'i8', 'f8', 'f8'])
        self.meanMonthRain = np.tile(weather['rainfall'], 2)
        self.meanMonthTemp = np.tile(weather['temperature'], 2)
        self.weatherMonth = np.tile(weather['month'], 2)
        self.weatherYear = np.tile(weather['year'], 2)
        # weather habitat data
        self.nWeather = len(self.weatherYear)
        self.weatherHabitat = np.zeros(self.nWeather, dtype = int)
        self.weatherHabitat[self.nWeather:] = 1
        #### CROP DATA
#        fnamePath = os.path.join(self.rodentpath, self.params.cropFname)
        fnamePath = os.path.join(self.params.rodentInputPath, self.params.cropFname)
        crops = np.genfromtxt(fnamePath, names=True, delimiter = ',',
            dtype=['i8', 'i8', 'i8', 'f8', 'f8', 'i8'])
        self.cropYear = crops['year']
        self.cropMonth = crops['month']
        self.cropHabitat = crops['habitat']
        self.cropRice = crops['rice']
        self.cropVeg = crops['vegetables']
        self.cropTrees = crops['trees']
        mask0 = self.cropHabitat == 0
        mask1 = self.cropHabitat == 1
        self.cropHabitat = np.append(self.cropHabitat[mask0], self.cropHabitat[mask1])
        self.cropYear = np.append(self.cropYear[mask0], self.cropYear[mask1])
        self.cropMonth = np.append(self.cropMonth[mask0], self.cropMonth[mask1])
        self.cropRice = np.append(self.cropRice[mask0], self.cropRice[mask1])
        self.cropVeg = np.append(self.cropVeg[mask0], self.cropVeg[mask1])
        self.cropTrees = np.append(self.cropTrees[mask0], self.cropTrees[mask1])
        #### TRAPPING DATA
#        fnamePath = os.path.join(self.rodentpath, self.params.trapFname)
        fnamePath = os.path.join(self.params.rodentInputPath, self.params.trapFname)
        traps = np.genfromtxt(fnamePath, names=True, delimiter = ',',
            dtype=['i8', 'i8', 'i8', 'i8', 'i8'])
        self.trapYear = traps['year']
        self.trapMonth = traps['month']
        self.trapsSet = traps['trapsSet']
        self.trapHabitat = traps['habitat']
        self.trapNCapt = traps['nCapt']
        mask0 = self.trapHabitat == 0
        mask1 = self.trapHabitat == 1
        self.trapHabitat = np.append(self.trapHabitat[mask0], self.trapHabitat[mask1])
        self.trapYear = np.append(self.trapYear[mask0], self.trapYear[mask1])
        self.trapMonth = np.append(self.trapMonth[mask0], self.trapMonth[mask1])
        self.trapsSet = np.append(self.trapsSet[mask0], self.trapsSet[mask1])
        self.trapNCapt = np.append(self.trapNCapt[mask0], self.trapNCapt[mask1])

    def getDates(self):
        """
        get current and forecasting dates with Datetime
        """
        self.dateCurrent = datetime.date(self.params.currentDate[0], 
            self.params.currentDate[1], 15)
        self.dateForecast = datetime.date(self.params.forecastDate[0],
            self.params.forecastDate[1], 15)
        ## Make array of dates
        self.dates = datetime.date(self.trapYear[0], self.trapMonth[0], 15)
        for i in range(1, self.nWeather):
            self.dates = np.append(self.dates, datetime.date(self.trapYear[i], 
                self.trapMonth[i], 15))
        self.predictMask = ((self.dates > self.dateCurrent) & 
            (self.dates <= self.dateForecast))
        self.currentDateMask = self.dates == self.dateCurrent
        ## forecast arrays
        self.forecastHabitat = self.trapHabitat[self.predictMask] 
        self.forecastDates = self.dates[self.predictMask]
        self.lagMax = self.params.laggedMonths[0]
        self.lagMin = self.params.laggedMonths[1]
        self.nlaggedMonths = self.params.laggedMonths[0] - self.params.laggedMonths[1] + 1
        self.nPredictions = np.sum(self.predictMask)
        
    def getLaggedRainTempData(self):
        """
        get lagged rain and temp data
        """
        ## empty rain and temperature arrays to populate with lagged data
        self.lagRainDat = np.zeros(self.nWeather)
        self.lagTempDat = np.zeros(self.nWeather)
        ## detection-rain covariate
        self.meanDetectRain = np.zeros(self.nWeather)
        ## forecast months for sin and cosine covariates
        self.monthForecast = np.zeros(self.nWeather)
        ## loop through all data
        for i in range(self.nWeather):
            ## operate only if it is a prediction date.
            if self.predictMask[i]:
                maxLag_i = i - self.lagMax
                minLag_i = i - self.lagMin + 1
                self.lagRainDat[i] = np.mean(self.meanMonthRain[maxLag_i : minLag_i])
                self.lagTempDat[i] = np.mean(self.meanMonthTemp[maxLag_i : minLag_i])
                self.monthForecast[i] = self.weatherMonth[i]
                self.meanDetectRain[i] = self.meanMonthRain[i]
        self.lagRainDat = np.log(self.lagRainDat[self.predictMask] + 1.0)
        self.lagTempDat = np.log(self.lagTempDat[self.predictMask] + 1.0)
        self.monthForecast = self.monthForecast[self.predictMask]
        self.uniqueMonth = np.unique(self.monthForecast)
        self.nUniqueMonths = len(self.uniqueMonth)
        self.meanDetectRain = np.log(self.meanDetectRain[self.predictMask] + 1.0) 
        self.forecastMonHab0 = self.monthForecast[self.forecastHabitat == 0]
        
    def getCropTrapData(self):
        """
        get crop data
        """
        ## crop data
        self.forecastVeg = self.cropVeg[self.predictMask]
        self.forecastTrees = self.cropTrees[self.predictMask]
        self.forecastRice = self.cropRice[self.predictMask] 
        ## trap data
        self.forecastTrapsSet = self.trapsSet[self.predictMask]
        self.forecastNCapt = self.trapNCapt[self.predictMask]
        ## number of traps set and number of captures in current month (2 hab types)
        self.currentTrapsSet = self.trapsSet[self.currentDateMask]
        self.currentNCapt = self.trapNCapt[self.currentDateMask]
        ## number of months to 
        self.nForecastMonths = len(self.monthForecast[self.forecastHabitat == 0])

    def getSinCosCovariates(self):
        """
        get the harmonic sine and cosine covariates for the forecast months
        """
        self.sin1 = np.sin(2.0 * np.pi * self.monthForecast / 12.0)
        self.cos1 = np.cos(2.0 * np.pi * self.monthForecast / 12.0)
        self.sin2 = np.sin(4.0 * np.pi * self.monthForecast / 12.0)
        self.cos2 = np.cos(4.0 * np.pi * self.monthForecast / 12.0)

    def makeDetectionCovariates(self):
        """
        make array of detection covariates
        """
        ## Array of covariates for starting detection model
        startDetectRain = np.log(self.meanMonthRain[self.currentDateMask] + 1.0)[0]
        self.startDetectCovArray = np.array([1.0, startDetectRain]).reshape(1,2)
        ## array of detection covariates for forecast months
        self.detectCovariates = np.ones((self.nPredictions, 2))
        self.detectCovariates[:, 1] = self.meanDetectRain

    def makeGrowthCovariate(self):
        """
        make 2-d array of covariates for population growth model
        """
        self.growthArray = np.ones((self.nPredictions, self.nAlpha))
        self.growthArray[:,1] = self.sin1
        self.growthArray[:,2] = self.cos1
        self.growthArray[:,3] = self.sin2
        self.growthArray[:,4] = self.cos2
        self.growthArray[:,-2] = self.forecastVeg
        self.growthArray[:,-1] = self.forecastTrees
        ## Environmental covariates conditioned on model
        ## options are Rain, Temp, Rain0Rice', tmp0Rice, vegTree
        if (self.params.covMod == 'tmp0Rice') | (self.params.covMod == 'Rain0Rice'):
            self.growthArray[:,-3] = self.forecastRice
        if (self.params.covMod == 'Rain') | (self.params.covMod == 'Rain0Rice'):
            self.growthArray[:,5] = self.lagRainDat 
        if (self.params.covMod == 'Temp') | (self.params.covMod == 'tmp0Rice'):
            self.growthArray[:,5] = self.lagTempDat 
        

    ############################################
    ############################################
    ### Functions for forecast             
    def simWrapper(self):
        """
        Wrapper fx for running forecast
        """
        self.iter = self.params.iter
        self.variateIndx = np.random.choice(self.nMcmc, self.iter)
        self.loopIter()     # run loop function
        
        
    def loopIter(self):
        """
        Create arrays and loop thru iterations
        """
        self.NForecast = np.zeros((self.iter, self.nPredictions))   # 2-d storage array N
        self.nCaptForecast = np.zeros((self.iter, self.nPredictions), dtype = int)   # 3-d storage array captureed
        for i in range(self.iter):
            # draw random varitates
            self.drawVariates(i)
            self.loopHabitatMonths(i)

    def drawVariates(self, i):
        """
        draw random variates for iteration i from gibbs
        """
        self.variate_i = self.variateIndx[i]
        self.b_i = self.bgibbs[self.variate_i]        
        self.alpha_i = self.alphagibbs[self.variate_i]
        self.g_i = self.ggibbs[self.variate_i]
        self.sigma_i = self.siggibbs[self.variate_i]

    def loopHabitatMonths(self, i):
        """
        loop thru months j.
        """
        ## loop Habitat first
        for j in range(2):
            self.maskHab_j = self.forecastHabitat == j
            self.b_ij = self.b_i[j]
            self.getStartN(j)
            ## loop through months
            for k in self.forecastMonHab0:
#            for k in self.uniqueMonth:      #range(self.nForecastMonths):
                self.habMonthMask = self.maskHab_j & (self.monthForecast == k)
                growthArray_jk = self.growthArray[self.habMonthMask]
                a_jk = np.dot(growthArray_jk, self.alpha_i)
                DD_jk = self.b_ij * self.x_j
                self.x_j = (self.x_j + a_jk + DD_jk)
                self.x_j = np.random.normal(self.x_j, self.sigma_i[j])
                self.N_j = np.exp(self.x_j)
                ## simulate the capture process
                self.captureProcess(i, j)

    def getStartN(self, j):
        """
        get starting N - of current month
        """
        theta0 = self.currentNCapt[j] / self.currentTrapsSet[j]
        rho0 = np.exp(np.dot(self.startDetectCovArray, self.g_i))
        self.N_j = -(np.log(1.0 - theta0)) / rho0
        self.x_j = np.log(self.N_j)
        
    def captureProcess(self, i, j):
        """
        # do random draw of captured individuals
        """
        trapset_jk = self.forecastTrapsSet[self.habMonthMask]
        rho_ijk = np.exp(np.dot(self.detectCovariates[self.habMonthMask], self.g_i))
        theta_ijk = 1.0 - np.exp(-rho_ijk * self.N_j)
        ncapt_ijk = np.random.binomial(trapset_jk, theta_ijk)
        ## populate arrays
        self.NForecast[i,self.habMonthMask] = self.N_j
        self.nCaptForecast[i, self.habMonthMask] = ncapt_ijk
                
    #######################
    ####################### TRAP RATE RESULTS
    #######################
    def trapRateTableFX(self):
        """
        make table of capture results, and historical captures summary
        """
        trapRate = (self.nCaptForecast / self.forecastTrapsSet.reshape(1, 
            self.nPredictions) * 100.0)
        self.trapRateTable = np.zeros(shape = (self.nPredictions, 8))
        self.trapRateTable[:,0] = self.monthForecast
        self.trapRateTable[:,1] = self.forecastHabitat
        self.trapRateTable[:,2] = np.round(np.mean(trapRate, axis = 0), 2)
        quantsTrapRate = np.round(mquantiles(trapRate, prob = [0.025, 0.975], 
            axis= 0), 2)
        self.trapRateTable[:, 3:5] = quantsTrapRate.transpose()
        ## get Historic mean capture summaries
        self.getHistoricTrapRate()
        print("########################################")
        print("#####  TRAP RATE FORECAST RESULTS  #####")
        aa = prettytable.PrettyTable(['Month', 'Habitat', 'Mean forecast', 
            'Forecast 2.5% CI', 'Forecast 97.5% CI', 'Mean historic', 
            'Historic 2.5% CI', 'Historic 97.5% CI'])
        for i in range(self.nPredictions):
            row = self.trapRateTable[i].tolist()
            aa.add_row(row)
        print(aa)
        
        
    def getHistoricTrapRate(self):
        """
        get means and CI of capture data for each calendar month of year
        """
        trapSetNAN = self.nTrapSetAll.astype(float)
        trapSetNAN[trapSetNAN <= 0.0] = np.nan
        for i in range(self.nPredictions):
            mask_i = ((self.habIndxAll == self.forecastHabitat[i]) & 
                (self.monthsAll == self.monthForecast[i]))
            trapRate_i = self.nTrappedAll[mask_i] / trapSetNAN[mask_i] * 100.0
            self.trapRateTable[i, 5] = np.round(np.nanmean(trapRate_i), 2)
            self.trapRateTable[i,6] = np.round(np.nanpercentile(trapRate_i, 2.5), 2)
            self.trapRateTable[i,7] = np.round(np.nanpercentile(trapRate_i, 97.5), 2)
        
        
    def writeTrapRateTable(self):
        """
        write txt file of trap rate table to directory
        """
        self.habitatString = np.repeat('Irrigated', self.nPredictions)
        self.habitatString[self.forecastHabitat == 1] = 'Upland'
        (m, n) = self.trapRateTable.shape
        ## create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Dates', datetime.date), ('Habitat', 'U10'), 
            ('Mean forecast', np.float), ('Forecast 2.5% CI', np.float),
                    ('Forecast 97.5% CI', np.float), ('Mean historic', np.float), 
                    ('Historic 2.5% CI', np.float), ('Historic 97.5% CI', np.float)])
        ## copy data over
        structured['Dates'] = self.forecastDates     # self.monthForecast
        structured['Habitat'] = self.habitatString      # self.forecastHabitat
        structured['Mean forecast'] = self.trapRateTable[:, 2]
        structured['Forecast 2.5% CI'] = self.trapRateTable[:, 3]
        structured['Forecast 97.5% CI'] = self.trapRateTable[:, 4]
        structured['Mean historic'] = self.trapRateTable[:, 5]
        structured['Historic 2.5% CI'] = self.trapRateTable[:, 6]
        structured['Historic 97.5% CI'] = self.trapRateTable[:, 7]
        tableName = ('trapRate_' + self.params.covMod + str(self.params.laggedMonths[0]) + 
            '_' + str(self.params.laggedMonths[1]) + '_Fore' + str(self.nUniqueMonths) +
            '.txt')
        tablePathFname = os.path.join(self.params.rodentOutputPath, tableName)
        np.savetxt(tablePathFname, structured, fmt=['%s', '%s', '%.4f', '%.4f', '%.4f', 
            '%.4f', '%.4f', '%.4f'], comments='', 
            header='Dates Habitat Forecast_mean Forecast_Lo_CI Forecast_Hi_CI Mean_historic Historic_Lo_CI Historic_Hi_CI')        


    def getPlotMonths(self):
        """
        get months as string for x-axis on plots
        """
        ## calendar months for plotting
        self.calendarMonths = np.array(['January', 'February', 'March', 'April', 'May',
            'June', 'July', 'August', 'September', 'October', 'November', 'December'])
        monIndx = self.monthForecast.astype(int) - 1
        monIndx = monIndx[:self.nUniqueMonths]
        self.calendarForecast = self.calendarMonths[monIndx]
#        print('calendar', self.calendarForecast)
 
            
    def plotTrapRate(self):
        """
        plot trap rate for forecast and historical means and CI
        """
        self.plotDates = self.forecastDates[:self.nUniqueMonths]
        P.figure(figsize=(14, 9))
        for i in range(4):
            if (i==0) | (i==2):
                table_i = self.trapRateTable[:, 2:5]
            else:
                table_i = self.trapRateTable[:, -3:]
            ax1 = P.subplot(2, 2, i + 1)
            if i <= 1:
                habMask = self.forecastHabitat == 0
                P.title('Irrigated', fontsize = 10)
            else:
                habMask = self.forecastHabitat == 1
                P.title('Upland', fontsize = 10)
            lns1 = ax1.plot(self.plotDates, table_i[habMask, 0], 'ko-', ms = 10,
                linewidth = 5, color = 'k', label = 'Mean traprate')
            lns2 = ax1.plot(self.plotDates, table_i[habMask, 1], 
                linewidth = 1.5, color = 'k',label = '95% CI traprate')
            lns3 = ax1.plot(self.plotDates, table_i[habMask, 2], 
                linewidth = 1.5, color = 'k')
            lns = lns1 + lns2  
            labs = [l.get_label() for l in lns]
            if i == 0:
                ax1.legend(lns, labs, loc = 'upper right')

            y_max = np.max(self.trapRateTable[habMask, 3:]) * 1.1
            y_min = np.min(self.trapRateTable[habMask, 3:]) * 0.9
            ax1.set_ylim([y_min, y_max])
            minDate = self.plotDates[0] - datetime.timedelta(days=4)
            maxDate = self.plotDates[-1] + datetime.timedelta(days=4)
            ax1.set_xlim(minDate, maxDate)
            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(12)
            if (i == 0) | (i == 2):
                ax1.set_ylabel('Forecasted traprate x 100', fontsize = 12)
            else:
                ax1.set_ylabel('Historic traprate x 100', fontsize = 12)
            if i > 1:
                ax1.set_xlabel('Months forecasted', fontsize = 12)
            if (i == 1) | (i == 3):
                P.xticks(self.plotDates, self.calendarForecast, rotation = 30, 
                    fontsize = 10)
            else:
                P.xticks(self.plotDates, rotation = 30, fontsize = 10)
        P.tight_layout(pad=.75, w_pad=.75)
#            if (i == 0) | (i == 2): 
#                ax1.set_xticks(self.plotDates)
#            else: 
#                ax1.set_xticks(self.monthForecast)
#            P.xticks(self.plotDates, self. rotation=30, size = 'small')
        figFname = ('trapRate_' + self.params.covMod + str(self.params.laggedMonths[0]) + 
            '_' + str(self.params.laggedMonths[1]) + '_Fore' + str(self.nUniqueMonths) +
            '.png')
        fnamePath = os.path.join(self.params.rodentOutputPath, figFname)
        P.savefig(fnamePath, format='png')
        P.show()
   
    
    #####################
    #######################     END TRAP RATE RESULTS
    #####################
    
    #####
    #############   N FORECAST RESULTS
    ######
    def NTableFX(self):
        """
        make table of N forecast results, and historical N summary
        """
        self.NTable = np.zeros(shape = (self.nPredictions, 8))
        self.NTable[:,0] = self.monthForecast
        self.NTable[:,1] = self.forecastHabitat
        self.NTable[:,2] = np.round(np.mean(self.NForecast, axis = 0), 3)
        quantsN = np.round(mquantiles(self.NForecast, prob = [0.025, 0.975], 
            axis= 0), 3)
        self.NTable[:, 3:5] = quantsN.transpose()
        ## get Historic mean capture summaries
        self.getHistoricN()
        aa = prettytable.PrettyTable(['Month', 'Habitat', 'Mean forecast', 
            'Forecast 2.5% CI', 'Forecast 97.5% CI', 'Mean historic', 
            'Historic 2.5% CI', 'Historic 97.5% CI'])
        for i in range(self.nPredictions):
            row = self.NTable[i].tolist()
            aa.add_row(row)
        print("########################################")
        print("#####  POPULATION SIZE FORECAST RESULTS  #####")
        print(aa)
        
    def getHistoricN(self):
        """
        get mean and 95% CI for modelled N from MCMC
        """
        for i in range(self.nPredictions):
            hab_i = self.forecastHabitat[i]
            mon_i = self.monthForecast[i]
            gibbsMask = (self.months == mon_i) & (self.habIndx == hab_i)
            N_i = self.Ngibbs[:, gibbsMask]
            self.NTable[i, 5] = np.round(np.nanmean(N_i), 3)
            self.NTable[i,6] = np.round(np.nanpercentile(N_i, 2.5), 3)
            self.NTable[i,7] = np.round(np.nanpercentile(N_i, 97.5), 3)
   

    def writeNTable(self):
        """
        write txt file of N forecasting table to directory
        """
        self.habitatString = np.repeat('Irrigated', self.nPredictions)
        self.habitatString[self.forecastHabitat == 1] = 'Upland'
        (m, n) = self.trapRateTable.shape
        ## create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Dates', datetime.date), ('Habitat', 'U10'), 
            ('Mean forecast', np.float), ('Forecast 2.5% CI', np.float),
                    ('Forecast 97.5% CI', np.float), ('Mean historic', np.float), 
                    ('Historic 2.5% CI', np.float), ('Historic 97.5% CI', np.float)])
        ## copy data over
        structured['Dates'] = self.forecastDates     # self.monthForecast
        structured['Habitat'] = self.habitatString      # self.forecastHabitat
        structured['Mean forecast'] = self.NTable[:, 2]
        structured['Forecast 2.5% CI'] = self.NTable[:, 3]
        structured['Forecast 97.5% CI'] = self.NTable[:, 4]
        structured['Mean historic'] = self.NTable[:, 5]
        structured['Historic 2.5% CI'] = self.NTable[:, 6]
        structured['Historic 97.5% CI'] = self.NTable[:, 7]
        tableName = ('NForecast_' + self.params.covMod + str(self.params.laggedMonths[0]) + 
            '_' + str(self.params.laggedMonths[1]) + '_Fore' + str(self.nUniqueMonths) +
            '.txt')
        tablePathFname = os.path.join(self.params.rodentOutputPath, tableName)
        np.savetxt(tablePathFname, structured, fmt=['%s', '%s', '%.4f', '%.4f', '%.4f', 
            '%.4f', '%.4f', '%.4f'], comments='', 
            header='Dates Habitat Forecast_mean Forecast_Lo_CI Forecast_Hi_CI Mean_historic Historic_Lo_CI Historic_Hi_CI')        

   

    def plotNForecast(self):
        """
        plot forecasted N and historical means and CI
        """
        P.figure(figsize=(14, 9))
        for i in range(4):
            if (i==0) | (i==2):
                table_i = self.NTable[:, 2:5]
            else:
                table_i = self.NTable[:, -3:]
            ax1 = P.subplot(2, 2, i + 1)
            if i <= 1:
                habMask = self.forecastHabitat == 0
                P.title('Irrigated', fontsize = 10)
            else:
                habMask = self.forecastHabitat == 1
                P.title('Upland', fontsize = 10)
            lns1 = ax1.plot(self.plotDates, table_i[habMask, 0], 'ko-', ms = 10,
                linewidth = 5, color = 'k', label = 'Mean N')
            lns2 = ax1.plot(self.plotDates, table_i[habMask, 1], 
                linewidth = 1.5, color = 'k',label = '95% CI N')
            lns3 = ax1.plot(self.plotDates, table_i[habMask, 2], 
                linewidth = 1.5, color = 'k')
            lns = lns1 + lns2  
            labs = [l.get_label() for l in lns]
            if i == 0:
                ax1.legend(lns, labs, loc = 'upper right')
            y_max = np.max(self.NTable[habMask, 2:]) * 1.1
            y_min = np.min(self.NTable[habMask, 2:]) * 0.9
            ax1.set_ylim([y_min, y_max])
            minDate = self.plotDates[0] - datetime.timedelta(days=4)
            maxDate = self.plotDates[-1] + datetime.timedelta(days=4)
            ax1.set_xlim(minDate, maxDate)
            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(12)
            if (i == 0) | (i == 2):
                ax1.set_ylabel('Forecasted N', fontsize = 12)
            else:
                ax1.set_ylabel('Historic N', fontsize = 12)
            if i > 1:
                ax1.set_xlabel('Months forecasted', fontsize = 12)
            if (i == 1) | (i == 3):
                P.xticks(self.plotDates, self.calendarForecast, rotation = 30,
                    fontsize = 10)
            else:
                P.xticks(self.plotDates, rotation = 30, fontsize = 10)
        P.tight_layout(pad=.75, w_pad=.75)
#            ax1.set_xticks(self.plotDates)
#            P.xticks(rotation=30, size = 'small')
        figFname = ('NForecast_' + self.params.covMod + str(self.params.laggedMonths[0]) + 
            '_' + str(self.params.laggedMonths[1]) + '_Fore' + str(self.nUniqueMonths) +
            '.png')
        fnamePath = os.path.join(self.params.rodentOutputPath, figFname)
        P.savefig(fnamePath, format='png')
        P.show()
        


def main():

    ## paths and data to read in
#    rodentpath = os.getenv('CHINAPROJDIR', default='.')

    ## Instance of Parameters class
    params = forecastParameters.Params() 
    params.rodentInputPath = params.inputDataPath
    params.rodentOutputPath = params.outputDataPath

    ## file name from MCMC to read in
    
    gibbsName = ('gibbs' + params.covMod + str(params.laggedMonths[0]) + '_' +
            str(params.laggedMonths[1]) + '.pkl')

    ## load MCMC results from pickle
    inputGibbs = os.path.join(params.rodentInputPath, gibbsName)
    fileobj = open(inputGibbs, 'rb')
    gibbsobj = pickle.load(fileobj)
    fileobj.close()

    predictobj = PredictProcessing(gibbsobj, params)



if __name__ == '__main__':
    main()


