#!/usr/bin/env python

import os
from scipy import stats
from scipy.special import gammaln
from scipy.special import gamma
import numpy as np
#from numba import jit
import pickle
import pylab as P


def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))


def vupdate(y, pred, s1, s2, n):
#    pred = np.dot(x, para)
#    pred = pred.transpose()
    predDiff = y - pred
#    mufx = self.params.y - pred
#    muTranspose = mufx.transpose()
#    sx = np.dot(mufx, muTranspose)
    sx = np.sum(predDiff**2.0)
    u1 = s1 + np.multiply(.5, n)
    u2 = s2 + np.multiply(.5, sx)               # rate parameter    
    isg = np.random.gamma(u1, 1./u2, size = None)            # formulation using shape and scale
    sg = 1.0/isg
    return(sg)


class BasicData(object):
    def __init__(self, params, apodemusdata):
        """
        Object to read in apodemus data
        Import updatable params from params
        """
        #############################################
        ############ Run basicdata functions
        self.getParams(params)
        self.getVariables(apodemusdata)
        self.maskOutData()
#        self.makeCovariates()
        if self.params.modelNumber < 5:
            self.getInitialXt()
        self.getInitialDetection()
        self.getInitialRho()
        self.getInitialSurvivorship()
        self.getNRecruits() 
#        self.populationArrayPDF()
#        self.binomCombinTerm()
        ############## End run of basicdata functions
        #############################################

    def getParams(self, params):
        """
        # move params parameters into basicdata
        """
        self.params = params
        self.sigma = self.params.sigma          # population variance
        self.sigmaSqrt = np.sqrt(self.sigma)    # population standard deviation
        self.b = self.params.b.copy()                   # density dependence by hab
        self.alpha = self.params.alpha.copy()          # repro covariate parameters
        self.g = self.params.g.copy()                # covariate parameters on detection (Rain)
        if self.params.modelNumber == 3:
            self.logitRho_1 = self.params.logitRho_1.copy()
            self.logitRho_2 = self.params.logitRho_2.copy()
        self.sigmaRho = self.params.sigmaRho
        self.sigmaRhoSqrt = np.sqrt(self.sigmaRho)
 
    def getVariables(self, apodemusdata):
        """
        get variables from apodemusdata
        """ 
        # move apodemusdata variables into basicdata
        self.apodemusdata = apodemusdata
        # move variables
        self.nsessions = self.apodemusdata.nsessions      # n sessions per habitat
        self.datSessions = self.apodemusdata.datSessions
        self.datNTrapped = self.apodemusdata.datNTrapped
        self.datNSetTraps = self.apodemusdata.datNSetTraps
        self.datMonthRun = self.apodemusdata.datMonthRun  # from 13 up
        self.datMonth = self.apodemusdata.datMonth
        self.datYear = self.apodemusdata.datYear
        self.datHabitat = self.apodemusdata.datHabitat    # 0 = lowland and 1 = upland
        self.habMask0 = self.datHabitat == 0
        self.habMask1 = self.datHabitat == 1
        self.datStartMonth = apodemusdata.datStartMonth
        self.datMinMonthRun = apodemusdata.datMinMonthRun
        self.datMaxMonthRun = apodemusdata.datMaxMonthRun
        self.reproMask = apodemusdata.reproMask
        self.t_1ExtractMask = apodemusdata.t_1ExtractMask
        self.t_2Mask = apodemusdata.t_2Mask
        ## repro variables
        self.datNFemales = self.apodemusdata.datNFemales
        self.datNPregnant = self.apodemusdata.datNPregnant
        self.datPromTesticle = self.apodemusdata.datPromTesticle
        self.nDat = self.apodemusdata.nDat                # total data
        self.datRain = self.apodemusdata.datRain
        self.datTemperature = self.apodemusdata.datTemperature
        self.datRice = self.apodemusdata.datRice
        self.datRapeWheat = self.apodemusdata.datRapeWheat
        self.datVegetables = self.apodemusdata.datVegetables
        self.datTrees = self.apodemusdata.datTrees
        self.datDryCrop = self.apodemusdata.datDryCrop
        self.uDatYear = self.apodemusdata.uDatYear
        self.nDatYear = self.apodemusdata.nDatYear
        # Cross validation data
        self.monthCV = self.apodemusdata.monthCV
        self.yearCV = self.apodemusdata.yearCV
        self.cvDates = self.apodemusdata.cvDates
        self.nCV = self.apodemusdata.nCV
        self.cvPointMask = self.apodemusdata.cvPointMask
        # Covariates
        self.tArray = self.apodemusdata.tArray
        self.detectCovariates = self.apodemusdata.detectCovariates
        self.sigmaSQRTArray = self.sigmaSqrt[self.datHabitat]
        self.nSigUpdate = self.nsessions - 2

    def maskOutData(self):
        """
        mask out data from cross-validation points
        """
        self.buildNTrapped = self.datNTrapped.copy()             # ntrapped for analysis
        self.buildNTrapped[self.cvPointMask] = 0                 # make cv pt zero
        self.buildNSetTraps = self.datNSetTraps.copy()
        self.buildNSetTraps[self.cvPointMask] = 0
        ## repro variables
        self.buildNFemales = self.datNFemales.copy()
        self.buildNFemales[self.cvPointMask] = 0
        self.buildNPregnant = self.datNPregnant.copy()
        self.buildNPregnant[self.cvPointMask] = 0
        self.buildPromTesticle = self.datPromTesticle.copy()
        self.buildPromTesticle[self.cvPointMask] = 0

    def getInitialXt(self):
        """
        get initial xt and N 
        """
        self.N = np.zeros(self.nDat)
        ## loop thru habitat
        for i in range(2):
            ## loop thru months
            habMask = self.datHabitat == i
            meanCalendar = np.zeros(12)
            for j in range(1, 13):
                monMask = self.datMonth == j
                habMonMask = habMask & monMask
                ntrapped = self.datNTrapped[habMonMask]
#                meanNonZero = np.mean(ntrapped[ntrapped > 0])
                meanTr = np.mean(ntrapped)
                self.N[habMonMask] = meanTr * 1.75
#                self.N[habMonMask] = meanNonZero * 2.25
        for i in range(self.nDat):
            if (self.N[i] <= self.datNTrapped[i]) | (self.N[i] < 0.0):
                self.N[i] = self.datNTrapped[i] + 15
#                print('hab', i, 'mon', j, 'N', meanNonZero * 2.25)
        self.Xt = np.log(self.N)
        ## get min and max Xt for truncated normal Xt updater
        self.minXt = np.zeros(self.nDat)
        self.minXt[self.buildNTrapped > 0] = np.log(self.buildNTrapped[self.buildNTrapped > 0])  
        self.maxXt = np.log(self.params.maxN)    

    def getInitialDetection(self):
        """
        get initial lambda and theta 
        """
#        self.nonZeroTrapMask = self.nTrapSet > 0
#        self.lambdaPred = np.zeros(self.nDat)
        self.lambdaPred = np.exp(np.dot(self.detectCovariates, self.g))
        expTerm = self.N * self.lambdaPred
        self.theta = 1.0 - np.exp(-expTerm)
#        prArr = np.hstack([self.lambdaPred, self.theta])
#        print('lam and th', prArr[0:50])
        self.detectPMF = stats.binom.logpmf(self.buildNTrapped, self.buildNSetTraps, self.theta)

    def getInitialRho(self):
        """
        ## get initial predicted Rho reproductive parameters    
        """
        ## If have AR2 for rho...
        if np.in1d(self.params.modelNumber, [3, 4, 5]):
            self.rho = np.zeros(self.nDat)          # latent RHO
            self.logit_rho = np.zeros(self.nDat)     # logit of latent RHO
            self.mu = np.zeros(self.nDat)
            self.sess0Mask = self.datSessions == 0
            self.sess1Mask = self.datSessions == 1
            self.sess01Mask = self.datSessions < 2

            ## Expand tArray for AR1 RHO
            addCol = np.zeros((self.nDat, 2))
            self.tArray = np.column_stack((self.tArray, addCol))
            self.tArray[self.sess0Mask, -1] = self.logitRho_2   # t=0 populate with t-2 RHO
            self.tArray[self.sess0Mask, -2] = self.logitRho_1   # t=0 populate with t-1 RHO
            self.tArray[self.sess1Mask, -1] = self.logitRho_1   # t=1 populate with t-2 RHO
            for i in range(self.nsessions):
                maskSess_i = self.datSessions == i
                self.mu[maskSess_i] = np.dot(self.tArray[maskSess_i], self.alpha)
                i_logitRho = np.random.normal(self.mu[i], .5)               # add some variation
                self.logit_rho[maskSess_i] = i_logitRho
                self.rho[maskSess_i] = inv_logit(i_logitRho)
                if i < self.nsessions - 1:
                    maskSess_up1 = self.datSessions == (i + 1)
                    self.tArray[maskSess_up1, -2] = i_logitRho                   
                    if i < self.nsessions - 2:
                        maskSess_up2 = self.datSessions == (i + 2)
                        self.tArray[maskSess_up2, -1] = i_logitRho
        else:
            self.logit_rho = np.dot(self.tArray, self.alpha)
            self.rho = inv_logit(self.logit_rho)
#            print('rho', self.rho[:100])
        self.sumPregRatePMF = np.sum(stats.binom.logpmf(self.buildNPregnant, 
            self.buildNFemales, self.rho))
#        print('pregPMF', self.pregRatePMF[:16], 'NFemales', self.buildNFemales[:16])
#        print('rho', self.logit_rho[:10], 'initial tArray', np.round(self.tArray[:10], 2))

    def getInitialSurvivorship(self):
        """
        ## get initial survivorship values for each habitat and session
        """
        self.Surv = np.zeros(self.nDat)
#        self.reproMask = self.datMonthRun >= self.datStartMonth
#        self.t_1ExtractMask = ((self.datMonthRun >= (self.datStartMonth - 1)) &
#                              (self.datMonthRun < self.datMaxMonthRun))
#        self.t_2Mask = self.datMonthRun <= (self.datMaxMonthRun - 2)
#        print('extr mask', self.t_1ExtractMask[:5], self.t_1ExtractMask[345:352],
#            self.t_1ExtractMask[-4:])
        b_Array = self.b[self.datHabitat]
        self.Surv[self.reproMask] = (np.exp(-self.N[self.t_1ExtractMask] * 
                            b_Array[self.reproMask]))
#        print('Survive', self.Surv[:25])
        ### Make repro and habitat mask for sigma update
        self.reproHab0Mask = self.reproMask & self.habMask0
        self.reproHab1Mask = self.reproMask & self.habMask1
        self.nReproHabSess = np.sum(self.reproHab0Mask)


    def getNRecruits(self):
        """
        ## get n recruits and the A mean of predicted N_t
        """
        self.nRecruits = np.zeros(self.nDat)
        self.A = np.zeros(self.nDat)
        self.A_log = np.zeros(self.nDat)

#        print('extr t-2 mask', self.t_2Mask[:5], self.t_2Mask[345:352],
#            self.t_2Mask[-4:])
        nRec_tmp =self.rho * self.N / 2.0 * self.params.nFetusPerPreg
        self.nRecruits[self.reproMask] = nRec_tmp[self.t_2Mask]
#        print('nRecruits', self.nRecruits[:15])
        Atmp = (self.N[self.t_1ExtractMask] + self.nRecruits[self.reproMask]) * self.Surv[self.reproMask]
        self.A[self.reproMask] = Atmp
        self.A_log[self.reproMask] = np.log(self.A[self.reproMask])
#        print('N', self.N[:15], 'A', self.A[:15], 'A_log', self.A_log[:15]) 

#    def populationArrayPDF(self):
#        """   
#        ## get intial population normal pdf
#        """
#        self.sigmaSQRTArray = self.sigmaSqrt[self.datHabitat]
#        self.Npdf = self.N[self.reproMask]
#        self.sigmaPDF = self.sigmaSQRTArray[self.reproMask]
#        self.Apdf = self.A[self.reproMask]
#        self.popPDF = stats.norm.logpdf(np.log(self.Npdf), np.log(self.Apdf), self.sigmaPDF)

#    def binomCombinTerm(self):
#        """
#        ## calc the binom combinatorial term for rho update pmf
#        """
#        nNotPreg = (self.datNFemales - self.datNPregnant)
#        self.binomCombo = (gammaln(self.datNFemales + 1) - gammaln(self.datNPregnant + 1) -
#                gammaln(nNotPreg + 1))

class MCMC(object):
    def __init__(self, params, basicdata):
        """
        Class to run mcmc
        """
        #############################################
        ############ Run MCMC functions
        self.storageArrays(params, basicdata)
        self.mcmcFX()
        ############## End run of MCMC functions
        #############################################

    def storageArrays(self, params, basicdata):
        """
        make storage arrays for parameters
        """
        self.basicdata = basicdata
        self.params = params
        self.nAlpha = len(self.basicdata.alpha)
        self.bgibbs = np.zeros((self.params.ngibbs, 2)) 
#        self.agibbs = np.zeros((self.params.ngibbs, 2)) 
        self.ggibbs = np.zeros((self.params.ngibbs, self.params.n_gPara)) 
        self.alphagibbs = np.zeros((self.params.ngibbs, self.nAlpha)) 
        self.siggibbs = np.zeros((self.params.ngibbs, 2))
        self.Ngibbs = np.zeros((self.params.ngibbs, self.basicdata.nDat)) 
        self.NHab0gibbs = np.zeros((self.params.ngibbs, self.basicdata.nsessions))
        ## if have AR1 rho effect, make storage array
        if self.params.modelNumber == 3:
            self.lrho_1gibbs = np.zeros((self.params.ngibbs, 2))
            self.lrho_2gibbs = np.zeros((self.params.ngibbs, 2))
            self.logit_rhogibbs = np.zeros((self.params.ngibbs, self.basicdata.nDat))
            self.sigmarhogibbs = np.zeros(self.params.ngibbs)
        self.a_pdf = np.zeros(self.basicdata.nDat)
        self.a_pdf_s = np.zeros(self.basicdata.nDat)
        self.logA_s = np.zeros(self.basicdata.nDat)

    def b_UpdateFX(self):
        """
        update survivorship parameters for each habitat
        """
        b_s = np.exp(np.random.normal(np.log(self.basicdata.b), self.params.bJump))
        bArray_s = b_s[self.basicdata.datHabitat]
        for i in range(2):
            if i == 0:
                habMask = self.basicdata.habMask0
                repHabMask = self.basicdata.reproHab0Mask.copy()
                extract_1HabMask = self.basicdata.t_1ExtractMask & habMask
            else:
                habMask = self.basicdata.habMask1
                repHabMask = self.basicdata.reproHab1Mask.copy()
                extract_1HabMask = self.basicdata.t_1ExtractMask & habMask
            survive_s = (np.exp(-self.basicdata.N[extract_1HabMask] * 
                   b_s[i]))
            A_s = (self.basicdata.N[extract_1HabMask] + 
                self.basicdata.nRecruits[repHabMask]) * survive_s
            A_log_s = np.log(A_s)
            Xt = self.basicdata.Xt[repHabMask]
            pdf_i = stats.norm.logpdf(Xt, self.basicdata.A_log[repHabMask], 
                self.basicdata.sigmaSqrt[i])
            pdf_i_s = stats.norm.logpdf(Xt, A_log_s, self.basicdata.sigmaSqrt[i])
            priorNow = stats.gamma.logpdf(self.basicdata.b[i], self.params.bPrior[0],
                loc = 0, scale = self.params.bPrior[1])
            priorNew = stats.gamma.logpdf(b_s[i], self.params.bPrior[0],
                loc = 0, scale = self.params.bPrior[1])
            pnow = np.sum(pdf_i) + priorNow
            pnew = np.sum(pdf_i_s) + priorNew
            pdiff = pnew - pnow
            if pdiff > 1.0:
                rValue = 1.0
                zValue = 0.0
            elif pdiff < -12.0:
                rValue = 0.0
                zValue = 1.0
            else:
                rValue = np.exp(pnew - pnow)        # calc importance ratio
                zValue = np.random.uniform(0.0, 1.0, size = None)
            if (rValue > zValue):
                self.basicdata.b[i] = b_s[i]
                self.basicdata.Surv[repHabMask] = survive_s
                self.basicdata.A[repHabMask] = A_s
                self.basicdata.A_log[repHabMask] = A_log_s
#                self.basicdata.b_Array[habMask] = b_s[i]
#        self.basicdata.bArray = self.basicdata.b[self.basicdata.datHabitat] 
                     

    def a_update(self):
        """
        update alpha parameters using gibbs
        """
        xTranspose = np.transpose(self.basicdata.tArray)
        xCrossProd = np.dot(xTranspose, self.basicdata.tArray)
        sinv = 1.0/self.basicdata.sigmaRho
        sx = np.multiply(xCrossProd, (1./self.basicdata.sigmaRho))
        xyCrossProd = np.dot(xTranspose, self.basicdata.logit_rho)
        sy = np.multiply(xyCrossProd, sinv)
        bigv = np.linalg.inv(sx + self.params.vinvert)
        smallv = sy + self.params.ppart
        meanVariate = np.dot(bigv, smallv)
        a = np.random.multivariate_normal(meanVariate, bigv)
        self.basicdata.alpha =  np.transpose(a)
        self.basicdata.mu = np.dot(self.basicdata.tArray, self.basicdata.alpha)



    def laggedRhoUpdate(self):
        """
        ## update the two initial rho values for both habitats
        """
        ### UPDATE LRHO_2 - TWO BACK
        logitRho_2_s = np.random.normal(self.basicdata.logitRho_2, self.params.rho_search)
        tarray_s = self.basicdata.tArray[self.basicdata.sess0Mask]
        tarray_s[:, -1] = logitRho_2_s
        mu_s = np.dot(tarray_s, self.basicdata.alpha)
        mu = self.basicdata.mu[self.basicdata.sess0Mask]
        r_pdf = np.sum(stats.norm.logpdf(self.basicdata.logit_rho[self.basicdata.sess0Mask],
                mu, self.basicdata.sigmaRho))
        r_pdf_s = np.sum(stats.norm.logpdf(self.basicdata.logit_rho[self.basicdata.sess0Mask],
                mu_s, self.basicdata.sigmaRho))
        priorNow = np.sum(stats.norm.logpdf(self.basicdata.logitRho_2, self.params.rho0Prior[0], 
            self.params.rho0Prior[1]))                  
        priorNew = np.sum(stats.norm.logpdf(logitRho_2_s, self.params.rho0Prior[0], 
            self.params.rho0Prior[1]))
        pnow = r_pdf + priorNow
        pnew = r_pdf_s + priorNew                  
        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0.0, 1.0, size = None)
#        print('pdf', r_pdf, 'pdf_s', r_pdf_s)
        if (rValue > zValue):
            self.basicdata.logitRho_2 = logitRho_2_s.copy()
            self.basicdata.tArray[self.basicdata.sess0Mask, -1] = logitRho_2_s
            self.basicdata.mu[self.basicdata.sess0Mask] = mu_s

        ### UPDATE LRHO_1 - ONE BACK
        logitRho_1_s = np.random.normal(self.basicdata.logitRho_1, self.params.rho_search)
        tarray_s = self.basicdata.tArray[self.basicdata.sess01Mask]
        tarray_s[0, -2] = logitRho_1_s[0]
        tarray_s[1, -1] = logitRho_1_s[0]
        tarray_s[2, -2] = logitRho_1_s[1]
        tarray_s[3, -1] = logitRho_1_s[1]
        mu_s = np.dot(tarray_s, self.basicdata.alpha)
        mu = self.basicdata.mu[self.basicdata.sess01Mask]
        r_pdf = np.sum(stats.norm.logpdf(self.basicdata.logit_rho[self.basicdata.sess01Mask],
                mu, self.basicdata.sigmaRho))
        r_pdf_s = np.sum(stats.norm.logpdf(self.basicdata.logit_rho[self.basicdata.sess01Mask],
                mu_s, self.basicdata.sigmaRho))
        priorNow = np.sum(stats.norm.logpdf(self.basicdata.logitRho_1, self.params.rho0Prior[0], 
            self.params.rho0Prior[1]))                  
        priorNew = np.sum(stats.norm.logpdf(logitRho_1_s, self.params.rho0Prior[0], 
            self.params.rho0Prior[1]))
        pnow = r_pdf + priorNow
        pnew = r_pdf_s + priorNew                  
        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0.0, 1.0, size = None)
#        print('pnow', pnow, 'pnew', pnew)

        if (rValue > zValue):
            self.basicdata.logitRho_1 = logitRho_1_s.copy()
            self.basicdata.tArray[self.basicdata.sess0Mask, -2] = logitRho_1_s
            self.basicdata.tArray[self.basicdata.sess1Mask, -1] = logitRho_1_s
            self.basicdata.mu[self.basicdata.sess01Mask] = mu_s

        


    def rho_updateFX(self):
        """
        ## update latent rho
        """
        logit_rho_s = np.random.normal(self.basicdata.logit_rho, self.params.rho_search)
        rho_s = inv_logit(logit_rho_s)
        nRec_s = (rho_s * self.basicdata.N / 2.0 * self.params.nFetusPerPreg)
        A_s = ((self.basicdata.N[self.basicdata.t_1ExtractMask] + 
            nRec_s[self.basicdata.t_2Mask]) * self.basicdata.Surv[self.basicdata.reproMask])
        self.logA_s[self.basicdata.reproMask] = np.log(A_s)
        PregRatePMF_s = (stats.binom.logpmf(self.basicdata.buildNPregnant, 
            self.basicdata.buildNFemales, rho_s))
        PregRatePMF = (stats.binom.logpmf(self.basicdata.buildNPregnant, 
            self.basicdata.buildNFemales, self.basicdata.rho))
        self.a_pdf[self.basicdata.reproMask] = (stats.norm.logpdf(
            self.basicdata.Xt[self.basicdata.reproMask], 
            self.basicdata.A_log[self.basicdata.reproMask], 
            self.basicdata.sigmaSQRTArray[self.basicdata.reproMask]))
        self.a_pdf_s[self.basicdata.reproMask] = (stats.norm.logpdf(
            self.basicdata.Xt[self.basicdata.reproMask], 
            self.logA_s[self.basicdata.reproMask], 
            self.basicdata.sigmaSQRTArray[self.basicdata.reproMask]))
        for i in range(self.basicdata.nDat):
            sess_i = self.basicdata.datSessions[i]
            mu_i = self.basicdata.mu[i]
            logit_rho_i = self.basicdata.logit_rho[i]
            logit_rho_s_i = logit_rho_s[i]
            if (sess_i < self.basicdata.nsessions - 2):         # less than second to last
                tArray_up1_s = self.basicdata.tArray[(i + 1)]
                tArray_up1_s[-2] = logit_rho_s_i
                tArray_up2_s = self.basicdata.tArray[(i + 2)]
                tArray_up2_s[-1] = logit_rho_s_i
                mu_up1_s = np.dot(tArray_up1_s, self.basicdata.alpha)
                mu_up2_s = np.dot(tArray_up2_s, self.basicdata.alpha)
                pdf_rho_up1 = stats.norm.logpdf(self.basicdata.logit_rho[i + 1], 
                    self.basicdata.mu[i + 1], self.basicdata.sigmaRhoSqrt)
                pdf_rho_up1_s = stats.norm.logpdf(self.basicdata.logit_rho[i + 1], 
                    mu_up1_s, self.basicdata.sigmaRhoSqrt)
                pdf_rho_up2 = stats.norm.logpdf(self.basicdata.logit_rho[i + 2], 
                    self.basicdata.mu[i + 2], self.basicdata.sigmaRhoSqrt)
                pdf_rho_up2_s = stats.norm.logpdf(self.basicdata.logit_rho[i + 2], 
                    mu_up2_s, self.basicdata.sigmaRhoSqrt)
            elif (sess_i < self.basicdata.nsessions - 1):         # less than last
                tArray_up1_s = self.basicdata.tArray[(i + 1)]
                tArray_up1_s[-2] = logit_rho_s_i
                mu_up1_s = np.dot(tArray_up1_s, self.basicdata.alpha)
                pdf_rho_up1 = stats.norm.logpdf(self.basicdata.logit_rho[i + 1], 
                    self.basicdata.mu[i + 1], self.basicdata.sigmaRhoSqrt)
                pdf_rho_up1_s = stats.norm.logpdf(self.basicdata.logit_rho[i + 1], 
                    mu_up1_s, self.basicdata.sigmaRhoSqrt)
            pdf_logitRho = stats.norm.logpdf(self.basicdata.logit_rho[i], mu_i, 
                self.basicdata.sigmaRhoSqrt)
            pdf_logitRho_s = stats.norm.logpdf(logit_rho_s_i, mu_i, 
                self.basicdata.sigmaRhoSqrt)
            pnow = pdf_logitRho + PregRatePMF[i]
            pnew =  pdf_logitRho_s + PregRatePMF_s[i]
            if (sess_i < self.basicdata.nsessions - 2):
                pnow += (pdf_rho_up1 + pdf_rho_up2 + self.a_pdf[i + 2])
                pnew += (pdf_rho_up1_s + pdf_rho_up2_s + self.a_pdf_s[i + 2])
            elif (sess_i < self.basicdata.nsessions - 1):
                pnow += pdf_rho_up1
                pnew += pdf_rho_up1_s
            rValue = np.exp(pnew - pnow)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
            if (rValue > zValue):
                self.basicdata.rho[i] = rho_s[i]
                self.basicdata.logit_rho[i] = logit_rho_s[i]
                PregRatePMF[i] = PregRatePMF_s[i]
                ## if less than second to last
                if sess_i < (self.basicdata.nsessions - 2):       
                    self.basicdata.nRecruits[i + 2] = nRec_s[i]
                    self.basicdata.A_log[i + 2] = self.logA_s[i + 2]
                    self.basicdata.mu[i + 2] = mu_up2_s
                    self.basicdata.mu[i + 1] = mu_up1_s
                    self.basicdata.tArray[(i + 2), -1] = logit_rho_s_i
                    self.basicdata.tArray[(i + 1), -2] = logit_rho_s_i
                if (sess_i == (self.basicdata.nsessions - 2)):
                    self.basicdata.mu[i + 1] = mu_up1_s
                    self.basicdata.tArray[(i + 1), -2] = logit_rho_s_i
        self.sumPregRatePMF = np.sum(PregRatePMF)

    def N_UpdateFX(self):
        """
        update N estimates
        """
        (Xt_s, Pnow_new, Pnew_now) = self.proposeN()
        N_s = np.exp(Xt_s)
        nRec_s_tmp =self.basicdata.rho * N_s / 2.0 * self.params.nFetusPerPreg
        for i in range(self.basicdata.nDat):
            ns = N_s[i]
            xt_s = Xt_s[i]
            Xt = self.basicdata.Xt[i]
            # proposed prob of trap
            expTerm_s = ns * self.basicdata.lambdaPred[i]
            theta_s = 1.0 - np.exp(-expTerm_s)
            detectPMF_s = stats.binom.logpmf(self.basicdata.buildNTrapped[i], 
                    self.basicdata.buildNSetTraps[i], theta_s)
            detectPMF = self.basicdata.detectPMF[i]            
            habIndx_i = self.basicdata.datHabitat[i]
            bPara = self.basicdata.b[habIndx_i]
            sigPara = self.basicdata.sigmaSqrt[habIndx_i]
            ## if in ReproMask, i.e. not first 2 months => get present pdf
            if self.basicdata.datMonthRun[i] >= (self.basicdata.datMinMonthRun + 2):
                A_log_i = self.basicdata.A_log[i]
                pdf_i = stats.norm.logpdf(Xt, A_log_i, sigPara)
                pdf_i_s = stats.norm.logpdf(xt_s, A_log_i, sigPara)
            ## if middle months, get up 1 pdf => get up 1 pdf
            if ((self.basicdata.datMonthRun[i] > self.basicdata.datMinMonthRun) &
                (self.basicdata.datMonthRun[i] < self.basicdata.datMaxMonthRun)):
                nRecruits_up1 =  self.basicdata.nRecruits[i + 1]
                Surv_up1_s = np.exp(-ns * bPara)
                A_log_up1_s = np.log((ns + nRecruits_up1) * Surv_up1_s)
                up1_pdf_s = stats.norm.logpdf(self.basicdata.Xt[i + 1], A_log_up1_s, sigPara)
                up1_pdf = stats.norm.logpdf(self.basicdata.Xt[i + 1], self.basicdata.A_log[i + 1], sigPara)
            ## if months is less than max - 1; up to second to last => get up 2 pdf
            if self.basicdata.datMonthRun[i] < (self.basicdata.datMaxMonthRun - 1):
                ## should be ns
                nRec_up2_s = nRec_s_tmp[i]
####                nRec_up2_s = nRec_s_tmp[i+1]    # self.basicdata.rho[i] * ns / 2.0 * self.params.nFetusPerPreg


                A_log_up2_s = np.log((self.basicdata.N[i + 1] + nRec_up2_s) * self.basicdata.Surv[i + 2])
                up2_pdf_s = stats.norm.logpdf(self.basicdata.Xt[i + 2], A_log_up2_s, sigPara)
                up2_pdf = stats.norm.logpdf(self.basicdata.Xt[i + 2], self.basicdata.A_log[i + 2], sigPara)
            # Calc pnow and pnew
            zValue = np.random.uniform(0.0, 1.0)
            ## If first month; i = 0
            if self.basicdata.datSessions[i] == 0:
                pnow = detectPMF + up2_pdf +  Pnew_now[i]
                pnew = detectPMF_s + up2_pdf_s + Pnow_new[i]
                rValue = np.exp(pnew - pnow)        # calc importance ratio
                if rValue > zValue:
                    self.basicdata.N[i] = ns
                    self.basicdata.A_log[i + 2] = A_log_up2_s
                    self.basicdata.Xt[i] = xt_s
                    self.basicdata.nRecruits[i + 2] = nRec_up2_s
                    self.basicdata.detectPMF[i] = detectPMF_s
                    self.basicdata.theta[i] = theta_s
            ## if second month; i == 1
            elif self.basicdata.datSessions[i] == 1:
                pnow = detectPMF + up1_pdf + up2_pdf +  Pnew_now[i]
                pnew = detectPMF_s + up1_pdf_s + up2_pdf_s + Pnow_new[i]
                rValue = np.exp(pnew - pnow)        # calc importance ratio
                if rValue > zValue:
                    self.basicdata.N[i] = ns
                    self.basicdata.A_log[i + 2] = A_log_up2_s
                    self.basicdata.A_log[i + 1] = A_log_up1_s
                    self.basicdata.Xt[i] = xt_s
                    self.basicdata.nRecruits[i + 2] = nRec_up2_s
                    self.basicdata.Surv[i + 1] = Surv_up1_s
                    self.basicdata.detectPMF[i] = detectPMF_s
                    self.basicdata.theta[i] = theta_s  
            ### Session 2 to third to last
            elif ((self.basicdata.datSessions[i] > 1) & 
                (self.basicdata.datSessions[i] < (self.basicdata.nsessions - 2))):    
                pnow = detectPMF + pdf_i + up1_pdf + up2_pdf +  Pnew_now[i]
                pnew = detectPMF_s + pdf_i_s + up1_pdf_s + up2_pdf_s + Pnow_new[i]
                rValue = np.exp(pnew - pnow)        # calc importance ratio
                if rValue > zValue:
                    self.basicdata.N[i] = ns
                    self.basicdata.A_log[i + 2] = A_log_up2_s
                    self.basicdata.A_log[i + 1] = A_log_up1_s
                    self.basicdata.Xt[i] = xt_s
                    self.basicdata.nRecruits[i + 2] = nRec_up2_s
                    self.basicdata.Surv[i + 1] = Surv_up1_s
                    self.basicdata.detectPMF[i] = detectPMF_s
                    self.basicdata.theta[i] = theta_s 
            ### Session second to last
            elif (self.basicdata.datSessions[i] == (self.basicdata.nsessions - 2)):    
                pnow = detectPMF + pdf_i + up1_pdf +  Pnew_now[i]
                pnew = detectPMF_s + pdf_i_s + up1_pdf_s + Pnow_new[i]
                rValue = np.exp(pnew - pnow)        # calc importance ratio
                if rValue > zValue:
                    self.basicdata.N[i] = ns
                    self.basicdata.A_log[i + 1] = A_log_up1_s
                    self.basicdata.Xt[i] = xt_s
                    self.basicdata.Surv[i + 1] = Surv_up1_s
                    self.basicdata.detectPMF[i] = detectPMF_s
                    self.basicdata.theta[i] = theta_s 
            ### If last Session
            elif (self.basicdata.datSessions[i] == (self.basicdata.nsessions - 1)):    
                pnow = detectPMF + pdf_i + Pnew_now[i]
                pnew = detectPMF_s + pdf_i_s + Pnow_new[i]
                rValue = np.exp(pnew - pnow)        # calc importance ratio
                if rValue > zValue:
                    self.basicdata.N[i] = ns
                    self.basicdata.Xt[i] = xt_s
                    self.basicdata.detectPMF[i] = detectPMF_s
                    self.basicdata.theta[i] = theta_s 


    def proposeN(self):
        """
        propose new Nx for updating
        """
        loDiff = self.basicdata.minXt - self.basicdata.Xt
        hiDiff = self.basicdata.maxXt - self.basicdata.Xt
        truncNorm_Now = stats.truncnorm(loDiff / self.params.XtJump, hiDiff / self.params.XtJump, 
            loc = self.basicdata.Xt, scale = self.params.XtJump)
        Xt_s = truncNorm_Now.rvs()
        Pnew_now = truncNorm_Now.logpdf(Xt_s) # log of truncated N(xt_s|xt, ySearch)
        loDiff2 = self.basicdata.minXt - Xt_s
        hiDiff2 = self.basicdata.maxXt - Xt_s   
#        (a, b) = (loDiff2 / self.params.XtJump, hiDiff2 / self.params.XtJump)
#        truncNorm_New = stats.truncnorm(a, b, loc = Xt_s, scale = self.params.XtJump)
        truncNorm_New = stats.truncnorm(loDiff2 / self.params.XtJump, hiDiff2 / self.params.XtJump, 
            loc = Xt_s, scale = self.params.XtJump)
        Pnow_new = truncNorm_New.pdf(self.basicdata.Xt) # log of truncated N(xt|xt_s, ySearch)
#        Pnew_now = truncNorm_Now.logpdf(Xt_s) # log of truncated N(yt_s|yt, ySearch)
        return(Xt_s, Pnow_new, Pnew_now)


    def sig_UpdateFX(self):
        """
        update sigma variance parameters for N model and Rho model.
        """
        ### Update sigma for N Model
        ## loop thru habitat
        for i in range(2):
            if i == 0:
                xtHab = self.basicdata.Xt[self.basicdata.reproHab0Mask]
                log_AHab = self.basicdata.A_log[self.basicdata.reproHab0Mask]
            else:
                xtHab = self.basicdata.Xt[self.basicdata.reproHab1Mask]
                log_AHab = self.basicdata.A_log[self.basicdata.reproHab1Mask]            

            self.basicdata.sigma[i] = vupdate(xtHab, log_AHab, 
                self.params.sigmaPriors[0], self.params.sigmaPriors[1], self.basicdata.nSigUpdate)
        self.basicdata.sigmaSqrt = np.sqrt(self.basicdata.sigma)
        self.basicdata.sigmaSQRTArray = self.basicdata.sigmaSqrt[self.basicdata.datHabitat]
        ####################################
        ## UPDATE sigmaRho parameter
        self.basicdata.sigmaRho = vupdate(self.basicdata.logit_rho, self.basicdata.mu,
            self.params.s1, self.params.s2, self.basicdata.nDat)


    def g_UpdateFX(self):
        """
        update detection parameters
        """
        for i in range(self.params.n_gPara):
            g_s = self.basicdata.g.copy()
            g_s[i] = np.random.normal(self.basicdata.g[i], self.params.g_search[i], size = None)
            lambdaPred_s = np.exp(np.dot(self.basicdata.detectCovariates, g_s))
            expTerm = self.basicdata.N * lambdaPred_s
            theta_s = 1.0 - np.exp(-expTerm)
            detectPMF_s = stats.binom.logpmf(self.basicdata.buildNTrapped, 
                            self.basicdata.buildNSetTraps, theta_s)
            prior = stats.norm.logpdf(self.basicdata.g[i],
                    self.params.gPrior[0], self.params.gPrior[1])
            prior_s = stats.norm.logpdf(g_s[i],
                    self.params.gPrior[0], self.params.gPrior[1])
            pnow = np.sum(self.basicdata.detectPMF) + prior
            pnew = np.sum(detectPMF_s) + prior_s
            pdiff = pnew - pnow
            if pdiff > 1.0:
                rValue = 1.0
                zValue = 0.0
            elif pdiff < -12.0:
                rValue = 0.0
                zValue = 1.0
            else:
                rValue = np.exp(pnew - pnow)        # calc importance ratio
                zValue = np.random.uniform(0.0, 1.0, size = None)
            if (rValue > zValue):
                self.basicdata.g[i] = g_s[i] 
                self.basicdata.detectPMF = detectPMF_s.copy()
                self.basicdata.theta = theta_s.copy()
                self.basicdata.lambdaPred = lambdaPred_s.copy()

        
    def vupdate_NOT_USED(self, i):
        """
        Gibbs updater of variance parameter
        """
        if i == 0:
            xtHab = self.basicdata.Xt[self.basicdata.reproHab0Mask]
            log_AHab = self.basicdata.A_log[self.basicdata.reproHab0Mask]
        else:
            xtHab = self.basicdata.Xt[self.basicdata.reproHab1Mask]
            log_AHab = self.basicdata.A_log[self.basicdata.reproHab1Mask]            
        predDiff = xtHab - log_AHab
        sx = np.sum(predDiff**2.0)
        u1 = self.params.sigmaPriors[0] + np.multiply(0.5, self.basicdata.nReproHabSess)
        u2 = self.params.sigmaPriors[1] + np.multiply(0.5, sx)      # gamma rate parameter
        isg = np.random.gamma(u1, 1.0 / u2, size = None)            # formulation using shape and scale
        self.basicdata.sigma[i] = 1.0 / isg



    def alpha_UpdateFX_NOT_USED(self):
        """
        ## NOT USED !!!
        update pregnancy or reproductive rate parameters
        """
        for i in range(self.nAlpha):
            alpha_s = self.basicdata.alpha.copy()
            alpha_s[i] = np.random.normal(self.basicdata.alpha[i], self.params.alpha_search[i])
            logitRho_s = np.dot(self.basicdata.tArray, alpha_s)
            rho_s = inv_logit(logitRho_s)
            nRec_s = (rho_s * self.basicdata.N / 2.0 * self.params.nFetusPerPreg)
            A_s = ((self.basicdata.N[self.basicdata.t_1ExtractMask] + 
                nRec_s[self.basicdata.t_2Mask]) * self.basicdata.Surv[self.basicdata.reproMask])
            logA_s = np.log(A_s)
            sumPregRatePMF_s = np.sum(stats.binom.logpmf(self.basicdata.buildNPregnant, 
                self.basicdata.buildNFemales, rho_s))
            self.a_pdf[self.basicdata.reproMask] = np.sum(stats.norm.logpdf(
                self.basicdata.Xt[self.basicdata.reproMask], 
                self.basicdata.A_log[self.basicdata.reproMask], 
                self.basicdata.sigmaSQRTArray[self.basicdata.reproMask]))
            self.a_pdf_s[self.basicdata.reproMask] = np.sum(stats.norm.logpdf(
                self.basicdata.Xt[self.basicdata.reproMask], 
                logA_s, self.basicdata.sigmaSQRTArray[self.basicdata.reproMask]))
            alphaPriors = stats.norm.logpdf(self.basicdata.alpha[i], self.params.alpha_Prior[0],
                    self.params.alpha_Prior[1])
            alphaPriors_s = stats.norm.logpdf(alpha_s[i], self.params.alpha_Prior[0], 
                    self.params.alpha_Prior[1])
            pnow = self.basicdata.sumPregRatePMF + a_pdf + alphaPriors
            pnew = sumPregRatePMF_s + a_pdf_s + alphaPriors_s
            pdiff = pnew - pnow
            if pdiff > 1.0:
                rValue = 1.0
                zValue = 0.0
            elif pdiff < -12.0:
                rValue = 0.0
                zValue = 1.0
            else:
                rValue = np.exp(pnew - pnow)        # calc importance ratio
                zValue = np.random.uniform(0.0, 1.0, size = None)
            if (rValue > zValue):
                self.basicdata.alpha[i] = alpha_s[i]
                self.basicdata.rho = rho_s.copy()
                self.basicdata.sumPregRatePMF = sumPregRatePMF_s
                self.basicdata.nRecruits[self.basicdata.reproMask] = nRec_s[self.basicdata.t_2Mask]
                self.basicdata.A_log[self.basicdata.reproMask] = logA_s




########            Main mcmc function
########
    def mcmcFX(self):
        """
        update parameters
        """
        cc = 0
        for g in range(self.params.ngibbs * self.params.thinrate + self.params.burnin):
#            print('g', g)

            self.b_UpdateFX()

            self.g_UpdateFX()

            self.a_update()
#            self.alpha_UpdateFX()

            self.sig_UpdateFX()

            self.N_UpdateFX()

            if self.params.modelNumber == 3:
                self.laggedRhoUpdate()

                self.rho_updateFX()

            if g in self.params.keepseq:
                self.bgibbs[cc] = self.basicdata.b
                self.ggibbs[cc] = self.basicdata.g
                self.alphagibbs[cc] = self.basicdata.alpha
                self.siggibbs[cc] = self.basicdata.sigma
                self.Ngibbs[cc] = self.basicdata.N
                self.NHab0gibbs[cc] = self.basicdata.N[self.basicdata.habMask0] 
#                self.NHab1gibbs[cc] = self.basicdata.N[:, 1] 
                if self.params.modelNumber == 3:
                    self.lrho_1gibbs[cc] = self.basicdata.logitRho_1
                    self.lrho_2gibbs[cc] = self.basicdata.logitRho_2
                    self.logit_rhogibbs[cc] = self.basicdata.logit_rho
                    self.sigmarhogibbs[cc] = self.basicdata.sigmaRho


                cc = cc + 1
        
#        P.figure()
#        P.plot(self.basicdata.N[:120], color = 'g', linewidth = 3.0)
#        P.plot(np.exp(self.basicdata.mu[:120]), color = 'k', linewidth = 3.0)
#        P.plot(np.exp(self.basicdata.Xt[:120]), color = 'b', linewidth = 3.0)
#        P.show()

#        print('logit rho gibbs', self.logit_rhogibbs[:,:4])

#        print('Ngibbs', np.round(self.NHab0gibbs[:, self.params.followSess], 0))
#        print('siggibbs', self.siggibbs)
#        print('bgibbs', self.bgibbs)
#        print('alphagibbs', np.round(self.alphagibbs, 2))
#        print('ggibbs', self.ggibbs)
#        print('sigmaRho', self.sigmarhogibbs)

#        print('logitRho_1', self.basicdata.logitRho_1)
#        print('logitRho_2', self.basicdata.logitRho_2)
#        print('tArray sess01', self.basicdata.tArray[self.basicdata.sess01Mask, 2:])



########            Pickle results to directory
########
class Gibbs(object):
    def __init__(self, mcmcobj, basicdata, params):
        self.Ngibbs = mcmcobj.Ngibbs
#        self.NHab0gibbs = mcmcobj.NHab0gibbs
#        self.NHab1gibbs = mcmcobj.NHab1gibbs
        self.bgibbs = mcmcobj.bgibbs
        self.ggibbs = mcmcobj.ggibbs
        self.alphagibbs = mcmcobj.alphagibbs
        self.siggibbs = mcmcobj.siggibbs
        self.modelNumber = params.modelNumber
        if self.modelNumber == 3:
            self.lrho_1gibbs = mcmcobj.lrho_1gibbs
            self.lrho_2gibbs = mcmcobj.lrho_2gibbs
            self.logit_rhogibbs = mcmcobj.logit_rhogibbs
            self.sigmarhogibbs = mcmcobj.sigmarhogibbs

        # move variables
        self.nsessions = basicdata.nsessions      # n sessions per habitat
        self.datSessions = basicdata.datSessions
        self.datNTrapped = basicdata.datNTrapped
        self.datNSetTraps = basicdata.datNSetTraps
        self.datMonthRun = basicdata.datMonthRun  # from 13 up
        self.datMonth = basicdata.datMonth
        self.datYear = basicdata.datYear
        self.datHabitat = basicdata.datHabitat    # 0 = lowland and 1 = upland
        self.habMask0 = self.datHabitat == 0
        self.habMask1 = self.datHabitat == 1
        self.datStartMonth = basicdata.datStartMonth
        self.datMinMonthRun = basicdata.datMinMonthRun
        self.datMaxMonthRun = basicdata.datMaxMonthRun
        ## repro variables
        self.datNFemales = basicdata.datNFemales
        self.datNPregnant = basicdata.datNPregnant
        self.datPromTesticle = basicdata.datPromTesticle
        self.nDat = basicdata.nDat                # total data
        self.datRain = basicdata.datRain
        self.datTemperature = basicdata.datTemperature
        self.datRice = basicdata.datRice
        self.datRapeWheat = basicdata.datRapeWheat
        self.datVegetables = basicdata.datVegetables
        self.datTrees = basicdata.datTrees
        self.datDryCrop = basicdata.datDryCrop
        self.uDatYear = basicdata.uDatYear
        self.nDatYear = basicdata.nDatYear
        # Cross validation data
        self.monthCV = basicdata.monthCV
        self.yearCV = basicdata.yearCV
        self.cvDates = basicdata.cvDates
        self.nCV = basicdata.nCV
        self.cvPointMask = basicdata.cvPointMask
        # Covariates
        self.tArray = basicdata.tArray
        self.detectCovariates = basicdata.detectCovariates
        # Params
        self.thinrate = params.thinrate
        self.burnin = params.burnin
        self.followSess = params.followSess
        self.nFetusPerPreg = params.nFetusPerPreg
