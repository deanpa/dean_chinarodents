#!/usr/bin/env python

#######################################################
#######################################################
#######################################################
# This Python script contains functions for forecasting striped field mouse 
# (Apodemus agrarius) abundance in agricultural fields in Yuqing County, 
# Guizhou, China

# Copyright (C) 2017 Dean Anderson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#######################################################
#######################################################
#######################################################


import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
from scipy import stats
#import prettytable
import pickle
import os
import datetime
import forecastParameters


    
class Params(object):
    def __init__(self):
        """
        set prediction names for input and output pickle files
        """
        ########################
        ## number of iterations
        self.iter = 1000
        self.nYears = 30

        ## Environmental model (covariates):
            ## options are Rain, Temp, Rain0Rice', tmp0Rice, vegTree
        self.covMod = 'tmp0Rice'       # Rain, Temp, 'RainTemp, Rain0Rice', tmp0Rice, vegTree

        ## Lagged months for weather data
            # options are [10,8], [9,7], [8,6], [7,5], [6,4], [5,3], [4,2], [3,1]
        self.laggedMonths = np.array([6,4])

        ## mean annual temperature increase/decrease
        self.temperatureRate = 1.20      # after 30 years = 3.0 c
        ## Temperature standard deviation factor increase
        self.sdIncrease = 1.555
        
        ## veg increase for lowland and upland
        self.vegIncrease = np.array([1.0, 0.80])
        
        ## File names of forecasting data to read in
        ## rain and temperature data
        self.weather30YFname = 'weather30Y.csv'  
        ## crop data
        self.crop30YFname = 'crop30Y.csv'
        
        ## set paths to scripts and data
        self.inputDataPath = os.path.join(os.getenv('CHINAPROJDIR', default='.'))
        ## write results here
        self.outputDataPath = os.path.join(os.getenv('CHINAPROJDIR', default='.'))
        if not os.path.isdir(self.outputDataPath):
            os.mkdir(self.outputDataPath)


class Simulate(object):
    def __init__(self, params):
#    def __init__(self, gibbsobj, rodentpath, params):
        self.params = params
        #####################################
        #####################################
        # Run functions
        self.getGibbsResults()
        self.getData()
        self.makeStorageArrays()
        self.increaseTemp()
        self.getLaggedRainTempData()
        self.makeTArray()
        self.loopFX()
        self.makeSummaryTable()
        self.plotSim()
        self.plot30YearMeans()
        self.plotSimShare()
        self.plotSimVeg()
        self.plotVegTempShare()
        #####   END FUNCTIONS   ##############
        
        
    #########
    #############   FUNCTIONS
    def getGibbsResults(self):
        """
        ## read in gibbs pickle from MCMC 
        """
        ## file name from MCMC to read in
        gibbsName = ('gibbs' + self.params.covMod + str(self.params.laggedMonths[0]) + '_' +
            str(self.params.laggedMonths[1]) + '.pkl')
        ## load MCMC results from pickle
        inputGibbs = os.path.join(self.params.inputDataPath, gibbsName)
        fileobj = open(inputGibbs, 'rb')
        self.gibbsobj = pickle.load(fileobj)
        fileobj.close()
        ## transfer gibbs elements to simulateObj
        self.bgibbs = self.gibbsobj.bgibbs
        self.alphagibbs = self.gibbsobj.alphagibbs
        self.nAlpha = np.shape(self.alphagibbs)[1]
        self.siggibbs = self.gibbsobj.siggibbs
#        self.sigForecast = self.siggibbs * self.params.sdIncrease
        self.sessions = self.gibbsobj.sessions
        self.nsessions = self.gibbsobj.nsessions        
        self.maxSession = self.gibbsobj.maxSession
        self.minSession = self.gibbsobj.minSession
        self.lastSessMask = self.sessions == self.maxSession
        self.Ngibbs = self.gibbsobj.Ngibbs 
        self.nStart = self.Ngibbs[:, self.lastSessMask]
#        self.nTrapped = self.gibbsobj.nTrapped
        self.nMcmc = np.shape(self.Ngibbs)[0]
        ### Temp data from apodemus.py
        self.temperatureDatAll = self.gibbsobj.temperatureDatAll
        self.yearsAll = self.gibbsobj.yearsAll
        self.monthsAll = self.gibbsobj.monthsAll
        self.habIndxAll = self.gibbsobj.habIndxAll
        
        
    def getData(self):
        """
        ## read in weather and crop data
        """
        ## get weather data
        FPathName = os.path.join(self.params.inputDataPath, self.params.weather30YFname)
        weather = np.genfromtxt(FPathName, names=True, delimiter = ',',
            dtype=['i8', 'f8', 'f8'])
        self.meanMonthRain = weather['rain']
        self.meanMonthTemp = weather['temperature']
        self.weatherMonth = weather['month']
        ## get crop data
        FPathName = os.path.join(self.params.inputDataPath, self.params.crop30YFname)
        crops = np.genfromtxt(FPathName, names=True, delimiter = ',',
            dtype=['i8', 'i8', 'f8', 'f8', 'i8'])
        self.cropMonth = crops['month']
        self.cropHabitat = crops['habitat']
        self.cropRice = crops['rice']
        self.cropVeg = crops['vegetables']
        self.cropTrees = crops['trees']
#        self.cropVegIncrease = self.cropVeg.copy()
#        self.cropVegIncrease[self.cropHabitat == 0] = 1.0
#        self.cropVegIncrease[self.cropHabitat == 1] = 0.9

        
    def makeStorageArrays(self):
        """
        ## make arrays to store results
        """
        self.nMonths1Habitat = self.params.nYears * 12
        self.months1Habitat = np.tile(np.arange(12), self.params.nYears)
        self.nMonths2Habitats = self.nMonths1Habitat * 2
        self.months2Habitats = np.tile(np.arange(12), self.params.nYears * 2)
        self.habMask = np.ones(self.nMonths2Habitats, dtype = int)
        self.habMask[:self.nMonths1Habitat] = 0
        self.habMask0 = self.habMask == 0
        self.habMask1 = self.habMask == 1
        self.years1Habitat = np.repeat(np.arange(self.params.nYears), 12)
        ## make vegetable data
        self.veg2Habitats = np.zeros(self.nMonths2Habitats)
        self.veg2Habitats[self.habMask == 0] = np.tile(
            self.cropVeg[self.cropHabitat == 0], self.params.nYears)
        self.veg2Habitats[self.habMask == 1] = np.tile(
            self.cropVeg[self.cropHabitat == 1], self.params.nYears)
#        ## increase veg data
#        self.vegIncrease2Habitats = np.zeros(self.nMonths2Habitats)
#        self.vegIncrease2Habitats[self.habMask == 0] = np.tile(
#            self.cropVegIncrease[self.cropHabitat == 0], self.params.nYears)
#        self.vegIncrease2Habitats[self.habMask == 1] = np.tile(
#            self.cropVegIncrease[self.cropHabitat == 1], self.params.nYears)
        ## make tree data
        self.trees2Habitats = np.zeros(self.nMonths2Habitats)
        self.trees2Habitats[self.habMask == 0] = np.tile(
            self.cropTrees[self.cropHabitat == 0], self.params.nYears)
        self.trees2Habitats[self.habMask == 1] = np.tile(
            self.cropTrees[self.cropHabitat == 1], self.params.nYears)
        
    def increaseTemp(self):
        """
        ## function to increase mean annual temp by specified amount
        """
        self.tempWt_stdev()
        self.monthlyTempIncrease = self.relativeWt * self.params.temperatureRate
        self.increasedTempArray = np.tile(self.meanMonthTemp, 
            (self.params.nYears + 1))
        ## year array used to get lagged data
        self.tmpYearsArray = np.repeat(np.arange(self.params.nYears +1), 12)
        ## loop thru years to get adjusted temperature data
        for i in range(self.params.nYears + 1):
            yearMask = self.tmpYearsArray == i
            self.increasedTempArray[yearMask] = (self.meanMonthTemp +
                (self.monthlyTempIncrease * i))
            self.increasedTempArray[yearMask] = (self.meanTempLate +
                (self.monthlyTempIncrease * i))
            print('year', 2015 + i, np.mean(self.increasedTempArray[yearMask]))
        ## get extended array of rainfall
        self.increasedRainArray = np.tile(self.meanMonthRain, 
            (self.params.nYears + 1))
        print('meanTemp', np.round(self.meanTempLate,3), 'relwt', np.round(
            self.relativeWt, 3), 'sum month temp increase',
            np.sum(self.monthlyTempIncrease),
            'sdTempLate', np.round(self.sdTempLate, 3), 'annual monthly increase', 
            np.round(self.monthlyTempIncrease,3), 'meanTempChange', 
            np.mean(self.meanTempLate) - np.mean(self.meanTempEarly),
            'mean diff first and last year', (np.mean(self.increasedTempArray[-12:]) -
            np.mean(self.meanTempLate)), 'len last', len(self.increasedTempArray[-12:]))
            
    def tempWt_stdev(self):
        """
        ## use existing data to get weighting factors for distributing 
        ## increased temperature across months
        """
        maskHab0 = self.habIndxAll == 0
        mask1 = (self.yearsAll < 1997) & maskHab0
        mask2 = (self.yearsAll > 2005) & maskHab0
        self.meanTempEarly = np.zeros(12)
        self.sdTempEarly = np.zeros(12)
        self.meanTempLate = np.zeros(12)
        self.sdTempLate = np.zeros(12)
        ## loop thru months to populate arrays
        for i in range(12):
            ## get means and std of first 10 years
            mask1_i = mask1 & (self.monthsAll == (i + 1))
            self.meanTempEarly[i] = np.mean(self.temperatureDatAll[mask1_i])
            self.sdTempEarly[i] = np.std(self.temperatureDatAll[mask1_i])
            ## get mean and std of last 10 years
            mask2_i = mask2 & (self.monthsAll == (i + 1))
            self.meanTempLate[i] = np.mean(self.temperatureDatAll[mask2_i])
            self.sdTempLate[i] = np.std(self.temperatureDatAll[mask2_i])
        tempDiff = self.meanTempLate - self.meanTempEarly
        tempDiff = tempDiff + np.abs(np.min(tempDiff))
        self.relativeWt = tempDiff / np.sum(tempDiff)
        print('sd early', self.sdTempEarly, 'sd late', self.sdTempLate, 
            'sd Diff', (self.sdTempLate - self.sdTempEarly),
            'sd Proportion', (self.sdTempLate / self.sdTempEarly),
            'mean sd Proportion', np.mean(self.sdTempLate / self.sdTempEarly),
            'relWt', self.relativeWt) 


     
    def getLaggedRainTempData(self):
        """
        get lagged rain and temp data
        """
        # extended month run arrays for getting lag data
        self.nMonthsRun = self.nMonths1Habitat + 12
        self.monthRun = np.arange(self.nMonthsRun)
        self.extendedCalendarMonth = np.tile(np.arange(12), (self.params.nYears + 1))
        ## empty rain and temperature arrays to populate with lagged data
        self.lagRainDat = np.zeros(self.nMonths1Habitat)
        self.lagTempDat = np.zeros(self.nMonths1Habitat)
        self.maxLag = self.params.laggedMonths[0] 
        self.minLag = self.params.laggedMonths[1]
        monthArray = np.zeros(3, dtype = int)
        ## present Temperature arrays and preparation
        self.presentLagTemp = np.zeros(self.nMonths1Habitat)
        self.presentTempArray = np.tile(self.meanMonthTemp, 
            (self.params.nYears + 1))
        ## loop years
        for i in range(12, self.nMonthsRun):
            for j in range(3):
                monthArray[j] = self.monthRun[i] - (self.maxLag - j)
            ## populate Increasing temperature arrays
            self.lagTempDat[(i - 12)] = np.mean(self.increasedTempArray[monthArray])
            ## populate present Temp arrays
            self.presentLagTemp[(i - 12)] = np.mean(
                self.presentTempArray[monthArray])
     
    def makeTArray(self):
        """
        ## make array of covariates to predict a (pop growth)
        ## rows = 2 habitats
        """
        ## empty 2-d array for Forecast covariates
        self.tArrayForecast = np.ones((self.nMonths2Habitats, self.nAlpha))
        self.tArrayForecast[:,1] = np.sin(2.0 * np.pi * (self.months2Habitats+1) / 12.0)
        self.tArrayForecast[:,2] = np.cos(2.0 * np.pi * (self.months2Habitats+1) / 12.0)
        self.tArrayForecast[:,3] = np.sin(4.0 * np.pi * (self.months2Habitats+1) / 12.0)
        self.tArrayForecast[:,4] = np.cos(4.0 * np.pi * (self.months2Habitats+1) / 12.0)
        ## populate the lagged temperature data
        self.tArrayForecast[:,5] = np.log(np.append(self.lagTempDat, 
            self.lagTempDat) + 1.0)
#        self.tArrayForecast[:,6] = self.riceAll[predIndx]
        self.tArrayForecast[:,6] = self.veg2Habitats
        self.tArrayForecast[:,7] = self.trees2Habitats
        ################# Array for Present weather scenario
        self.tArrayPresent = self.tArrayForecast.copy()
        ## populate the lagged PRESENT temperature data
        self.tArrayPresent[:,5] = np.log(np.append(self.presentLagTemp, 
            self.presentLagTemp) + 1.0)
        
    def loopFX(self):
        """
        ## draw random variates of parameters from mcmc
        """
        self.minMonthKeep = self.nMonths1Habitat - 12
        ## Results storage arrays
        self.presentResults = np.zeros((self.params.iter, 24))
        self.forecastResults = np.zeros((self.params.iter, 24))
        self.vegResults = np.zeros((self.params.iter, 24))
        ## array to store estimates for all years to get mean N
        self.meanForecast30Y = np.zeros((2, self.nMonths2Habitats))
        ## array to store increase veg scenario
        self.increaseVeg30Y = np.zeros(self.nMonths2Habitats)
        ## array to store increase veg  and Temperature scenario
        self.vegTempResults = np.zeros((self.params.iter, 24))
        self.randSample = np.random.choice(np.arange(self.nMcmc), self.params.iter,
            replace = True)
        ## loop Habitat
        for i in range(2):
            habMask_i = self.habMask == i
            tArrayPresent_i = self.tArrayPresent[habMask_i]
            tArrayForecast_i = self.tArrayForecast[habMask_i]
            ## loop iter
            for j in range(self.params.iter):
                rand_j = self.randSample[j]
                Nt_Present = self.nStart[rand_j, i]
                Nt_Forecast = Nt_Present
                xt_Present = np.log(Nt_Present)
                xt_Forecast = xt_Present
                Nt_vegIncrease = Nt_Present
                xt_vegIncrease = np.log(Nt_vegIncrease)
                Nt_vegTempIncrease = Nt_Present
                xt_vegTempIncrease = np.log(Nt_vegTempIncrease)
                alpha_j = self.alphagibbs[rand_j]
                bPara_ji = self.bgibbs[rand_j, i]
                sigma_ji = self.siggibbs[rand_j, i]
                cc = 0
                kk = 0
                for k in range(self.nMonths1Habitat):
                    ## get temperature variates
                    mon_k = self.months1Habitat[k]
                    sdPresent_k = self.sdTempLate[mon_k] 
                    sdForecast_k = sdPresent_k * self.params.sdIncrease
                    randTempPres = np.random.normal(self.presentLagTemp[k], 
                        sdPresent_k)
                    if randTempPres <= -1.0:
                        randTempPres = -0.55
                    tArrayPresent_i[k, 5] = np.log(randTempPres + 1.0)
                    ## get tArray for veg Increase
                    tArrayVeg = tArrayPresent_i[k]
                    tArrayVeg[6] = self.params.vegIncrease[i]
                    randTempForecast = np.random.normal(self.lagTempDat[k], 
                        sdForecast_k)
                    if randTempForecast <= -1.0:
                        randTempForecast = -0.35
                    tArrayForecast_i[k, 5] = np.log(randTempForecast + 1.0)    
                    ## update pop for forecast scenario
                    aPredForecast = np.dot(tArrayForecast_i[k], alpha_j)
                    xt_Forecast = xt_Forecast + aPredForecast + (bPara_ji * 
                        xt_Forecast)
                    xt_Forecast = np.random.normal(xt_Forecast, sigma_ji)
                    Nt_Forecast = np.exp(xt_Forecast)
                    ## update pop for Present scenario
                    aPredPresent = np.dot(tArrayPresent_i[k], alpha_j)
                    xt_Present = xt_Present + aPredPresent + (bPara_ji * 
                        xt_Present)
                    xt_Present = np.random.normal(xt_Present, sigma_ji)
                    Nt_Present = np.exp(xt_Present)

                   ## update pop for Vegetable increase scenario
                    aPredVeg = np.dot(tArrayVeg, alpha_j)
                    xt_vegIncrease = (xt_vegIncrease + aPredVeg + (bPara_ji * 
                        xt_vegIncrease))
                    xt_vegIncrease = np.random.normal(xt_vegIncrease, sigma_ji)
                    Nt_vegIncrease = np.exp(xt_vegIncrease)
                    
                    ## update pop for temp and veg increase
                    newTemp = tArrayForecast_i[k, 5]
                    tArrayTempVeg = tArrayVeg.copy()
                    tArrayTempVeg[5] = newTemp
                    a_PredvegTemp = np.dot(tArrayTempVeg, alpha_j)
                    xt_vegTempIncrease = (xt_vegTempIncrease + a_PredvegTemp + 
                        (bPara_ji * xt_vegTempIncrease))
                    xt_vegTempIncrease = np.random.normal(xt_vegTempIncrease, sigma_ji)
                    Nt_vegTempIncrease = np.exp(xt_vegTempIncrease)
                    if k >= self.minMonthKeep:
                        if i == 0:
                            self.presentResults[j, cc] = Nt_Present
                            self.forecastResults[j, cc] = Nt_Forecast
                            self.vegResults[j, cc] = Nt_vegIncrease
                            self.vegTempResults[j, cc] = Nt_vegTempIncrease
                        else:
                            self.presentResults[j, (cc + 12)] = Nt_Present
                            self.forecastResults[j, (cc + 12)] = Nt_Forecast
                            self.vegResults[j, (cc + 12)] = Nt_vegIncrease
                            self.vegTempResults[j, (cc + 12)] = Nt_vegTempIncrease
                        cc += 1
                    if i == 1:
                        kk = k + self.nMonths1Habitat
                        self.meanForecast30Y[0, kk] += Nt_Present
                        self.meanForecast30Y[1, kk] += Nt_Forecast
#                        print('kk', kk, 'Npres', Nt_Present)
                    else:
                        self.meanForecast30Y[0, k] += Nt_Present
                        self.meanForecast30Y[1, k] += Nt_Forecast
#                        print('k', k, 'Npres', Nt_Present)
                    
        self.meanForecast30Y = self.meanForecast30Y / self.params.iter
#        print('for', np.transpose(self.meanForecast30Y[:, :12]))

    def makeSummaryTable(self):
        """
        ## calc means and 95% CI of results
        """
        self.summaryPresent = np.zeros((3, 24))
        self.summaryForecast = np.zeros((3, 24))
        self.summaryPresent[0] = np.mean(self.presentResults, axis = 0)
        self.summaryPresent[1:3] = mquantiles(self.presentResults,
            prob = [0.025, 0.975], axis = 0)
        self.summaryForecast[0] = np.mean(self.forecastResults, axis = 0)
        self.summaryForecast[1:3] = mquantiles(self.forecastResults,
            prob = [0.025, 0.975], axis = 0)
        self.summaryVeg = np.zeros((3,24))
        self.summaryVeg[0] = np.mean(self.vegResults, axis = 0)
        self.summaryVeg[1:3] = mquantiles(self.vegResults,
            prob = [0.025, 0.975], axis = 0)
        self.summaryVegTemp = np.zeros((3,24))
        self.summaryVegTemp[0] = np.mean(self.vegTempResults, axis = 0)
        self.summaryVegTemp[1:3] = mquantiles(self.vegTempResults,
            prob = [0.025, 0.975], axis = 0)
        

    def plotSim(self):
        """
        plot trap rate for forecast and historical means and CI
        """
        monthNumbers = np.array([0, 2, 4, 6, 8, 10])
        self.calendarMonths = np.array(['January', 'March', 'May', 'July', 
            'September', 'November'])        
        P.figure(figsize=(14, 11))
        titleNames = ['Lowland; forecast temperature', 
            'Lowland; present temperature',
            'Upland; forecast temperature', 
            'Upland; present temperature']
        mask0 = np.arange(24)
        xPlot = np.arange(12)
        maskLowland = np.arange(24) < 12
        maskUpland = np.arange(24) >= 12
        yMax = np.max(np.append(self.summaryForecast[2], self.summaryPresent[2]))+2
        yMin = np.min(np.append(self.summaryForecast[1], self.summaryPresent[1]))-2
        cc = 0
        ## loop scenario and habitat
        for i in range(4):
            if (i == 0) | (i == 2):
                table_i = self.summaryForecast.copy()
            else:
                table_i = self.summaryPresent.copy()
            if (i < 2):
                table_i = table_i[:, maskLowland]
            else:
                table_i = table_i[:, maskUpland]               
            ax1 = P.subplot(2, 2, (i + 1))
            P.title(titleNames[i], fontsize = 12)
            lns1 = ax1.plot(xPlot, table_i[0], 'ko-', ms = 10,
                linewidth = 5, color = 'k', label = 'Mean pop. size')
            lns2 = ax1.plot(xPlot, table_i[1], 
                linewidth = 1.5, color = 'k',label = '95% CI pop. size')
            lns3 = ax1.plot(xPlot, table_i[2], 
                linewidth = 1.5, color = 'k')
            lns = lns1 + lns2  
            labs = [l.get_label() for l in lns]
            if i == 0:
                ax1.legend(lns, labs, loc = 'upper right')
            ax1.set_ylim([yMin, yMax])
            ax1.set_xlim(-.5, 11.5)
            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            if (i == 0) | (i == 2):
                ax1.set_ylabel('Forecast population density', fontsize = 17)
#            else:
#                ax1.set_ylabel('Historic traprate x 100', fontsize = 17)
            if i < 2:
                P.xticks(monthNumbers, '')
            if i > 1:
                ax1.set_xlabel('Months in 2045', fontsize = 17)
                P.xticks(monthNumbers, self.calendarMonths, rotation = 30, 
                    fontsize = 12)
        figFname = 'forcast30Y.png'
        fnamePath = os.path.join(self.params.outputDataPath, figFname)
        P.savefig(fnamePath, format='png')
        P.show()
 

    def plotSimShare(self):
        """
        plot trap rate for forecast and historical means and CI
        """
        monthNumbers = np.array([0, 2, 4, 6, 8, 10])
        self.calendarMonths = np.array(['January', 'March', 'May', 'July', 
            'September', 'November'])        
        P.figure(figsize=(14, 11))
        titleNames = ['Lowland', 'Upland']
        mask0 = np.arange(24)
        xPlot = np.arange(12)
        maskLowland = np.arange(24) < 12
        maskUpland = np.arange(24) >= 12
        yMax = np.max(np.append(self.summaryForecast[2], self.summaryPresent[2])) + 2
        yMin = np.min(np.append(self.summaryForecast[1], self.summaryPresent[1])) - 2
        cc = 0
        ## loop habitat
        for i in range(2):
            if (i == 0):
                tableForecast_i = self.summaryForecast[:, maskLowland] 
                tablePresent_i = self.summaryPresent[:, maskLowland] 
            else:
                tableForecast_i = self.summaryForecast[:, maskUpland] 
                tablePresent_i = self.summaryPresent[:, maskUpland]  
            ax1 = P.subplot(2, 1, (i + 1))
            P.title(titleNames[i], fontsize = 12)
            lns1 = ax1.plot(xPlot, tablePresent_i[0], 'ko-', ms = 8,
                linewidth = 4, color = 'k', label = 'Present temperature scenario')
            lns2 = ax1.plot(xPlot, tablePresent_i[1], 
                linewidth = 1., color = 'k', linestyle = '--')
            lns3 = ax1.plot(xPlot, tablePresent_i[2], 
                linewidth = 1., color = 'k', linestyle = '--')
            lns4 = ax1.plot(xPlot, tableForecast_i[0], 'ko-', ms = 8,
                linewidth = 4, color = 'r', label = 'Increased temperature scenario')
            lns5 = ax1.plot(xPlot, tableForecast_i[1], 
                linewidth = 1., color = 'r', linestyle = '--')
            lns6 = ax1.plot(xPlot, tableForecast_i[2], 
                linewidth = 1., color = 'r', linestyle = '--')
            lns = lns1 + lns4  
            labs = [l.get_label() for l in lns]
            if i == 1:
                ax1.legend(lns, labs, loc = 'upper right')
            ax1.set_ylim([yMin, yMax])
            ax1.set_xlim(-.5, 11.5)
            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            ax1.set_ylabel('Forecast population density', fontsize = 17)
            if i == 0:
                P.xticks(monthNumbers, '')
            if i == 1:
                ax1.set_xlabel('Months in 2045', fontsize = 17)
                P.xticks(monthNumbers, self.calendarMonths, rotation = 30, 
                    fontsize = 12)
        figFname = 'PresentForcast30Y.png'
        fnamePath = os.path.join(self.params.outputDataPath, figFname)
        P.savefig(fnamePath, format='png')
        P.show()
 

 
    def plotSimVeg(self):
        """
        plot trap rate for forecast and historical means and CI
        """
        monthNumbers = np.array([0, 2, 4, 6, 8, 10])
        self.calendarMonths = np.array(['January', 'March', 'May', 'July', 
            'September', 'November'])        
        P.figure(figsize=(14, 11))
        titleNames = ['Lowland', 'Upland']
        mask0 = np.arange(24)
        xPlot = np.arange(12)
        maskLowland = np.arange(24) < 12
        maskUpland = np.arange(24) >= 12
        yMax = np.max(np.append(self.summaryVeg[2], self.summaryPresent[2])) + 2
        yMin = np.min(np.append(self.summaryVeg[1], self.summaryPresent[1])) - 2
        cc = 0
        ## loop habitat
        for i in range(2):
            if (i == 0):
                tableForecast_i = self.summaryVeg[:, maskLowland] 
                tablePresent_i = self.summaryPresent[:, maskLowland] 
            else:
                tableForecast_i = self.summaryVeg[:, maskUpland]
                tablePresent_i = self.summaryPresent[:, maskUpland]  
            ax1 = P.subplot(2, 1, (i + 1))
            P.title(titleNames[i], fontsize = 12)
            lns1 = ax1.plot(xPlot, tablePresent_i[0], 'ko-', ms = 8,
                linewidth = 4, color = 'k', label = 'Present temperature scenario')
            lns2 = ax1.plot(xPlot, tablePresent_i[1], 
                linewidth = 1., color = 'k', linestyle = '--')
            lns3 = ax1.plot(xPlot, tablePresent_i[2], 
                linewidth = 1., color = 'k', linestyle = '--')
            lns4 = ax1.plot(xPlot, tableForecast_i[0], 'ko-', ms = 8,
                linewidth = 4, color = 'r', label = 'Increased vegetable scenario')
            lns5 = ax1.plot(xPlot, tableForecast_i[1], 
                linewidth = 1., color = 'r', linestyle = '--')
            lns6 = ax1.plot(xPlot, tableForecast_i[2], 
                linewidth = 1., color = 'r', linestyle = '--')
                               
            lns = lns1 + lns4  
            labs = [l.get_label() for l in lns]
            if i == 1:
                ax1.legend(lns, labs, loc = 'upper right')
            ax1.set_ylim([yMin, yMax])
            ax1.set_xlim(-.5, 11.5)
            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            ax1.set_ylabel('Forecast population density', fontsize = 17)
            if i == 0:
                P.xticks(monthNumbers, '')
            if i == 1:
                ax1.set_xlabel('Months in 2045', fontsize = 17)
                P.xticks(monthNumbers, self.calendarMonths, rotation = 30, 
                    fontsize = 12)
        figFname = 'PresentVeg30Y.png'
        fnamePath = os.path.join(self.params.outputDataPath, figFname)
        P.savefig(fnamePath, format='png')
        P.show()
 

 
#    def makeSummaryTable2(self):
#        """
#        ## calc means and 95% CI of results
#        """
#        self.summaryPresent = np.zeros((3, 24))
#        self.summaryForecast = np.zeros((3, 24))
#        self.summaryPresent[0] = np.mean(self.presentResults, axis = 0)
#        self.summaryPresent[1:3] = mquantiles(self.presentResults,
#            prob = [0.025, 0.975], axis = 0)
#        self.summaryForecast[0] = np.mean(self.forecastResults, axis = 0)
#        self.summaryForecast[1:3] = mquantiles(self.forecastResults,
#            prob = [0.025, 0.975], axis = 0)
#        print('sumForecast', np.round(self.summaryForecast, 1))
#        print('sumPres', np.round(self.summaryPresent, 1))


            
    def plot30YearMeans(self):
        """
        plot forecasted mean pop size for present and projected temperatures
        over 30 years
        """
        self.getDates()
        P.figure(figsize=(14, 11))
        titleNames = ['Lowland', 'Upland'] 
        yMax = np.max(self.meanForecast30Y) + 2
        yMin = np.min(self.meanForecast30Y)  - 2
        cc = 0
        ## loop scenario and habitat
        for i in range(2):
            habmask_i = self.habMask == i
            table_i = self.meanForecast30Y[:, habmask_i]

            print('len date', len(self.datesForecast), 'len data', len(table_i[1]))

            ax1 = P.subplot(2, 1, (i + 1))           
            P.title(titleNames[i], fontsize = 12)
            lns1 = ax1.plot(self.datesForecast, table_i[0], linewidth = 2., 
                color = 'k', label = 'Mean N; present temp.')
            lns2 = ax1.plot(self.datesForecast, table_i[1], linewidth = 2., 
                color = 'r', label = 'Mean N; forecast temp.')
            lns = lns1 + lns2  
            labs = [l.get_label() for l in lns]
            if i == 1:
                ax1.legend(lns, labs, loc = 'upper right')
            ax1.set_ylim([yMin, yMax])
#            ax1.set_xlim(-.5, 11.5)
            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            ax1.set_ylabel('Forecast population density', fontsize = 17)
            if i == 0:
                P.xticks(self.datesPlot, '')
            if i == 1:
                ax1.set_xlabel('Months (2016-2045)', fontsize = 17)
                P.xticks(self.datesPlot, rotation = 30, 
                    fontsize = 12)
        figFname = 'meanN30Y.png'
        fnamePath = os.path.join(self.params.outputDataPath, figFname)
        P.savefig(fnamePath, format='png')
        P.show()
 

    def getDates(self):
        """
        ## get dates for plotting
        """
        mon_i = 1
        years = self.years1Habitat + 2016
        mons = self.months1Habitat + 1
        self.datesForecast = datetime.date(2016, 1, 15)
        for i in range(1, self.nMonths1Habitat):
            self.datesForecast = np.append(self.datesForecast, 
                datetime.date(years[i], mons[i], 15))
        ## get plotting dates
        subsetArray = np.arange(self.nMonths1Habitat, step = 36)
        self.datesPlot = self.datesForecast[subsetArray]
                
                

    def plot30YrVegTemp(self):
        """
        plot forecasted mean pop size for present and projected temperatures
        over 30 years
        """
        self.getDates()
        P.figure(figsize=(14, 11))
        titleNames = ['Lowland', 'Upland'] 
        yMax = np.max(self.meanForecast30Y)  + 2
        yMin = np.min(self.meanForecast30Y) - 2
        cc = 0
        ## loop scenario and habitat
        for i in range(2):
            habmask_i = self.habMask == i
            table_i = self.meanForecast30Y[:, habmask_i] 

            print('len date', len(self.datesForecast), 'len data', len(table_i[1]))

            ax1 = P.subplot(2, 1, (i + 1))           
            P.title(titleNames[i], fontsize = 12)
            lns1 = ax1.plot(self.datesForecast, table_i[0], linewidth = 2., 
                color = 'k', label = 'Mean N; present temp.')
            lns2 = ax1.plot(self.datesForecast, table_i[1], linewidth = 2., 
                color = 'r', label = 'Mean N; forecast temp.')
            lns = lns1 + lns2  
            labs = [l.get_label() for l in lns]
            if i == 1:
                ax1.legend(lns, labs, loc = 'upper right')
            ax1.set_ylim([yMin, yMax])
#            ax1.set_xlim(-.5, 11.5)
            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            ax1.set_ylabel('Forecast population density', fontsize = 17)
            if i == 0:
                P.xticks(self.datesPlot, '')
            if i == 1:
                ax1.set_xlabel('Months in 2045', fontsize = 17)
                P.xticks(self.datesPlot, rotation = 30, 
                    fontsize = 12)
        figFname = 'meanN30Y.png'
        fnamePath = os.path.join(self.params.outputDataPath, figFname)
        P.savefig(fnamePath, format='png')
        P.show()


 
    def plotVegTempShare(self):
        """
        plot trap rate for forecast and historical means and CI
        """
        monthNumbers = np.array([0, 2, 4, 6, 8, 10])
        self.calendarMonths = np.array(['January', 'March', 'May', 'July', 
            'September', 'November'])        
        P.figure(figsize=(14, 11))
        titleNames = ['Lowland', 'Upland']
        mask0 = np.arange(24)
        xPlot = np.arange(12)
        maskLowland = np.arange(24) < 12
        maskUpland = np.arange(24) >= 12
        yMax = np.max(np.append(self.summaryVegTemp[2], self.summaryPresent[2])) + 2
        yMin = np.min(np.append(self.summaryVegTemp[1], self.summaryPresent[1])) - 2
        cc = 0
        ## loop habitat
        for i in range(2):
            if (i == 0):
                tableForecast_i = self.summaryVegTemp[:, maskLowland] 
                tablePresent_i = self.summaryPresent[:, maskLowland] 
            else:
                tableForecast_i = self.summaryVegTemp[:, maskUpland] 
                tablePresent_i = self.summaryPresent[:, maskUpland] 
            ax1 = P.subplot(2, 1, (i + 1))
            P.title(titleNames[i], fontsize = 12)
            lns1 = ax1.plot(xPlot, tablePresent_i[0], 'ko-', ms = 8,
                linewidth = 4, color = 'k', label = 'Present temp. and veg. scenario')
            lns2 = ax1.plot(xPlot, tablePresent_i[1], 
                linewidth = 1., color = 'k', linestyle = '--')
            lns3 = ax1.plot(xPlot, tablePresent_i[2], 
                linewidth = 1., color = 'k', linestyle = '--')
            lns4 = ax1.plot(xPlot, tableForecast_i[0], 'ko-', ms = 8,
                linewidth = 4, color = 'r', 
                label = 'Increased temp. and veg. scenario')
            lns5 = ax1.plot(xPlot, tableForecast_i[1], 
                linewidth = 1., color = 'r', linestyle = '--')
            lns6 = ax1.plot(xPlot, tableForecast_i[2], 
                linewidth = 1., color = 'r', linestyle = '--')
                               
            lns = lns1 + lns4  
            labs = [l.get_label() for l in lns]
            if i == 1:
                ax1.legend(lns, labs, loc = 'upper right')
            ax1.set_ylim([yMin, yMax])
            ax1.set_xlim(-.5, 11.5)
            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            ax1.set_ylabel('Forecast population density', fontsize = 17)
            if i == 0:
                P.xticks(monthNumbers, '')
            if i == 1:
                ax1.set_xlabel('Months in 2045', fontsize = 17)
                P.xticks(monthNumbers, self.calendarMonths, rotation = 30, 
                    fontsize = 12)
        figFname = 'PresentTempVeg30Y.png'
        fnamePath = os.path.join(self.params.outputDataPath, figFname)
        P.savefig(fnamePath, format='png')
        P.show()
         
                
                
                
                
                
def main():

    ## Instance of Parameters class
    params = Params()
    simulateObj = Simulate(params)



if __name__ == '__main__':
    main()

